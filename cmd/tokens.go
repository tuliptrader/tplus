package cmd

import (
	"fmt"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"go.uber.org/dig"
	"os"
)

func TokensCommand(c *dig.Container, super *cobra.Command) {


	var root = &cobra.Command{
		Use:   "tokens",
		Short: "Manage access tokens",
	}

	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List tokens",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				tokens,err := d.GetTokens()
				if err != nil {
					fmt.Println(err)
				} else {

					data := [][]string{}

					table := tablewriter.NewWriter(os.Stdout)
					table.SetHeader([]string{"Id","UserId", "Created", "Last used"})


					for _, c := range tokens {
						data = append(data, []string{
							c.Id, c.UserId, c.Created.Format("January 02,  15:04:05"),c.LastRead.Format("January 02,  15:04:05"),
						})
					}
					for _, v := range data {
						table.Append(v)
					}
					table.SetBorder(false)
					fmt.Println()
					table.Render() // Send output
					fmt.Println()

				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var rm = &cobra.Command{
		Use:   "rm <id>",
		Short: "Delete Token",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				var id string

				if len(args) == 1 {
					id = args[0]
				} else {
					fmt.Println("\nUsage: rm <tokenId>\n")
					os.Exit(1)
				}

				u,e := d.GetInviteCodeById(id)
				if e == nil && u.Id == id {
					d.Delete(u)
				}

			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}


	root.AddCommand(rm)
	root.AddCommand(ls)
	super.AddCommand(root)


}