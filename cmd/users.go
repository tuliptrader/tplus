package cmd

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"go.uber.org/dig"
	"os"
)

func UserCommands(c *dig.Container, super *cobra.Command) {

	var root = &cobra.Command{
		Use:   "user",
		Short: "Manage Server Users",
	}

	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List Users",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				users, err := d.GetUsers()
				if err != nil {
					fmt.Println(err)
				} else {

					data := [][]string{}

					table := tablewriter.NewWriter(os.Stdout)
					table.SetHeader([]string{"Id", "Username", "Admin", "Last Active"})

					for _, u := range users {
						ad := "no"
						if u.Admin {
							ad = "yes"
						}
						data = append(data, []string{
							u.Id, u.Username, ad, u.LastRead.Format("January 02,  15:04:05"),
						})
					}
					for _, v := range data {
						table.Append(v)
					}
					table.SetBorder(false)
					fmt.Println()
					table.Render() // Send output
					fmt.Println()

				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var rm = &cobra.Command{
		Use:   "rm <id>",
		Short: "Delete User",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				var id string

				if len(args) == 1 {
					id = args[0]
				} else {
					fmt.Println("\nUsage: rm <userId>\n")
					os.Exit(1)
				}

				u, e := d.GetUserById(id)
				if e == nil && u.Id == id {
					d.Delete(u)
				}

			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var create = &cobra.Command{
		Use:   "create",
		Short: "Create a new User",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {

				var qs = []*survey.Question{
					{
						Name:     "Username",
						Prompt:   &survey.Input{Message: "Username"},
						Validate: survey.Required,
					},
					{
						Name:     "Password",
						Prompt:   &survey.Password{Message: "Password"},
						Validate: survey.Required,
					},
					{
						Name: "access",
						Prompt: &survey.Select{
							Message: "Admin or default user",
							Options: []string{"user", "admin"},
							Default: "admin",
						},
					},
				}

				answers := struct {
					Password string
					Access   string
					Username string
				}{}

				err := survey.Ask(qs, &answers)
				if err != nil {
					fmt.Println(err.Error())
					return
				}

				Id := db.GetRandomId("user")

				user := db.User{}
				user.Username = answers.Username
				user.SetPassword(answers.Password)
				user.Admin = true
				if answers.Access != "admin" {
					user.Admin = false
				}
				user.Id = Id
				e := d.Create(&user)

				if e != nil {
					fmt.Println(e)
				} else {
					fmt.Println("User created.")
				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	root.AddCommand(rm)
	root.AddCommand(ls)
	root.AddCommand(create)
	super.AddCommand(root)

}
