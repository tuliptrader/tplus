package cmd

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/gobuffalo/packr"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"go.uber.org/dig"
	"gopkg.in/yaml.v2"
	"os"
	"syscall"
)

func ConfigCommands(c *dig.Container, super *cobra.Command) {

	var root = &cobra.Command{
		Use:   "config",
		Short: "Show and edit your Configration",
	}

	var view = &cobra.Command{
		Use:   "view",
		Short: "Show Configuration",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(conf *config.Config) {
				d, _ := yaml.Marshal(conf)
				fmt.Println()
				fmt.Println(string(d))
				fmt.Println()
			})
		},
	}

	var init = &cobra.Command{
		Use:   "init",
		Short: "Create config file",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(box *packr.Box) {
				var mode string
				mode = "workshop"

				/*
					var qs = []*survey.Question{
						{
							Name: "mode",
							Prompt: &survey.Select{
								Message: "Will you connect to this server locally or from somewhere else?",
								Options: []string{"local", "remote"},
								Default: "local",
							},
						},
					}

					answers := struct {
						Mode          string
					}{}


					err := survey.Ask(qs, &answers)
					if err != nil {
						fmt.Println(err.Error())
						return
					}


					/*
					if answers.Mode == "local" {
						mode = "local"
					} else {

						var qs = []*survey.Question{
							{
								Name: "mode",
								Prompt: &survey.Select{
									Message: "Will this server accessed by your team or strangers (workshops) ?",
									Options: []string{"team", "workshop"},
									Default: "team",
								},
							},
						}

						answers2 := struct {
							Mode          string
						}{}

						err := survey.Ask(qs, &answers2)
						if err != nil {
							fmt.Println(err.Error())
							return
						}

						mode = answers2.Mode

					}

				*/

				var hd string

				if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
					hd, _ = os.UserHomeDir() // no trailing /
				} else {
					hd = os.Getenv("TPLUS_SERVER_CONFIG_PATH")
				}

				path := hd + "/.tplus"

				conf := config.Config{}
				conf.DataDir = path + "/server/data"

				syscall.Mkdir(conf.DataDir, 0777)

				if mode == "local" {
					conf.Mode = "local"
					conf.Register.Enabled = false
					conf.Register.RequireInviteCode = false
					conf.Register.MaxEnvs = 0
					conf.Register.AllowPublicNodes = true
					conf.Register.ReCaptchaCode = "na"
					conf.Register.ReCaptchaEnabled = false
				}

				if mode == "team" {
					conf.Mode = "team"
					conf.Register.Enabled = true
					conf.Register.RequireInviteCode = true
					conf.Register.MaxEnvs = 50
					conf.Register.AllowPublicNodes = true
					conf.Register.ReCaptchaCode = "na"
					conf.Register.ReCaptchaEnabled = false
				}

				if mode == "workshop" {
					conf.Mode = "workshop"
					conf.Register.Enabled = true
					conf.Register.RequireInviteCode = true
					conf.Register.MaxEnvs = 3
					conf.Register.AllowPublicNodes = false
					conf.Register.ReCaptchaCode = "na"
					conf.Register.ReCaptchaEnabled = false
				}

				conf.Box = box

				qs := []*survey.Question{
					{
						Name:   "port",
						Prompt: &survey.Input{Message: "What port should the Web UI + API use?"},
					},
				}

				answers3 := struct {
					Port int
				}{}

				err := survey.Ask(qs, &answers3)
				if err != nil {
					fmt.Println(err.Error())
					return
				}

				conf.WebPort = answers3.Port

				conf.Save()

				fmt.Println("Configuration saved! \n")

			})
			if e != nil {
				fmt.Println(e)
			}
		},
	}

	root.AddCommand(init)
	root.AddCommand(view)
	super.AddCommand(root)

}
