package cmd

import (
"fmt"
"github.com/AlecAivazis/survey/v2"
"github.com/olekukonko/tablewriter"
"github.com/spf13/cobra"
"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
"go.uber.org/dig"
"os"
	"strconv"
)

func CodesCommand(c *dig.Container, super *cobra.Command) {


	var root = &cobra.Command{
		Use:   "codes",
		Short: "Manage invite codes",
	}

	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List Invite Codes",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				codes,err := d.GetInviteCodes()
				if err != nil {
					fmt.Println(err)
				} else {

					data := [][]string{}

					table := tablewriter.NewWriter(os.Stdout)
					table.SetHeader([]string{"Id","Description", "Created", "Used", "Max used"})


					for _, c := range codes {
						data = append(data, []string{
							c.Id, c.Description, c.Created.Format("January 02,  15:04:05"),strconv.Itoa(c.Used),strconv.Itoa(c.MaxUsed),
						})
					}
					for _, v := range data {
						table.Append(v)
					}
					table.SetBorder(false)
					fmt.Println()
					table.Render() // Send output
					fmt.Println()

				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var rm = &cobra.Command{
		Use:   "rm <id>",
		Short: "Delete InviteCode",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				var id string

				if len(args) == 1 {
					id = args[0]
				} else {
					fmt.Println("\nUsage: rm <codeId>\n")
					os.Exit(1)
				}

				u,e := d.GetInviteCodeById(id)
				if e == nil && u.Id == id {
					d.Delete(u)
				}

			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var create = &cobra.Command{
		Use:   "create",
		Short: "Create a new InviteCode",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {

				var qs = []*survey.Question{
					{
						Name:     "description",
						Prompt:   &survey.Input{Message: "Invite description"},
						Validate: survey.Required,
						Transform: survey.Title,
					},
					{
						Name:     "MaxUsed",
						Prompt:   &survey.Input{Message: "Max Used ( 0 for unlimited) "},
						Validate: survey.Required,
						Transform: survey.Title,
					},
				}

				answers := struct {
					Description string
					MaxUsed int
				}{}

				err := survey.Ask(qs, &answers)
				if err != nil {
					fmt.Println(err.Error())
					return
				}

				Id := db.GetRandomId("ic")

				invite := db.InviteCode{}
				invite.Description = answers.Description
				invite.Used = 0
				invite.MaxUsed = answers.MaxUsed
				invite.Id = Id
				e := d.Create(&invite)

				if e != nil {
					fmt.Println(e)
				} else {
					fmt.Println("Invite created.")
				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}




	root.AddCommand(rm)
	root.AddCommand(ls)
	root.AddCommand(create)
	super.AddCommand(root)


}