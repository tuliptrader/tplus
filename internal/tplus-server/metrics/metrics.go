package metrics

import (
	"bytes"
	"encoding/json"
	"github.com/karlseguin/ccache"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"go.etcd.io/bbolt"
	"strings"
	"time"
)

type Metrics struct {
	tm          *task.TaskManager
	bolt        *bbolt.DB
	db          *db.DB
	buckets     []string
	nodeMetrics map[string]*NodeMetrics
	docker      *service.DockerService
	cache       *ccache.Cache
	events      *events.Events
}

func NewMetrics(tm *task.TaskManager, db *bbolt.DB, dbw *db.DB, docker *service.DockerService, events *events.Events) *Metrics {
	mm := Metrics{}
	mm.tm = tm
	go mm.Init()
	mm.bolt = db
	mm.db = dbw
	mm.buckets = []string{}
	mm.nodeMetrics = map[string]*NodeMetrics{}
	mm.docker = docker
	mm.cache = ccache.New(ccache.Configure().MaxSize(400).ItemsToPrune(10))
	mm.events = events
	return &mm
}

func (this *Metrics) Init() {
	time.Sleep(5 * time.Second)
	for {
		this.StartNodeManagers()

		time.Sleep(1 * time.Second)
	}
}

func (this *Metrics) GetMetrics(envid, source, metric string, start, end time.Time) []db.MetricSimple {

	// breaks series into 6 hour splits and caches them,

	startNormlised := time.Date(start.Year(), start.Month(), start.Day(), start.Hour(), 0, 0, 0, start.Location())
	endNormalised := time.Date(end.Year(), end.Month(), end.Day(), end.Add(-6*time.Hour).Hour(), 0, 0, 0, end.Location())

	current := startNormlised

	result := []db.MetricSimple{}
	if startNormlised.Before(endNormalised) {
		for {
			ckey := envid + source + metric + current.Format("2006-01-02T15:04:05Z")
			item, err := this.cache.Fetch(ckey, time.Minute*25, func() (interface{}, error) {
				r := this.getMetricDB(envid, source, metric, current, current.Add(1*time.Hour))
				return r, nil
			})

			if err == nil {
				for _, i := range item.Value().([]db.MetricSimple) {
					if i.Time.Before(end) && start.Before(i.Time) {
						result = append(result, i)
					}
				}
			}

			current = current.Add(6 * time.Hour)
			if endNormalised.Before(current) {
				break
			}
		}

		appendData := this.getMetricDB(envid, source, metric, endNormalised, end)
		result = append(result, appendData...)
	} else {
		return this.getMetricDB(envid, source, metric, start, end)
	}
	return result
}

func (this *Metrics) getMetricDB(envid, source, metric string, start, end time.Time) []db.MetricSimple {
	res := []db.MetricSimple{}
	bucketName := strings.ToLower(envid + source + metric)

	_ = this.bolt.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		if b == nil {
			return nil
		}
		c := b.Cursor()
		format := "2006-01-02T15:04:05Z"
		min := []byte(start.Format(format)) //RFC3339
		max := []byte(end.Format(format))

		for k, v := c.Seek(min); k != nil && bytes.Compare(k, max) <= 0; k, v = c.Next() {
			tmp := db.MetricSimple{}
			json.Unmarshal(v, &tmp)
			if tmp.Time.Before(end) && start.Before(tmp.Time) {
				res = append(res, tmp)
			}
		}
		return nil
	})

	return res
}

func (this *Metrics) StartNodeManagers() {
	for key, nm := range this.nodeMetrics {
		if !nm.IsRunning() {
			delete(this.nodeMetrics, key)
		}
	}
	list, err := this.db.GetEnvironments()
	if err != nil {
		return
	}
	for i, e := range list {
		if _, ok := this.nodeMetrics[list[i].Id]; !ok && e.Tezos.Container.Status == "running" {
			newNM := NewNodeMetrics(&list[i], this.savemetric, this.docker)
			this.nodeMetrics[list[i].Id] = newNM
		}

	}
}

func (this *Metrics) savemetric(m *db.Metric) {
	bucketName := strings.ToLower(m.EnvironmentId + m.Source + m.Metric)
	this.makeBucket(bucketName)
	format := "2006-01-02T15:04:05Z"
	key := []byte(m.Time.Format(format))
	// this.events.MetricEvent(m.EnvironmentId + "::" + m.Source + "::" + m.Metric, m)
	_ = this.bolt.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		err := b.Put(key, []byte(m.ToBytes()))
		return err
	})
}

func (this *Metrics) makeBucket(b string) {
	for _, a := range this.buckets {
		if a == b {
			return
		}
	}

	_ = this.bolt.Update(func(tx *bbolt.Tx) error {
		_, _ = tx.CreateBucket([]byte(b))
		return nil
	})

	this.buckets = append(this.buckets, b)
	return
}
