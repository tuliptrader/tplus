package metrics

import (
	"bufio"
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type NodeMetrics struct {
	env     *db.Environment
	save    func(metric *db.Metric)
	Running bool
	docker  *service.DockerService
	bhready bool
}

func NewNodeMetrics(env *db.Environment, savefnc func(metric *db.Metric), docker *service.DockerService) *NodeMetrics {
	nm := NodeMetrics{}
	nm.env = env
	nm.save = savefnc
	nm.Running = true
	nm.docker = docker
	nm.bhready = false
	go nm.Init()
	return &nm
}

func (this *NodeMetrics) checkBHReady() {

	// we dont want to bombard the node with http request if it is not
	// ready for them yet, check it syncing started via logs, then send http gets
	logs, err := this.docker.GetLogs(this.env.Tezos.Container.Id, false, "100")
	count := 0
	scanner := bufio.NewScanner(logs)
	if err != nil {
		return
	}
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "successfully validated") {
			count++
			if count == 3 {
				this.bhready = true
				break
			}
		}
	}
}

func (this *NodeMetrics) IsRunning() bool {
	return this.Running
}

func (this *NodeMetrics) saveMetrics() {
	stat := this.docker.GetContainerStats(this.env.Tezos.Container.Id)
	mem := stat.GetMemoryUsage()
	cpu := stat.CalculateCPUPercent()
	netIn := stat.GetNetworkIn()
	netOut := stat.GetNetworkOut()
	if mem == 0 || cpu == 0 {
		this.Running = false
		return
		// metrics will spawn a new NodeMetrics if container state changes
	}

	mmem := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "mem", Value: mem}
	mcpu := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "cpu", Value: cpu}
	mnetIn := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "netIn", Value: netIn}
	mnetOut := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "netOut", Value: netOut}

	this.save(&mmem)
	this.save(&mcpu)
	this.save(&mnetIn)
	this.save(&mnetOut)

	storage, err := this.DirSize(this.env.Tezos.DataDir)
	if err == nil {
		mdisk := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "disksize", Value: float64(storage)}
		this.save(&mdisk)
	}

	if !this.bhready {
		this.checkBHReady()
		if !this.bhready {
			return
		}
	}

	// bh := uint64(0)
	info, err := this.docker.GetContainerInfo(this.env.Tezos.Container.Id)
	if err != nil {
		this.Running = false
		return
	}
	ip := info.NetworkSettings.Networks[this.env.Tezos.Container.Network].IPAddress
	start := time.Now()
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	resp, err := client.Get("http://" + ip + ":8732/chains/main/blocks/head")
	if err == nil {
		end := time.Now()
		duration := end.Sub(start)
		bytes, _ := ioutil.ReadAll(resp.Body)
		block := tezcon.Block{}
		err2 := json.Unmarshal(bytes, &block)
		if err2 == nil {
			mbh := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "blockheight", Value: float64(block.Header.Level)}
			mlat := db.Metric{Time: time.Now(), EnvironmentId: this.env.Id, Source: "Tezos", Metric: "latency", Value: float64(duration.Nanoseconds())}
			this.save(&mbh)
			this.save(&mlat)
		}
	} else {
		fmt.Println(err)
	}
}

func (this *NodeMetrics) DirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func (this *NodeMetrics) Init() {
	t := time.NewTicker(10 * time.Second)
	for {
		<-t.C
		if this.Running == false {
			break
		}
		go this.saveMetrics()
	}
}
