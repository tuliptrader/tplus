package code

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
)

type CodeContainerCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	project     *db.Project
}

func NewCodeContainerCheck(e *db.Environment, project *db.Project) *CodeContainerCheck {
	t := CodeContainerCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.project = project
	return &t
}

func (this *CodeContainerCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *CodeContainerCheck) GetProgress() string {
	return this.Status
}

func (this *CodeContainerCheck) GetEventPrefix() string {
	return "container_check_code"
}

func (this *CodeContainerCheck) GetName() string {
	return "vscode container Check"
}

func (this *CodeContainerCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *CodeContainerCheck) GetDescription() string {
	return "Check if container exists"
}

func (this *CodeContainerCheck) GetId() string {
	return this.Id
}

func (this *CodeContainerCheck) Run() {

	id, e := this.Docker.GetContainerId(this.project.Id + "_vscode")
	if e != nil || len(id) <= 4 {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}
