package code

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type CodePullTask struct {
	task.TaskBase
	Environment *db.Environment
	project     *db.Project
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewCodePullTask(e *db.Environment, project *db.Project) *CodePullTask {
	t := CodePullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.project = project
	return &t
}

func (this *CodePullTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *CodePullTask) GetProgress() string {
	return this.Status
}

func (this *CodePullTask) GetEventPrefix() string {
	return "code_pull"
}

func (this *CodePullTask) GetName() string {
	return "Pull Image (vscode)"
}

func (this *CodePullTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *CodePullTask) GetDescription() string {
	return "Pulling Image for vscode"
}

func (this *CodePullTask) GetId() string {
	return this.Id
}

func (this *CodePullTask) Run() {
	this.Status = "in Progress"
	start := time.Now()
	var err error
	logs := []byte{}
	image := "docker.io/tuliptools/tplus-code:latest"
	logs, err = this.Docker.PullImageBlocking(image)
	done := time.Now()
	this.result.Logs = map[string]string{
		"Pull Log": string(logs),
	}
	if err == nil {
		this.result.Success = true
		this.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		this.result.Success = false
		this.result.Message = "Error: " + err.Error()
	}
	this.wg.Done()
}
