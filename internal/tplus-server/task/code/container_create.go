package code

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type CodeContainerCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	project     *db.Project
}

func NewCodeContainerCreateTask(e *db.Environment, project *db.Project) *CodeContainerCreate {
	t := CodeContainerCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.project = project
	return &t
}

func (this *CodeContainerCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *CodeContainerCreate) GetProgress() string {
	return this.Status
}

func (this *CodeContainerCreate) GetEventPrefix() string {
	return "container_create_code"
}

func (this *CodeContainerCreate) GetName() string {
	return "vscode Container create"
}

func (this *CodeContainerCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *CodeContainerCreate) GetDescription() string {
	return "create Container"
}

func (this *CodeContainerCreate) GetId() string {
	return this.Id
}

func (this *CodeContainerCreate) Run() {

	datadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/projects/" + this.project.Id + "/code"

	template := docker.GetVsCodeTemplate(this.project, datadir, this.Environment.Tezos.DataDir)

	id, err := this.Docker.ContainerCreate(template)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	this.project.CodeContainer.Id = id
	this.project.CodeContainer.Status = "created"
	this.project.CodeContainer.Name = template.ContainerName
	this.SaveChan <- this.project

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}
