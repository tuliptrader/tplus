package code

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type CodeStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	project     *db.Project
}

func NewCodeStartTask(e *db.Environment, p *db.Project) *CodeStart {
	t := CodeStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.project = p
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *CodeStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *CodeStart) GetProgress() string {
	return this.Status
}

func (this *CodeStart) GetEventPrefix() string {
	return "code_start"
}

func (this *CodeStart) GetName() string {
	return "vscode Start"
}

func (this *CodeStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *CodeStart) GetDescription() string {
	return "Start vscode container"
}

func (this *CodeStart) GetId() string {
	return this.Id
}

func (this *CodeStart) Run() {

	name := this.project.Id + "_vscode"

	info, err := this.Docker.GetContainerInfo(name)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		this.project.CodeContainer.Status = "running"
		this.SaveChan <- this.project
		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.project.CodeContainer.Status = "running"
	this.SaveChan <- this.project

	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}
