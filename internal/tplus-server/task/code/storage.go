package code

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type StoragePrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	project     *db.Project
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewStoragePrepareTask(e *db.Environment, project *db.Project) *StoragePrepareTask {
	t := StoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.project = project
	return &t
}

func (this *StoragePrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *StoragePrepareTask) GetProgress() string {
	return this.Status
}

func (this *StoragePrepareTask) GetEventPrefix() string {
	return "code_storage_prepare"
}

func (this *StoragePrepareTask) GetName() string {
	return "Prepare Project storage"
}

func (this *StoragePrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *StoragePrepareTask) GetDescription() string {
	return "Preparing storage for vscode"
}

func (this *StoragePrepareTask) GetId() string {
	return this.Id
}

func (this *StoragePrepareTask) Run() {
	datadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/projects/" + this.project.Id + "/code"
	os.MkdirAll(datadir, 0777)

	file, err := this.Box.FindString("./project.txt")
	if err != nil {
		this.result.Success = false
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(datadir+"/README.md", []byte(file), 0777)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}

	this.result.Success = true
	this.result.Message = "Storage prepared"
	this.wg.Done()
}
