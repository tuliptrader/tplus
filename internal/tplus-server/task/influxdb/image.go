package influxdb

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type InfluxImagePullTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewInfluxImagePullTask() *InfluxImagePullTask {
	t := InfluxImagePullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	return &t
}

func (this *InfluxImagePullTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *InfluxImagePullTask) GetProgress() string {
	return this.Status
}

func (this *InfluxImagePullTask) GetEventPrefix() string {
	return "influx_pull_image"
}

func (this *InfluxImagePullTask) GetName() string {
	return "Pull docker Image"
}

func (this *InfluxImagePullTask) GetEnvironment() *db.Environment {
	return nil
}

func (this *InfluxImagePullTask) GetDescription() string {
	return "Pulling latest Image for InfluxDB  " + this.Environment.Tezos.Branch
}

func (this *InfluxImagePullTask) GetId() string {
	return this.Id
}

func (this *InfluxImagePullTask) Run() {
	this.Status = "in Progress"
	start := time.Now()
	var err error
	logs := []byte{}
	logs, err = this.Docker.PullImageBlocking("docker.io/influxdb:1.7.7")

	done := time.Now()
	logsS := []string{}
	for _, l := range logs {
		logsS = append(logsS, string(l))
	}
	this.result.Logs = logsS

	if err == nil {
		this.result.Success = true
		this.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		this.result.Success = false
		this.result.Message = "Error: " + err.Error()
	}
	this.wg.Done()
}
