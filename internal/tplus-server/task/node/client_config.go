package node

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type ClientConfigCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewClientConfigCheck(e *db.Environment) *ClientConfigCheck {
	t := ClientConfigCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ClientConfigCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ClientConfigCheck) GetProgress() string {
	return this.Status
}

func (this *ClientConfigCheck) GetEventPrefix() string {
	return "client_config_check"
}

func (this *ClientConfigCheck) GetName() string {
	return "Config Check"
}

func (this *ClientConfigCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ClientConfigCheck) GetDescription() string {
	return "Make sure config file exists"
}

func (this *ClientConfigCheck) GetId() string {
	return this.Id
}

func (this *ClientConfigCheck) Run() {
	if _, err := os.Stat(this.Environment.Tezos.ClientDataDir + "/config.json"); os.IsNotExist(err) {
		this.result.Success = false
		this.result.Message = "Config does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "config exists"
		bytes, _ := ioutil.ReadFile(this.Environment.Tezos.ClientDataDir + "/config.json")
		this.result.Logs = map[string]string{
			"Config file": string(bytes),
		}
	}
	this.wg.Done()
}

/*
 ***********************************************************************************
 */

type ClientConfigCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewClientConfigCreate(e *db.Environment) *ClientConfigCreate {
	t := ClientConfigCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ClientConfigCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ClientConfigCreate) GetProgress() string {
	return this.Status
}

func (this *ClientConfigCreate) GetEventPrefix() string {
	return "client_config_create"
}

func (this *ClientConfigCreate) GetName() string {
	return "Config create"
}

func (this *ClientConfigCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ClientConfigCreate) GetDescription() string {
	return "Write Client config File"
}

func (this *ClientConfigCreate) GetId() string {
	return this.Id
}

func (this *ClientConfigCreate) Run() {

	filename := "./tezos/client_config.json"

	file, err := this.Box.Find(filename)
	if err != nil {
		this.result.Success = false
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(this.Environment.Tezos.ClientDataDir+"/config", file, 0660)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}

	this.result.Success = true
	this.result.Message = "Config file written."
	bytes, _ := ioutil.ReadFile(this.Environment.Tezos.ClientDataDir + "/config")
	this.result.Logs = map[string]string{
		"Config file": string(bytes),
	}
	this.wg.Done()
}
