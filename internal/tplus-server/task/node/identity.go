package node

import (
	"fmt"
	"github.com/cstockton/go-conv"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"strconv"
	"sync"
	"time"
)

type NodeIdentityCheckTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeIdentityCheckTask(e *db.Environment) *NodeIdentityCheckTask {
	t := NodeIdentityCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeIdentityCheckTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeIdentityCheckTask) GetProgress() string {
	return this.Status
}

func (this *NodeIdentityCheckTask) GetEventPrefix() string {
	return "node_identity_check"
}

func (this *NodeIdentityCheckTask) GetName() string {
	return "Identity file check"
}

func (this *NodeIdentityCheckTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeIdentityCheckTask) GetDescription() string {
	return "Check if identity file exists"
}

func (this *NodeIdentityCheckTask) GetId() string {
	return this.Id
}

func (this *NodeIdentityCheckTask) Run() {
	if _, err := os.Stat(this.Environment.Tezos.DataDir + "/identity.json"); os.IsNotExist(err) {
		this.result.Success = false
		this.result.Message = "Identity file does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "Identity file exists"
	}
	this.wg.Done()
}

//--------------------------------------------------------------------------------------------------------------------//

type NodeIdentityCreateTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeIdentityCreateTask(e *db.Environment) *NodeIdentityCreateTask {
	t := NodeIdentityCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeIdentityCreateTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeIdentityCreateTask) GetProgress() string {
	return this.Status
}

func (this *NodeIdentityCreateTask) GetEventPrefix() string {
	return "node_identity_generate"
}

func (this *NodeIdentityCreateTask) GetName() string {
	return "Identity create"
}

func (this *NodeIdentityCreateTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeIdentityCreateTask) GetDescription() string {
	return "Creating new Identity File"
}

func (this *NodeIdentityCreateTask) GetId() string {
	return this.Id
}

func (this *NodeIdentityCreateTask) Run() {

	if this.Environment.Tezos.Branch == "sandbox"{
		file := this.Environment.Tezos.DataDir + "/identity.json"
		content,_ := this.Box.Find("tezos/default_identity.json")
		ioutil.WriteFile(file, content, 0777)

		this.result.Success = true
		this.result.Message = "Identity generated"
		this.wg.Done()
		return
	}


	u, _ := conv.String(time.Now().UnixNano())
	donechan := make(chan struct{})
	randid := db.GetRandomString(4)
	max := 3
	for i := 1; i <= max; i++ {
		go func(i int, e *db.Environment, done chan struct{}) {
			_, err := this.Docker.RunTezosTask(
				"tplus_"+randid+"_identity_gen_task"+u+"_"+strconv.Itoa(i),
				"registry.gitlab.com/tuliptools/tplus:tezos",
				e.Tezos.Container.Network,
				strslice.StrSlice{""},
				strslice.StrSlice{"/usr/local/bin/tezos-node", "identity", "generate", "--data-dir", "/tezosdata"},
				[]mount.Mount{
					mount.Mount{
						Type:   mount.TypeBind,
						Source: e.Tezos.DataDir,
						Target: "/tezosdata",
					},
				},
			)
			if err != nil {
				fmt.Println(err.Error())
				this.result.Message = "Error creating identity: " + err.Error()
				this.result.Success = false
				return
			}
			done <- struct{}{}
		}(i, this.Environment, donechan)
	}
	<-donechan
	this.Docker.DeleteLike("tplus_" + randid + "_identity_gen_task" + u)
	time.Sleep(500 * time.Millisecond)
	this.result.Success = true
	this.result.Message = "Identity generated"
	this.wg.Done()
}
