package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type SuccessTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	Name        string
}

func NewSuccessTask(name string) *SuccessTask {
	t := SuccessTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Name = name
	return &t
}

func (this *SuccessTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *SuccessTask) GetProgress() string {
	return this.Status
}

func (this *SuccessTask) GetEventPrefix() string {
	return "test"
}

func (this *SuccessTask) GetName() string {
	return this.Name
}

func (this *SuccessTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *SuccessTask) GetDescription() string {
	return "Testing Task"
}

func (this *SuccessTask) GetId() string {
	return this.Id
}

func (this *SuccessTask) Run() {
	time.Sleep(500 * time.Millisecond)
	this.result.Success = true
	this.wg.Done()
}

type FailureTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	Name        string
}

func NewFailureTask(name string) *FailureTask {
	t := FailureTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Name = name
	return &t
}

func (this *FailureTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *FailureTask) GetProgress() string {
	return this.Status
}

func (this *FailureTask) GetEventPrefix() string {
	return "test"
}

func (this *FailureTask) GetName() string {
	return this.Name
}

func (this *FailureTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *FailureTask) GetDescription() string {
	return "Testing Task"
}

func (this *FailureTask) GetId() string {
	return this.Id
}

func (this *FailureTask) Run() {
	this.wg.Add(1)
	time.Sleep(500 * time.Millisecond)
	this.result.Success = false
	this.wg.Done()
}
