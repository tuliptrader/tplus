package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
)

/*
 * Removes all chain data from the data dir and restart
 */
type RemoveEnvContainerTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewRemoveEnvContainerTask(e *db.Environment) *RemoveEnvContainerTask {
	t := RemoveEnvContainerTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *RemoveEnvContainerTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *RemoveEnvContainerTask) GetProgress() string {
	return this.Status
}

func (this *RemoveEnvContainerTask) GetEventPrefix() string {
	return "remove_containers"
}

func (this *RemoveEnvContainerTask) GetName() string {
	return "Destroy Environment Container"
}

func (this *RemoveEnvContainerTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *RemoveEnvContainerTask) GetDescription() string {
	return "Docker Cleanup ( Container + Networks )"
}

func (this *RemoveEnvContainerTask) GetId() string {
	return this.Id
}

func (this *RemoveEnvContainerTask) Run() {

	this.Docker.ContainerRM(this.Environment.Tezos.Container.Id)
	containers,_ := this.Docker.FindContainersForEnv(this.Environment.Id)

	for _,c := range containers {
		this.Docker.StopContainer(c)
		this.Docker.DeleteContainer(c)
	}
	this.Docker.RemoveNetwork(this.Environment.Tezos.Container.Network)

	this.result.Success = true
	this.result.Message = "Cleared all Networks and Containers"
	this.wg.Done()
}
