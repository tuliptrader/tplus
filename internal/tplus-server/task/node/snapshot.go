package node

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io"
	"net/http"
	"os"
	"sync"
	"time"
)

type SnapshotCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewSnapshotCheckTask(e *db.Environment) *SnapshotCheck {
	t := SnapshotCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *SnapshotCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *SnapshotCheck) GetProgress() string {
	return this.Status
}

func (this *SnapshotCheck) GetEventPrefix() string {
	return "node_snapshot_check"
}

func (this *SnapshotCheck) GetName() string {
	return "Snapshot Check"
}

func (this *SnapshotCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *SnapshotCheck) GetDescription() string {
	return "Make sure snapshot downloaded"
}

func (this *SnapshotCheck) GetId() string {
	return this.Id
}

func (this *SnapshotCheck) Run() {
	// skip if not first start, or no src specified
	if this.Environment.SnapshotSource == "" {
		fmt.Println("1")
		this.result.Success = true
		this.result.Message = "Skip snapshot import: not source specified"
		this.wg.Done()
		return
	}

	if this.Environment.Tezos.HeadLevel >= 1 {
		fmt.Println("2")
		this.result.Success = true
		this.result.Message = "Skip snapshot import: not first start"
		this.wg.Done()
		return
	}

	if _, err := os.Stat(this.Environment.Tezos.SnapshotDir + "start.snap"); os.IsNotExist(err) {
		fmt.Println("3")
		this.result.Success = false
		this.result.Message = "Snapshot does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "snapshot exists"
	}
	this.wg.Done()
}

/*
 ***********************************************************************************
 */

type SnapshotDownload struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewSnapshotDownloadTask(e *db.Environment) *SnapshotDownload {
	t := SnapshotDownload{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *SnapshotDownload) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *SnapshotDownload) GetProgress() string {
	return this.Status
}

func (this *SnapshotDownload) GetEventPrefix() string {
	return "node_snapshot_download"
}

func (this *SnapshotDownload) GetName() string {
	return "Download Snapshot"
}

func (this *SnapshotDownload) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *SnapshotDownload) GetDescription() string {
	return "Downloading Snapshot file and prepare for import"
}

func (this *SnapshotDownload) GetId() string {
	return this.Id
}

func (this *SnapshotDownload) Run() {
	err := DownloadFile(this.Environment.Tezos.SnapshotDir + "/start.snap", this.Environment.SnapshotSource)
	if err == nil {
		this.result.Success = true
		this.result.Message = "Snapshot file written."
		this.wg.Done()
	} else {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
	}
}


func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// CreatePublicNetworkNode the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

/*
 ***********************************************************************************
 */

type SnapshotImport struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}
func NewSnapshotImportTask(e *db.Environment) *SnapshotImport {
	t := SnapshotImport{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *SnapshotImport) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *SnapshotImport) GetProgress() string {
	return this.Status
}

func (this *SnapshotImport) GetEventPrefix() string {
	return "node_snapshot_import"
}

func (this *SnapshotImport) GetName() string {
	return "Import Snapshot"
}

func (this *SnapshotImport) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *SnapshotImport) GetDescription() string {
	return "Import Snapshot"
}

func (this *SnapshotImport) GetId() string {
	return this.Id
}

func (this *SnapshotImport) Run() {
	// skip if not first start, or no src specified
	if this.Environment.SnapshotSource == "" {
		this.result.Success = true
		this.result.Message = "Skip snapshot import: not source specified"
		this.wg.Done()
		return
	}

	if this.Environment.Tezos.HeadLevel >= 1 {
		this.result.Success = true
		this.result.Message = "Skip snapshot import: not first start"
		this.wg.Done()
		return
	}

	// delete in dir
	template := docker.GetSnapshotImportTemplate(this.Environment, "start.snap")


	id, err := this.Docker.ContainerCreate(template)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}


	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Import Container": string(b),
	}

	err = this.Docker.ContainerStart(id)

	if err != nil {
		this.result.Success = false
		this.result.Message = "Error starting import container"
		this.wg.Done()
		return
	}

	// wait for container exit
	for {
		time.Sleep(3)
		infoLogs, _ := this.Docker.GetContainerInfo(id)
		if infoLogs.State.Running != true {
			break
		}
	}


		this.result.Success = true
		this.result.Message = "Snapshot imported"
		this.wg.Done()
}