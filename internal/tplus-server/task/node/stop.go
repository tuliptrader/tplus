package node

import (
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type StopEnvTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewStopEnvTask(e *db.Environment) *StopEnvTask {
	t := StopEnvTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *StopEnvTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *StopEnvTask) GetProgress() string {
	return this.Status
}

func (this *StopEnvTask) GetEventPrefix() string {
	return "stop_env"
}

func (this *StopEnvTask) GetName() string {
	return "Stop Environment"
}

func (this *StopEnvTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *StopEnvTask) GetDescription() string {
	return "Environment Stop"
}

func (this *StopEnvTask) GetId() string {
	return this.Id
}

func (this *StopEnvTask) Run() {

	this.Environment.Tezos.Status = "stopping..."
	this.Environment.Tezos.Container.Status = "stopping..."
	delete(this.Environment.Consoles, "node")
	this.SaveChan <- this.Environment

	this.Docker.StopContainer(this.Environment.Tezos.Container.Id)
	this.Docker.StopContainer(this.Environment.ClientContainer.Id)

	i := 0

	for {
		i++
		time.Sleep(1 * time.Second)
		if i >= 120 {
			this.result.Success = false
			this.result.Message = "Docker failed to stop the Container within 120 seconds"
			this.wg.Done()
			break
		}

		info, err := this.Docker.GetContainerInfo(this.Environment.Tezos.Container.Id)
		if err == nil && info.State.Running == false {
			this.Environment.Tezos.Status = "stopped"
			this.Environment.Tezos.Container.Status = "stopped"
			this.SaveChan <- this.Environment
			this.result.Success = true
			this.result.Message = "Stopped container: " + this.Environment.Tezos.Container.Id
			this.wg.Done()
			break
		} else {
			fmt.Println(err)
		}
	}

	containers,_ := this.Docker.FindContainersForEnv(this.Environment.Id)

	for _,c := range containers {
		this.Docker.StopContainer(c)
	}

}
