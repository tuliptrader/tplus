package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type NetworkCheckTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNetworkCheckTask(e *db.Environment) *NetworkCheckTask {
	t := NetworkCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NetworkCheckTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NetworkCheckTask) GetProgress() string {
	return this.Status
}

func (this *NetworkCheckTask) GetEventPrefix() string {
	return "docker_network_check"
}

func (this *NetworkCheckTask) GetName() string {
	return "Check Network"
}

func (this *NetworkCheckTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NetworkCheckTask) GetDescription() string {
	return "Check if Docker Network exists"
}

func (this *NetworkCheckTask) GetId() string {
	return this.Id
}

func (this *NetworkCheckTask) Run() {

	if this.Environment.Tezos.Container.Network == "" {
		this.Environment.Tezos.Container.Network = "tplus_net_" + strings.Replace(strings.ToLower(this.Environment.Name), " ", "", -1) + "_" + db.GetRandomString(4)
		this.SaveChan <- this.Environment
	}

	res, e := this.Docker.CheckNetworkExists(this.Environment.Tezos.Container.Network)

	if e != nil {
		this.result.Success = false
		this.result.Message = e.Error()
		this.wg.Done()
		return
	}

	if res == false {
		this.result.Success = false
		this.result.Message = "Network does not exist"
		this.wg.Done()
		return
	}

	this.result.Success = true
	this.result.Message = "Network exists"
	this.wg.Done()
}

//--------------------------------------------------------------------------------------------------------------------//

type NetworkCreateTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNetworkCreateTask(e *db.Environment) *NetworkCreateTask {
	t := NetworkCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NetworkCreateTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NetworkCreateTask) GetProgress() string {
	return this.Status
}

func (this *NetworkCreateTask) GetEventPrefix() string {
	return "docker_network_create"
}

func (this *NetworkCreateTask) GetName() string {
	return "Create Network"
}

func (this *NetworkCreateTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NetworkCreateTask) GetDescription() string {
	return "Create Docker Network"
}

func (this *NetworkCreateTask) GetId() string {
	return this.Id
}

func (this *NetworkCreateTask) Run() {

	id, e := this.Docker.CreateNetwork(this.Environment.Tezos.Container.Network)

	if e != nil {
		this.result.Success = false
		this.result.Message = e.Error()
		this.wg.Done()
		return
	}

	this.Environment.Tezos.Container.NetworkID = id
	this.SaveChan <- this.Environment

	this.result.Success = true
	this.result.Message = "Network created"
	this.wg.Done()
}
