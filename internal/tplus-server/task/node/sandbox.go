package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
	"time"
)

type SandboxSetupTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewSandboxSetupTask(e *db.Environment) *SandboxSetupTask {
	t := SandboxSetupTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *SandboxSetupTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *SandboxSetupTask) GetProgress() string {
	return this.Status
}

func (this *SandboxSetupTask) GetEventPrefix() string {
	return "client_sandbox_setup"
}

func (this *SandboxSetupTask) GetName() string {
	return "Sandbox Setup"
}

func (this *SandboxSetupTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *SandboxSetupTask) GetDescription() string {
	return "Setup Client and Protocol"
}

func (this *SandboxSetupTask) GetId() string {
	return this.Id
}

func (this *SandboxSetupTask) Run() {

	time.Sleep(1*time.Second)
	if this.Environment.SandboxSetupDone {
		this.result.Success = true
		this.result.Message = "Skipped, not first setup"
		this.wg.Done()
		return
	}

	this.Environment.SandboxSetupDone = true
	this.SaveChan <- this.Environment

	client := this.Environment.ClientContainer.Id

	cmds := []string{
		"tezos-client import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6",
		"tezos-client import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh",
		"tezos-client import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo",
		"tezos-client import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ",
		"tezos-client import secret key baker unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm",
	}


	for _,c := range cmds {
		this.Docker.ExecInContainerById(client,strings.Split(c, " "),false)
	}

	if this.Environment.Protocol != "ProtoGenesis" {
		timestamp := time.Now().Add(-24*time.Hour).Format("2006-01-02T15:04:05Z")
		cmd := []string{"tezos-client","-block","genesis","activate","protocol",this.Environment.Protocol,"with","fitness","1","and","key","activator","and","parameters","/tezosconf/protocol_parameters.json","-timestamp", timestamp}
		this.Docker.ExecInContainerById(client,cmd,false)

		time.Sleep(1*time.Second)
		cmd = []string{"tezos-client","bake","for","baker"}
		this.Docker.ExecInContainerById(client,cmd,false)

		time.Sleep(1*time.Second)
		cmd = []string{"tezos-client","bake","for","baker"}
		this.Docker.ExecInContainerById(client,cmd,false)

		time.Sleep(1*time.Second)
		cmd = []string{"tezos-client","bake","for","baker"}
		this.Docker.ExecInContainerById(client,cmd,false)

		time.Sleep(1*time.Second)
		cmd = []string{"tezos-client","bake","for","baker"}
		this.Docker.ExecInContainerById(client,cmd,false)


	}

	this.result.Success = true
	this.result.Message = "Setup done"
	this.wg.Done()
	return
}
