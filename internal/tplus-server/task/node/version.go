package node

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
	"github.com/cstockton/go-conv"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type VersionFileCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewVersionFileCheck(e *db.Environment) *VersionFileCheck {
	t := VersionFileCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *VersionFileCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *VersionFileCheck) GetProgress() string {
	return this.Status
}

func (this *VersionFileCheck) GetEventPrefix() string {
	return "node_version_check"
}

func (this *VersionFileCheck) GetName() string {
	return "version.json Check"
}

func (this *VersionFileCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *VersionFileCheck) GetDescription() string {
	return "Make sure version.json file exists"
}

func (this *VersionFileCheck) GetId() string {
	return this.Id
}

func (this *VersionFileCheck) Run() {
	if _, err := os.Stat(this.Environment.Tezos.DataDir + "/version.json"); os.IsNotExist(err) {
		this.result.Success = false
		this.result.Message = "file does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "file exists"
	}
	this.wg.Done()
}

/*
 ***********************************************************************************
 */

type NodeVersionFileCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeVersionFileCreate(e *db.Environment) *NodeVersionFileCreate {
	t := NodeVersionFileCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeVersionFileCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeVersionFileCreate) GetProgress() string {
	return this.Status
}

func (this *NodeVersionFileCreate) GetEventPrefix() string {
	return "node_version_create"
}

func (this *NodeVersionFileCreate) GetName() string {
	return "version.json create"
}

func (this *NodeVersionFileCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeVersionFileCreate) GetDescription() string {
	return "Write Tezos version file"
}

func (this *NodeVersionFileCreate) GetId() string {
	return this.Id
}

func (this *NodeVersionFileCreate) Run() {

	if this.Environment.Tezos.Sandboxed {
		this.result.Success = true
		this.result.Message = "Skipped in Sandbox Mode"
		this.wg.Done()
		return
	}



	randid := db.GetRandomString(4)
	u, _ := conv.String(time.Now().UnixNano())

	res, err := this.Docker.RunTezosTask(
		"tplus_"+randid+"_version_"+u,
		"registry.gitlab.com/tuliptools/tplus:tezos",
		this.Environment.Tezos.Container.Network,
		strslice.StrSlice{""},
		strslice.StrSlice{"/bin/sh", "-c", "/usr/local/bin/tezos-node config init &&  /bin/cat ~/.tezos-node/version.json && echo \"\""},
		[]mount.Mount{},
	)
	if len(res) >= 8 {
		res = res[8:]
	}

	this.Docker.DeleteLike("tplus_" + randid + "_version_")

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(this.Environment.Tezos.DataDir+"/version.json", res, 0660)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}

	this.result.Success = true
	this.result.Message = "Version file written"
	this.result.Logs = map[string]string{
		"Version File Content": string(res),
	}
	this.wg.Done()
	return

}
