package node

import (
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"sync"
)

/*
 * Removes all chain data from the data dir and restart
 */
type RemoveEnvDataTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewRemoveEnvDataTask(e *db.Environment) *RemoveEnvDataTask {
	t := RemoveEnvDataTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *RemoveEnvDataTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *RemoveEnvDataTask) GetProgress() string {
	return this.Status
}

func (this *RemoveEnvDataTask) GetEventPrefix() string {
	return "remove_data_env"
}

func (this *RemoveEnvDataTask) GetName() string {
	return "Clean Environment"
}

func (this *RemoveEnvDataTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *RemoveEnvDataTask) GetDescription() string {
	return "Remove all chain-Data from Environment"
}

func (this *RemoveEnvDataTask) GetId() string {
	return this.Id
}

func (this *RemoveEnvDataTask) Run() {


	this.Docker.PullImageBlocking("docker.io/library/busybox:latest")
	_, _ = this.Docker.RunTask(
		"env_cleanup_"+this.Environment.Id,
		"docker.io/library/busybox:latest",
		this.Environment.Tezos.Container.NetworkID,
		strslice.StrSlice{"/bin/rm",},
		strslice.StrSlice{"-r", "/rmdata"},
		[]mount.Mount{
			{
				Type:   mount.TypeBind,
				Source: this.Config.DataDir + "/envs/" + this.Environment.Id,
				Target: "/rmdata",
			},
		},


	)

	os.RemoveAll(this.Config.DataDir + "/envs/" + this.Environment.Id)
	os.Remove(this.Config.DataDir + "/envs/" + this.Environment.Id)

	this.result.Success = true
	this.result.Message = "Cleared all Chain Data and Backups"
	this.wg.Done()
}
