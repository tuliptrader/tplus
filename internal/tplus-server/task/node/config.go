package node

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
	"bytes"
	"github.com/tyler-sommer/stick"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type NodeConfigCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeConfigCheck(e *db.Environment) *NodeConfigCheck {
	t := NodeConfigCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeConfigCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeConfigCheck) GetProgress() string {
	return this.Status
}

func (this *NodeConfigCheck) GetEventPrefix() string {
	return "node_config_check"
}

func (this *NodeConfigCheck) GetName() string {
	return "Config Check"
}

func (this *NodeConfigCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeConfigCheck) GetDescription() string {
	return "Make sure config file exists"
}

func (this *NodeConfigCheck) GetId() string {
	return this.Id
}

func (this *NodeConfigCheck) Run() {
	if _, err := os.Stat(this.Environment.Tezos.DataDir + "/config.json"); os.IsNotExist(err) {
		this.result.Success = false
		this.result.Message = "Config does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "config exists"
		bytes, _ := ioutil.ReadFile(this.Environment.Tezos.DataDir + "/config.json")
		this.result.Logs = map[string]string{
			"Config file": string(bytes),
		}
	}
	this.wg.Done()
}

/*
 ***********************************************************************************
 */

type NodeConfigCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeConfigCreate(e *db.Environment) *NodeConfigCreate {
	t := NodeConfigCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeConfigCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeConfigCreate) GetProgress() string {
	return this.Status
}

func (this *NodeConfigCreate) GetEventPrefix() string {
	return "node_config_create"
}

func (this *NodeConfigCreate) GetName() string {
	return "Config create"
}

func (this *NodeConfigCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeConfigCreate) GetDescription() string {
	return "Write Tezos config File"
}

func (this *NodeConfigCreate) GetId() string {
	return this.Id
}

func (this *NodeConfigCreate) Run() {

	filename := "./tezos/config_"+this.Environment.Tezos.Branch+".json.twig"

	file, err := this.Box.FindString(filename)
	if err != nil {
		this.result.Success = false
		this.wg.Done()
		return
	}

	vars := map[string]stick.Value{
		"privatemode":    "false",
		"historymode":    this.Environment.Tezos.HistoryMode,
		"loglevel":       "info",
		"network":        this.Environment.Tezos.Branch,
	}
	env := stick.New(nil)
	filebuffer := bytes.Buffer{}
	if err := env.Execute(file, &filebuffer, vars); err != nil {
		this.result.Success = false
		this.result.Message = "Error from templating: " + err.Error()
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(this.Environment.Tezos.DataDir+"/config.json", filebuffer.Bytes(), 0660)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}

	if this.Environment.Tezos.Branch == "sandbox" {
		file := this.Environment.SandboxConfig
		ioutil.WriteFile(this.Environment.Tezos.ConfigDir+"/sandbox.json", []byte(file),0777)

		file = this.Environment.SandboxParams
		ioutil.WriteFile(this.Environment.Tezos.ConfigDir+"/protocol_parameters.json", []byte(file),0777)
	}


	this.result.Success = true
	this.result.Message = "Config file written."
	bytes, _ := ioutil.ReadFile(this.Environment.Tezos.DataDir + "/config.json")
	this.result.Logs = map[string]string{
		"Config file": string(bytes),
	}
	this.wg.Done()
}
