package node

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ClientContainerCheckTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewClientContainerCheckTask(e *db.Environment) *ClientContainerCheckTask {
	t := ClientContainerCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ClientContainerCheckTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ClientContainerCheckTask) GetProgress() string {
	return this.Status
}

func (this *ClientContainerCheckTask) GetEventPrefix() string {
	return "client_container_check"
}

func (this *ClientContainerCheckTask) GetName() string {
	return "Client Container Check"
}

func (this *ClientContainerCheckTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ClientContainerCheckTask) GetDescription() string {
	return "Check if container exists"
}

func (this *ClientContainerCheckTask) GetId() string {
	return this.Id
}

func (this *ClientContainerCheckTask) Run() {
	if this.Environment.ClientContainer.Id == ""  {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	} else {
		id := this.Environment.ClientContainer.Id
		if len(id) <= 4 {
			this.result.Success = false
			this.result.Message = "Container does not exists"
			this.wg.Done()
			return
		}
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ClientContainerCreateTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewClientContainerCreateTask(e *db.Environment) *ClientContainerCreateTask {
	t := ClientContainerCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ClientContainerCreateTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ClientContainerCreateTask) GetProgress() string {
	return this.Status
}

func (this *ClientContainerCreateTask) GetEventPrefix() string {
	return "client_container_create"
}

func (this *ClientContainerCreateTask) GetName() string {
	return "Client Container create"
}

func (this *ClientContainerCreateTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ClientContainerCreateTask) GetDescription() string {
	return "Creating docker container"
}

func (this *ClientContainerCreateTask) GetId() string {
	return this.Id
}

func (this *ClientContainerCreateTask) Run() {

	template := docker.GetClientTemplate(this.Environment)
	id, err := this.Docker.ContainerCreate(template)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	this.Environment.ClientContainer.Id = id
	this.SaveChan <- this.Environment

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ClientContainerStartTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewClientContainerStartTask(e *db.Environment) *ClientContainerStartTask {
	t := ClientContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ClientContainerStartTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ClientContainerStartTask) GetProgress() string {
	return this.Status
}

func (this *ClientContainerStartTask) GetEventPrefix() string {
	return "client_container_start"
}

func (this *ClientContainerStartTask) GetName() string {
	return "Container Start"
}

func (this *ClientContainerStartTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ClientContainerStartTask) GetDescription() string {
	return "Start docker container"
}

func (this *ClientContainerStartTask) GetId() string {
	return this.Id
}

func (this *ClientContainerStartTask) Run() {
	time.Sleep(1*time.Second)
	if this.Environment.Consoles == nil {
		this.Environment.Consoles = map[string]db.Container{}
	}

	id := this.Environment.ClientContainer.Id

	info, err := this.Docker.GetContainerInfo(id)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		infoLogs, _ := this.Docker.GetContainerInfo(id)
		this.Environment.ClientContainer.Ips = []string{}
		for _, n := range infoLogs.NetworkSettings.Networks {
			this.Environment.ClientContainer.Ips = append(this.Environment.ClientContainer.Ips, n.IPAddress)
		}
		this.Environment.Consoles["client"] = this.Environment.ClientContainer
		this.SaveChan <- this.Environment

		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(id)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	this.Environment.ClientContainer.Status = "running"
	this.SaveChan <- this.Environment

	time.Sleep(200 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.Environment.ClientContainer.Ips = []string{}
	for _, n := range infoLogs.NetworkSettings.Networks {
		this.Environment.ClientContainer.Ips = append(this.Environment.ClientContainer.Ips, n.IPAddress)
	}

	this.Environment.Consoles["client"] = this.Environment.ClientContainer
	this.SaveChan <- this.Environment
	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}