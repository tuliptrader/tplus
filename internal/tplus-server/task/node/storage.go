package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"sync"
)

type NodeStoragePrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewNodeStoragePrepareTask(e *db.Environment) *NodeStoragePrepareTask {
	t := NodeStoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *NodeStoragePrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *NodeStoragePrepareTask) GetProgress() string {
	return this.Status
}

func (this *NodeStoragePrepareTask) GetEventPrefix() string {
	return "node_storage"
}

func (this *NodeStoragePrepareTask) GetName() string {
	return "Storage Prepare"
}

func (this *NodeStoragePrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *NodeStoragePrepareTask) GetDescription() string {
	return "Preparing filesystem for Tezos"
}

func (this *NodeStoragePrepareTask) GetId() string {
	return this.Id
}

func (this *NodeStoragePrepareTask) Run() {
	basedir := this.Config.DataDir + "/envs/" + this.Environment.Id
	nodedata := basedir + "/node"
	clientdata := basedir + "/client"
	sharedDir := basedir + "/shared"
	snapshot := basedir + "/snapshots"
	backups := basedir + "/backups"
	configs := basedir + "/configs"
	dirs := []string{basedir, nodedata, sharedDir, clientdata, backups,snapshot, configs}

	for _, dir := range dirs {
		e := os.MkdirAll(dir, 0777)
		if e == nil {
			this.result.Success = true
			this.result.Message = "Filesystem prepared"
		} else {
			this.result.Success = false
			this.result.Message = "Failed with: " + e.Error()
			this.wg.Done()
			return
		}
	}

	this.Environment.Tezos.DataDir = nodedata
	this.Environment.Tezos.SharedDir = sharedDir
	this.Environment.Tezos.ClientDataDir = clientdata
	this.Environment.Tezos.SnapshotDir = snapshot
	this.Environment.Tezos.ConfigDir = configs
	this.Environment.Tezos.BackupDir = backups
	this.SaveChan <- this.Environment
	this.wg.Done()
}
