package plugin

import (
	"bytes"
	"fmt"
	"github.com/tyler-sommer/stick"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
)

type PluginConfigPrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	service     *db.PluginService
	vars        map[string]stick.Value
}

func NewPluginConfigPrepareTask(e *db.Environment, p *db.PluginService, vars map[string]stick.Value) *PluginConfigPrepareTask {
	t := PluginConfigPrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	t.vars = vars
	return &t
}

func (this *PluginConfigPrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginConfigPrepareTask) GetProgress() string {
	return this.Status
}

func (this *PluginConfigPrepareTask) GetEventPrefix() string {
	return "plugin_configs"
}

func (this *PluginConfigPrepareTask) GetName() string {
	return "Write Config Files"
}

func (this *PluginConfigPrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginConfigPrepareTask) GetDescription() string {
	return "Write Templates/Config Files for Service " + this.service.Name
}

func (this *PluginConfigPrepareTask) GetId() string {
	return this.Id
}

func (this *PluginConfigPrepareTask) Run() {

	for key,val := range this.vars {
		this.vars[strings.ToUpper(key)] = val
	}


	for _,t := range this.service.Tempaltes {
		url := "https://gitlab.com/tuliptools/TplusPlugins/-/raw/master" + t.Source
		res,err := http.Get(url)
		if err != nil {
			fmt.Println(err.Error())
		}
		b,_ := ioutil.ReadAll(res.Body)
		stickEnv := stick.New(nil)
		filebuffer := bytes.Buffer{}
		if err := stickEnv.Execute(string(b), &filebuffer, this.vars); err == nil {
			ioutil.WriteFile(t.HostFile,filebuffer.Bytes(),0777)
		} else {
			fmt.Println(err.Error())
		}
	}

	this.result.Success = true
	this.result.Message = "Containers prepared"
	this.wg.Done()
}