package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type PluginContainerPrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerPrepareTask(e *db.Environment, p *db.PluginService) *PluginContainerPrepareTask {
	t := PluginContainerPrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (this *PluginContainerPrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginContainerPrepareTask) GetProgress() string {
	return this.Status
}

func (this *PluginContainerPrepareTask) GetEventPrefix() string {
	return "plugin_containers"
}

func (this *PluginContainerPrepareTask) GetName() string {
	return "Create containers for Plugin"
}

func (this *PluginContainerPrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginContainerPrepareTask) GetDescription() string {
	return "Preparing container for Plugin"
}

func (this *PluginContainerPrepareTask) GetId() string {
	return this.Id
}

func (this *PluginContainerPrepareTask) Run() {
	envID := strings.Replace(this.Environment.Id, "env-","",-1)
	envID = strings.ToLower(envID)
		sid := strings.ToLower(this.plugin.Name)
		sid = strings.Replace(sid, " ","",-1)
		containerName := "tplus_plugin_" + envID + "_" + sid

		id,_ := this.Docker.GetContainerId(containerName)
		info,err := this.Docker.GetContainerInfo(id)
		if err != nil || info.ID == "" {
			template := docker.GetPluginService(this.Environment,this.plugin)
			template.ContainerName = containerName
			this.Docker.PullImageBlocking(template.Image)
			this.Docker.ContainerCreate(template)
		}

	this.result.Success = true
	this.result.Message = "Containers prepared"
	this.wg.Done()
}