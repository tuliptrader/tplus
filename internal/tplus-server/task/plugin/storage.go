package plugin

import (
"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
"os"
"sync"
)

type PluginStoragePrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	service     *db.PluginService
}

func NewPluginStoragePrepareTask(e *db.Environment, p *db.PluginService) *PluginStoragePrepareTask {
	t := PluginStoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	return &t
}

func (this *PluginStoragePrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginStoragePrepareTask) GetProgress() string {
	return this.Status
}

func (this *PluginStoragePrepareTask) GetEventPrefix() string {
	return "plugin_storage"
}

func (this *PluginStoragePrepareTask) GetName() string {
	return "Storage Prepare"
}

func (this *PluginStoragePrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginStoragePrepareTask) GetDescription() string {
	return "Preparing filesystem for Plugin"
}

func (this *PluginStoragePrepareTask) GetId() string {
	return this.Id
}

func (this *PluginStoragePrepareTask) Run() {
	basedir := this.Config.DataDir + "/envs/" + this.Environment.Id
	basepPluginDir := basedir + "/plugins"
	basepPluginDir2 := basedir + "/plugins/" + this.service.Name
	configs := basedir + "/plugins/" + this.service.Name + "/configs"

	dirs := []string{basedir, basepPluginDir, basepPluginDir2, configs}

	for _,v := range this.service.Volumes {
		dirs = append(dirs, basepPluginDir2 + "/" + v.Name)
	}

	for _, dir := range dirs {
		e := os.MkdirAll(dir, 0777)
		if e == nil {
			this.result.Success = true
			this.result.Message = "Filesystem prepared"
		} else {
			this.result.Success = false
			this.result.Message = "Failed with: " + e.Error()
			this.wg.Done()
			return
		}
	}

	this.wg.Done()
}