package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type PluginContainerStartTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerStartTask(e *db.Environment, p *db.PluginService) *PluginContainerStartTask {
	t := PluginContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (this *PluginContainerStartTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginContainerStartTask) GetProgress() string {
	return this.Status
}

func (this *PluginContainerStartTask) GetEventPrefix() string {
	return "plugin_start"
}

func (this *PluginContainerStartTask) GetName() string {
	return "Start Container"
}

func (this *PluginContainerStartTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginContainerStartTask) GetDescription() string {
	return "Starting docker container"
}

func (this *PluginContainerStartTask) GetId() string {
	return this.Id
}

func (this *PluginContainerStartTask) Run() {

	envID := strings.Replace(this.Environment.Id, "env-","",-1)
	envID = strings.ToLower(envID)
		sid := strings.ToLower(this.plugin.Name)
		sid = strings.Replace(sid, " ","",-1)
		containerName := "tplus_plugin_" + envID + "_" + sid

		id,_ := this.Docker.GetContainerId(containerName)
		this.plugin.ContainerId = id
		info,err := this.Docker.GetContainerInfo(id)


		if err == nil && info.State.Running == false {
			this.Docker.ContainerStart(info.ID)
		}


	this.result.Success = true
	this.result.Message = "Containers started"
	this.wg.Done()
}
