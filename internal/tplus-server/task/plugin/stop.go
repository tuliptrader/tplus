package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type PluginContainerStopTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerStopTask(e *db.Environment, p *db.PluginService) *PluginContainerStopTask {
	t := PluginContainerStopTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (this *PluginContainerStopTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginContainerStopTask) GetProgress() string {
	return this.Status
}

func (this *PluginContainerStopTask) GetEventPrefix() string {
	return "plugin_stop"
}

func (this *PluginContainerStopTask) GetName() string {
	return "Stop Container"
}

func (this *PluginContainerStopTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginContainerStopTask) GetDescription() string {
	return "Stopping docker container"
}

func (this *PluginContainerStopTask) GetId() string {
	return this.Id
}

func (this *PluginContainerStopTask) Run() {

	envID := strings.Replace(this.Environment.Id, "env-","",-1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(this.plugin.Name)
	sid = strings.Replace(sid, " ","",-1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id,_ := this.Docker.GetContainerId(containerName)
	this.Docker.ContainerStop(id)

	this.result.Success = true
	this.result.Message = "Containers stoped"
	this.wg.Done()
}
