package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type PluginContainerRemoveTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerRemoveTask(e *db.Environment, p *db.PluginService) *PluginContainerRemoveTask {
	t := PluginContainerRemoveTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (this *PluginContainerRemoveTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginContainerRemoveTask) GetProgress() string {
	return this.Status
}

func (this *PluginContainerRemoveTask) GetEventPrefix() string {
	return "plugin_remove"
}

func (this *PluginContainerRemoveTask) GetName() string {
	return "Remove Container"
}

func (this *PluginContainerRemoveTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginContainerRemoveTask) GetDescription() string {
	return "remove docker container"
}

func (this *PluginContainerRemoveTask) GetId() string {
	return this.Id
}

func (this *PluginContainerRemoveTask) Run() {

	envID := strings.Replace(this.Environment.Id, "env-","",-1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(this.plugin.Name)
	sid = strings.Replace(sid, " ","",-1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id,_ := this.Docker.GetContainerId(containerName)
	this.Docker.ContainerStop(id)
	this.Docker.ContainerRM(id)

	this.result.Success = true
	this.result.Message = "Containers stoped"
	this.wg.Done()
}
