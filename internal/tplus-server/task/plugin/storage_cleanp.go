package plugin

import (
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"path/filepath"
	"sync"
)

type PluginStorageDeleteTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	service     *db.PluginService
}

func NewPluginStorageDeleteTask(e *db.Environment, p *db.PluginService) *PluginStorageDeleteTask {
	t := PluginStorageDeleteTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	return &t
}

func (this *PluginStorageDeleteTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *PluginStorageDeleteTask) GetProgress() string {
	return this.Status
}

func (this *PluginStorageDeleteTask) GetEventPrefix() string {
	return "plugin_storage_remove"
}

func (this *PluginStorageDeleteTask) GetName() string {
	return "Storage Delete"
}

func (this *PluginStorageDeleteTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *PluginStorageDeleteTask) GetDescription() string {
	return "Removing all files Plugin"
}

func (this *PluginStorageDeleteTask) GetId() string {
	return this.Id
}

func (this *PluginStorageDeleteTask) Run() {
	basedir := this.Config.DataDir + "/envs/" + this.Environment.Id
	basepPluginDir2 := basedir + "/plugins/" + this.service.Name

	dirs := []string{basepPluginDir2}

	this.Docker.PullImageBlocking("docker.io/library/alpine:latest")
	for _, dir := range dirs {
		_, _ = this.Docker.RunTask(
			"env_cleanup_"+this.Environment.Id,
			"docker.io/library/alpine:latest",
			this.Environment.Tezos.Container.NetworkID,
			strslice.StrSlice{"/bin/rm",},
			strslice.StrSlice{"-r", "/rmdata"},
			[]mount.Mount{
				{
					Type:   mount.TypeBind,
					Source: dir,
					Target: "/rmdata",
				},
			},
		)
		RemoveDir(dir)
	}

	this.result.Success = true
	this.result.Message = "Cleared all Data"
	this.wg.Done()
}

func RemoveDir(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	err = os.RemoveAll(dir)
	return err
}