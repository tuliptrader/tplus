package task

import (
	"github.com/gobuffalo/packr"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"sync"
	"time"
)

type TaskManager struct {
	conf               *config.Config
	db                 *db.DB
	docker             *service.DockerService
	registeredTasks    map[string]Task
	log                *logrus.Entry
	box                *packr.Box
	registeredType     map[string]string
	registeredTypeLock *sync.Mutex
}

func NewTaskManager(conf *config.Config, db *db.DB, docker *service.DockerService, log *logrus.Logger, box *packr.Box) *TaskManager {
	t := TaskManager{}
	t.conf = conf
	t.db = db
	t.docker = docker
	t.log = log.WithField("source", "TaskManager")
	t.registeredTasks = map[string]Task{}
	t.registeredType = map[string]string{}
	t.registeredTypeLock = &sync.Mutex{}
	t.box = box
	return &t
}

func (this *TaskManager) RegisterInternal(t Task) Task {
	// this.registeredTypeLock.Lock()
	// defer this.registeredTypeLock.Unlock()
	t.SetDocker(this.docker)
	t.SetConfig(this.conf)
	t.SetBox(this.box)
	this.registeredTasks[t.GetId()] = t
	this.registeredType[t.GetId()] = "internal"
	return t
}

func (this *TaskManager) RegisterUserTask(t Task) Task {
	// this.registeredTypeLock.Lock()
	// defer this.registeredTypeLock.Unlock()
	t.SetDocker(this.docker)
	t.SetConfig(this.conf)
	t.SetBox(this.box)
	this.registeredTasks[t.GetId()] = t
	this.registeredType[t.GetId()] = "user"
	return t
}

func (this *TaskManager) getResult(t Task) (*TaskResult, db.Task) {
	// this.registeredTypeLock.Lock()
	// defer this.registeredTypeLock.Unlock()
	dbE := db.Task{}
	dbE.Id = t.GetId()
	dbE.Name = t.GetName()
	dbE.Description = t.GetDescription()
	dbE.State = "pending"
	dbE.Done = false
	dbE.OnSuccess = t.GetOnSuccess()
	dbE.OnFailure = t.GetOnFail()
	dbE.Type = this.registeredType[t.GetId()]
	dbE.ObjectsUpdated = [][]string{}
	if t.GetEnvironment() != nil {
		dbE.EnvironmentId = t.GetEnvironment().Id
	}
	_ = this.db.Create(&dbE)

	go t.Run()

	dbE.State = "running"
	_ = this.db.Save(&dbE)

	ticker := time.NewTicker(1500 * time.Second)

	donechan := make(chan *TaskResult)

	var OldProgress string

	go func() {
		donechan <- t.GetResult()

	}()

	var result *TaskResult
outer:
	for {
		select {
		case model := <-t.GetSaveChan():
			_ = this.db.Save(model)
			dbE.ObjectsUpdated = append(dbE.ObjectsUpdated, []string{model.GetId(), string(model.Marshal())})

		case <-ticker.C:
			if t.GetProgress() != OldProgress {
				dbE.State = t.GetProgress()
				_ = this.db.Save(&dbE)
			}
		case res := <-donechan:
			ticker.Stop()
			dbE.Done = true
			dbE.State = t.GetProgress()
			dbE.Logs = res.Logs
			_ = this.db.Save(&dbE)
			result = res
			break outer
		}
	}

	if result.Success {
		dbE.Progress = "done"
		dbE.State = "successfull"
	} else {
		dbE.Progress = "done"
		dbE.State = "failure"
	}
	this.db.Save(&dbE)

	return result, dbE

}

func (this *TaskManager) RunAndBlock(task Task) *TaskResult {
	log := this.log.WithField("TaskID", task.GetId())
	if task.GetEnvironment() != nil {
		log = log.WithField("environment", task.GetEnvironment().GetId())
	}
	log.Info("Start Task: " + task.GetName())
	result, entity := this.getResult(task)
	if result.Success == true {
		log.Info(task.GetName() + " successful.")
		if task.GetOnSuccess() != "" {
			if next, ok := this.registeredTasks[task.GetOnSuccess()]; ok {
				log.Info(task.GetName() + " --on success--> " + next.GetName())
				result := this.RunAndBlock(next)
				if result.Success {
					entity.State = "successfull"
					_ = this.db.Save(&entity)
					return result
				} else {
					entity.State = "failed"
					_ = this.db.Save(&entity)
					log.Error("Task could not recover")
				}
			}
		}
		return result
	}
	if result.Success == false {
		log.Warn(task.GetName() + " failed")
		if task.GetOnFail() != "" {
			if next, ok := this.registeredTasks[task.GetOnFail()]; ok {
				log.Info(task.GetName() + " --recover with--> " + next.GetName())
				result := this.RunAndBlock(next)
				if result.Success {
					entity.State = "recovered"
					_ = this.db.Save(&entity)
					log.Info(task.GetName() + " recovered")

					if next, ok := this.registeredTasks[task.GetOnSuccess()]; ok {
						log.Info(task.GetName() + " --on success--> " + next.GetName())
						result := this.RunAndBlock(next)
						if result.Success {
							entity.State = "successfull"
							_ = this.db.Save(&entity)
							return result
						} else {
							entity.State = "failed"
							_ = this.db.Save(&entity)
							log.Error("Task could not recover")
						}
					}
					return result
				} else {
					entity.State = "failed"
					_ = this.db.Save(&entity)
					log.Error("Task could not recover")
					return result
				}
			}
		} else {
			entity.State = "failed"
			_ = this.db.Save(&entity)
			log.Error("Task has no OnFail, failed.")
			return result
		}
	}

	time.Sleep(10 * time.Second)
	return result
}

func (this *TaskManager) RunAndIgnore(t Task) {
	go this.RunAndBlock(t)
}
