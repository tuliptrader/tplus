package general

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type WaitTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string

	duration    time.Duration
	Name        string
	Description string
}

func NewWaitTask(duration time.Duration, name, description string) *WaitTask {
	t := WaitTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.duration = duration
	t.Name = name
	t.Description = description
	return &t
}

func (this *WaitTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *WaitTask) GetProgress() string {
	return this.Status
}

func (this *WaitTask) GetEventPrefix() string {
	return "wait"
}

func (this *WaitTask) GetName() string {
	return this.Name
}

func (this *WaitTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *WaitTask) GetDescription() string {
	return this.Description
}

func (this *WaitTask) GetId() string {
	return this.Id
}

func (this *WaitTask) Run() {
	this.result.Success = true
	this.result.Message = "waited"
	this.wg.Done()
}
