package remote
// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
"bytes"
"github.com/tyler-sommer/stick"
"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
"io/ioutil"
"os"
"sync"
)

type RemoteConfigCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewRemoteConfigCheck(e *db.Environment) *RemoteConfigCheck {
	t := RemoteConfigCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *RemoteConfigCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *RemoteConfigCheck) GetProgress() string {
	return this.Status
}

func (this *RemoteConfigCheck) GetEventPrefix() string {
	return "remote_config_check"
}

func (this *RemoteConfigCheck) GetName() string {
	return "Config Check"
}

func (this *RemoteConfigCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *RemoteConfigCheck) GetDescription() string {
	return "Make sure config file exists"
}

func (this *RemoteConfigCheck) GetId() string {
	return this.Id
}

func (this *RemoteConfigCheck) Run() {
	if _, err := os.Stat(this.Environment.Tezos.DataDir + "/config.yml"); os.IsNotExist(err) {
		this.result.Success = false
		this.result.Message = "Config does not exists"
	} else {
		this.result.Success = true
		this.result.Message = "config exists"
		bytes, _ := ioutil.ReadFile(this.Environment.Tezos.DataDir + "/config.yml")
		this.result.Logs = map[string]string{
			"Config file": string(bytes),
		}
	}
	this.wg.Done()
}

/*
 ***********************************************************************************
 */

type RemoteConfigCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewRemoteConfigCreate(e *db.Environment) *RemoteConfigCreate {
	t := RemoteConfigCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *RemoteConfigCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *RemoteConfigCreate) GetProgress() string {
	return this.Status
}

func (this *RemoteConfigCreate) GetEventPrefix() string {
	return "remote_config_create"
}

func (this *RemoteConfigCreate) GetName() string {
	return "Config create"
}

func (this *RemoteConfigCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *RemoteConfigCreate) GetDescription() string {
	return "Write Proxy config File"
}

func (this *RemoteConfigCreate) GetId() string {
	return this.Id
}

func (this *RemoteConfigCreate) Run() {

	filename := "./proxy/config.yml"

	file, err := this.Box.FindString(filename)
	if err != nil {
		this.result.Success = false
		this.wg.Done()
		return
	}

	vars := map[string]stick.Value{
		"privatemode":    "false",
		"historymode":    this.Environment.Tezos.HistoryMode,
		"loglevel":       "info",
		"network":        this.Environment.Tezos.Branch,
		"remotes" : this.Environment.Tezos.RemoteNets,
	}
	env := stick.New(nil)
	filebuffer := bytes.Buffer{}
	if err := env.Execute(file, &filebuffer, vars); err != nil {
		this.result.Success = false
		this.result.Message = "Error from templating: " + err.Error()
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(this.Environment.Tezos.DataDir+"/config.yml", filebuffer.Bytes(), 0660)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}
	

	this.result.Success = true
	this.result.Message = "Config file written."
	bytes, _ := ioutil.ReadFile(this.Environment.Tezos.DataDir + "/config.yml")
	this.result.Logs = map[string]string{
		"Config file": string(bytes),
	}
	this.wg.Done()
}

