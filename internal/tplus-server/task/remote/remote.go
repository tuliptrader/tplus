package remote

// dir := this.Config.DataDir + "/envs/" + this.Environment.Id

import (
"encoding/json"
"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
"sync"
"time"
)

type ContainerCheckTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewContainerCheckTask(e *db.Environment) *ContainerCheckTask {
	t := ContainerCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ContainerCheckTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ContainerCheckTask) GetProgress() string {
	return this.Status
}

func (this *ContainerCheckTask) GetEventPrefix() string {
	return "remote_container_check"
}

func (this *ContainerCheckTask) GetName() string {
	return "Container Check"
}

func (this *ContainerCheckTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ContainerCheckTask) GetDescription() string {
	return "Check if container file exists"
}

func (this *ContainerCheckTask) GetId() string {
	return this.Id
}

func (this *ContainerCheckTask) Run() {
	if this.Environment.Tezos.Container.Status == "" || this.Environment.Tezos.Container.Id == "" || this.Environment.Tezos.Container.Name == "" {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	} else {
		id, e := this.Docker.GetContainerId(this.Environment.Tezos.Container.Name)
		if e != nil || len(id) <= 4 {
			this.result.Success = false
			this.result.Message = "Container does not exists"
			this.wg.Done()
			return
		}
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ContainerCreateTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewContainerCreateTask(e *db.Environment) *ContainerCreateTask {
	t := ContainerCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ContainerCreateTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ContainerCreateTask) GetProgress() string {
	return this.Status
}

func (this *ContainerCreateTask) GetEventPrefix() string {
	return "remote_container_create"
}

func (this *ContainerCreateTask) GetName() string {
	return "Container create"
}

func (this *ContainerCreateTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ContainerCreateTask) GetDescription() string {
	return "Creating docker container"
}

func (this *ContainerCreateTask) GetId() string {
	return this.Id
}

func (this *ContainerCreateTask) Run() {

	if this.Environment.Tezos.Container.Name == "" {
		this.Environment.Tezos.Container.Name = "tplus_proxy_" + this.Environment.Tezos.Branch + "_" + db.GetRandomString(6)
		this.SaveChan <- this.Environment
	}

	template := docker.GetProxyTemplate(this.Environment)

	id, err := this.Docker.ContainerCreate(template)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	this.Environment.Tezos.Container.Id = id
	this.SaveChan <- this.Environment

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ContainerStartTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewContainerStartTask(e *db.Environment) *ContainerStartTask {
	t := ContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ContainerStartTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ContainerStartTask) GetProgress() string {
	return this.Status
}

func (this *ContainerStartTask) GetEventPrefix() string {
	return "node_container_start"
}

func (this *ContainerStartTask) GetName() string {
	return "Container Start"
}

func (this *ContainerStartTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ContainerStartTask) GetDescription() string {
	return "Start docker container"
}

func (this *ContainerStartTask) GetId() string {
	return this.Id
}

func (this *ContainerStartTask) Run() {
	if this.Environment.Consoles == nil {
		this.Environment.Consoles = map[string]db.Container{}
	}

	id := this.Environment.Tezos.Container.Id

	info, err := this.Docker.GetContainerInfo(id)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		infoLogs, _ := this.Docker.GetContainerInfo(id)
		this.Environment.Tezos.Container.Ips = []string{}
		for _, n := range infoLogs.NetworkSettings.Networks {
			this.Environment.Tezos.Container.Ips = append(this.Environment.Tezos.Container.Ips, n.IPAddress)
		}
		this.SaveChan <- this.Environment

		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(id)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	this.Environment.Tezos.Status = "running"
	this.Environment.Tezos.Container.Status = "running"
	this.SaveChan <- this.Environment

	time.Sleep(200 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.Environment.Tezos.Container.Ips = []string{}
	for _, n := range infoLogs.NetworkSettings.Networks {
		this.Environment.Tezos.Container.Ips = append(this.Environment.Tezos.Container.Ips, n.IPAddress)
	}

	this.SaveChan <- this.Environment
	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}

