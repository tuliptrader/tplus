package remote

import (
"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
"sync"
"time"
)

type RemoteImagePullTask struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewRemoteImagePullTask(e *db.Environment) *RemoteImagePullTask {
	t := RemoteImagePullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *RemoteImagePullTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *RemoteImagePullTask) GetProgress() string {
	return this.Status
}

func (this *RemoteImagePullTask) GetEventPrefix() string {
	return "node_pull_image"
}

func (this *RemoteImagePullTask) GetName() string {
	return "Pull docker Image"
}

func (this *RemoteImagePullTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *RemoteImagePullTask) GetDescription() string {
	return "Pulling latest Image for tezos " + this.Environment.Tezos.Branch
}

func (this *RemoteImagePullTask) GetId() string {
	return this.Id
}

func (this *RemoteImagePullTask) Run() {
	this.Status = "in Progress"
	start := time.Now()
	var err error
	logs := []byte{}
	image := "registry.gitlab.com/tuliptools/tezproxy:latest"
	tries := 0
	for {
		logs, err = this.Docker.PullImageBlocking(image)
		if err == nil {
			break
		}
		tries ++
		if tries >= 5 {
			break
		}
		time.Sleep(5*time.Second)
	}
	done := time.Now()

	this.result.Logs = map[string]string{
		"Pull Log": string(logs),
	}

	if err == nil {
		this.result.Success = true
		this.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		this.result.Success = false
		this.result.Message = "Error: " + err.Error()
	}
	this.wg.Done()
}

