package conseil

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ConseilPostgresPullTask struct {
	task.TaskBase
	Environment *db.Environment
	conseil     *db.Conseil
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewConseilPostgresPullTask(e *db.Environment, conseil *db.Conseil) *ConseilPostgresPullTask {
	t := ConseilPostgresPullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilPostgresPullTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresPullTask) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresPullTask) GetEventPrefix() string {
	return "conseil_postgres_pull"
}

func (this *ConseilPostgresPullTask) GetName() string {
	return "Pull Image (conseil DB)"
}

func (this *ConseilPostgresPullTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresPullTask) GetDescription() string {
	return "Pulling Image for conseil Postgres DB"
}

func (this *ConseilPostgresPullTask) GetId() string {
	return this.Id
}

func (this *ConseilPostgresPullTask) Run() {
	this.Status = "in Progress"
	start := time.Now()
	var err error
	logs := []byte{}
	image := "docker.io/library/postgres:12-alpine"
	logs, err = this.Docker.PullImageBlocking(image)
	done := time.Now()
	this.result.Logs = map[string]string{
		"Pull Log": string(logs),
	}
	if err == nil {
		this.result.Success = true
		this.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		this.result.Success = false
		this.result.Message = "Error: " + err.Error()
	}
	this.wg.Done()
}

type ConseilPullTask struct {
	task.TaskBase
	Environment *db.Environment
	conseil     *db.Conseil
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewConseilPullTask(e *db.Environment, conseil *db.Conseil) *ConseilPullTask {
	t := ConseilPullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilPullTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPullTask) GetProgress() string {
	return this.Status
}

func (this *ConseilPullTask) GetEventPrefix() string {
	return "conseil_image_pull"
}

func (this *ConseilPullTask) GetName() string {
	return "Pull Image (conseil)"
}

func (this *ConseilPullTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPullTask) GetDescription() string {
	return "Pull image for conseil Api + Indexer"
}

func (this *ConseilPullTask) GetId() string {
	return this.Id
}

func (this *ConseilPullTask) Run() {
	this.Status = "in Progress"
	start := time.Now()
	var err error
	logs := []byte{}
	image := "docker.io/tuliptools/conseil:latest"
	logs, err = this.Docker.PullImageBlocking(image)
	done := time.Now()
	this.result.Logs = map[string]string{
		"Pull Log": string(logs),
	}
	if err == nil {
		this.result.Success = true
		this.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		this.result.Success = false
		this.result.Message = "Error: " + err.Error()
	}
	this.wg.Done()
}
