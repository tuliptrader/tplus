package conseil

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ConseilPostgresReStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilPostgresReStartTask(e *db.Environment, c *db.Conseil) *ConseilPostgresReStart {
	t := ConseilPostgresReStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilPostgresReStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresReStart) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresReStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilPostgresReStart) GetName() string {
	return "conseil Postgres ReStart"
}

func (this *ConseilPostgresReStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresReStart) GetDescription() string {
	return "ReStart postgres container"
}

func (this *ConseilPostgresReStart) GetId() string {
	return this.Id
}

func (this *ConseilPostgresReStart) Run() {

	name := this.conseil.Id + "_conseil_postgres"

	this.Docker.ContainerStop(name)
	time.Sleep(3 * time.Second)
	err := this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container restarted"
	this.wg.Done()
	return
}

type ConseilIndexerReStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilIndexerReStartTask(e *db.Environment, c *db.Conseil) *ConseilIndexerReStart {
	t := ConseilIndexerReStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilIndexerReStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilIndexerReStart) GetProgress() string {
	return this.Status
}

func (this *ConseilIndexerReStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilIndexerReStart) GetName() string {
	return "conseil Indexer ReStart"
}

func (this *ConseilIndexerReStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilIndexerReStart) GetDescription() string {
	return "ReStart lorre container"
}

func (this *ConseilIndexerReStart) GetId() string {
	return this.Id
}

func (this *ConseilIndexerReStart) Run() {

	name := this.conseil.Id + "_conseil_indexer"

	this.Docker.ContainerStop(name)
	time.Sleep(3 * time.Second)
	err := this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container restarted"
	this.wg.Done()
	return
}

type ConseilApiReStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilApiReStartTask(e *db.Environment, c *db.Conseil) *ConseilApiReStart {
	t := ConseilApiReStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilApiReStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilApiReStart) GetProgress() string {
	return this.Status
}

func (this *ConseilApiReStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilApiReStart) GetName() string {
	return "conseil API ReStart"
}

func (this *ConseilApiReStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilApiReStart) GetDescription() string {
	return "ReStart conseil container"
}

func (this *ConseilApiReStart) GetId() string {
	return this.Id
}

func (this *ConseilApiReStart) Run() {

	name := this.conseil.Id + "_conseil_api"

	e := this.Docker.ContainerStop(name)
	fmt.Println(e)
	fmt.Println(e)
	fmt.Println(e)
	fmt.Println(e)
	time.Sleep(3 * time.Second)
	err := this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container restarted"
	this.wg.Done()
	return
}
