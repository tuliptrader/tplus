package conseil

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ConseilPostgresStop struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilPostgresStopTask(e *db.Environment, c *db.Conseil) *ConseilPostgresStop {
	t := ConseilPostgresStop{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilPostgresStop) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresStop) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresStop) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilPostgresStop) GetName() string {
	return "conseil Postgres Stop"
}

func (this *ConseilPostgresStop) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresStop) GetDescription() string {
	return "Stop postgres container"
}

func (this *ConseilPostgresStop) GetId() string {
	return this.Id
}

func (this *ConseilPostgresStop) Run() {

	name := this.conseil.Id + "_conseil_postgres"

	err := this.Docker.ContainerStop(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container stopped"
	this.wg.Done()
	return
}

type ConseilIndexerStop struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilIndexerStopTask(e *db.Environment, c *db.Conseil) *ConseilIndexerStop {
	t := ConseilIndexerStop{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilIndexerStop) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilIndexerStop) GetProgress() string {
	return this.Status
}

func (this *ConseilIndexerStop) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilIndexerStop) GetName() string {
	return "conseil Indexer Stop"
}

func (this *ConseilIndexerStop) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilIndexerStop) GetDescription() string {
	return "Stop lorre container"
}

func (this *ConseilIndexerStop) GetId() string {
	return this.Id
}

func (this *ConseilIndexerStop) Run() {

	name := this.conseil.Id + "_conseil_indexer"

	err := this.Docker.ContainerStop(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container stopped"
	this.wg.Done()
	return
}

type ConseilApiStop struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilApiStopTask(e *db.Environment, c *db.Conseil) *ConseilApiStop {
	t := ConseilApiStop{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilApiStop) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilApiStop) GetProgress() string {
	return this.Status
}

func (this *ConseilApiStop) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilApiStop) GetName() string {
	return "conseil API Stop"
}

func (this *ConseilApiStop) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilApiStop) GetDescription() string {
	return "Stop conseil container"
}

func (this *ConseilApiStop) GetId() string {
	return this.Id
}

func (this *ConseilApiStop) Run() {

	name := this.conseil.Id + "_conseil_api"

	err := this.Docker.ContainerStop(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container stopped"
	this.wg.Done()
	return
}
