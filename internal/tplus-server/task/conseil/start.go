package conseil

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ConseilPostgresStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilPostgresStartTask(e *db.Environment, c *db.Conseil) *ConseilPostgresStart {
	t := ConseilPostgresStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilPostgresStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresStart) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilPostgresStart) GetName() string {
	return "conseil Postgres Start"
}

func (this *ConseilPostgresStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresStart) GetDescription() string {
	return "Start postgres container"
}

func (this *ConseilPostgresStart) GetId() string {
	return this.Id
}

func (this *ConseilPostgresStart) Run() {

	name := this.conseil.Id + "_conseil_postgres"

	info, err := this.Docker.GetContainerInfo(name)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}

type ConseilIndexerStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilIndexerStartTask(e *db.Environment, c *db.Conseil) *ConseilIndexerStart {
	t := ConseilIndexerStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilIndexerStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilIndexerStart) GetProgress() string {
	return this.Status
}

func (this *ConseilIndexerStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilIndexerStart) GetName() string {
	return "conseil Indexer Start"
}

func (this *ConseilIndexerStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilIndexerStart) GetDescription() string {
	return "Start lorre container"
}

func (this *ConseilIndexerStart) GetId() string {
	return this.Id
}

func (this *ConseilIndexerStart) Run() {

	name := this.conseil.Id + "_conseil_indexer"

	info, err := this.Docker.GetContainerInfo(name)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}

type ConseilApiStart struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilApiStartTask(e *db.Environment, c *db.Conseil) *ConseilApiStart {
	t := ConseilApiStart{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.conseil = c
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (this *ConseilApiStart) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilApiStart) GetProgress() string {
	return this.Status
}

func (this *ConseilApiStart) GetEventPrefix() string {
	return "conseil_postgres_container_start"
}

func (this *ConseilApiStart) GetName() string {
	return "conseil API Start"
}

func (this *ConseilApiStart) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilApiStart) GetDescription() string {
	return "Start conseil container"
}

func (this *ConseilApiStart) GetId() string {
	return this.Id
}

func (this *ConseilApiStart) Run() {

	name := this.conseil.Id + "_conseil_api"

	info, err := this.Docker.GetContainerInfo(name)
	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	if info.State.Status == "running" {
		this.result.Success = true
		this.result.Message = "Container already running"
		this.wg.Done()
		return
	}
	err = this.Docker.ContainerStart(name)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(5000 * time.Millisecond)

	infoLogs, _ := this.Docker.GetContainerInfo(name)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container started"
	this.wg.Done()
	return
}
