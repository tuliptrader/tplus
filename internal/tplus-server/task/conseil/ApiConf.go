package conseil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"regexp"
	"strings"
	"sync"
	"time"
)

type ConseilUpdateApiKeysTask struct {
	task.TaskBase
	Environment *db.Environment
	conseil     *db.Conseil
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewConseilUpdateApiKeysTask(e *db.Environment, conseil *db.Conseil) *ConseilUpdateApiKeysTask {
	t := ConseilUpdateApiKeysTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilUpdateApiKeysTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilUpdateApiKeysTask) GetProgress() string {
	return this.Status
}

func (this *ConseilUpdateApiKeysTask) GetEventPrefix() string {
	return "conseil_api_keys"
}

func (this *ConseilUpdateApiKeysTask) GetName() string {
	return "Update API Keys for conseil"
}

func (this *ConseilUpdateApiKeysTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilUpdateApiKeysTask) GetDescription() string {
	return "Updating API Keys"
}

func (this *ConseilUpdateApiKeysTask) GetId() string {
	return this.Id
}

func (this *ConseilUpdateApiKeysTask) Run() {

	if len(this.conseil.ApiKeys) == 0 {
		this.conseil.ApiKeys = []string{""}
	}

	name := this.conseil.Id + "_conseil_api"

	read, writer, e := this.Docker.ExecRW(name, []string{"sh"})
	fmt.Println("writee", e)

	newstring, _ := json.Marshal(this.conseil.ApiKeys)

	// very not optimal, but works FOR NOW

	writer.Write([]byte("cat conseil.conf | grep keys:\n"))

	go func() {
		time.Sleep(500 * time.Millisecond)
		writer.Write([]byte("exit\n"))
	}()

	bytesr, _ := ioutil.ReadAll(read)

	lines := bytes.Split(bytesr, []byte("\n"))
	oldlineBytes := lines[1]
	oldline := string(oldlineBytes)

	oldescaped := regexp.QuoteMeta(string(oldline[:len(oldline)-1]))
	newescaped := strings.Replace(string(newstring), "\"", "\\\"", -1)

	replacecmd := "awk '{sub(/" + oldescaped + "/,\"    keys: " + newescaped + "\")}1' conseil.conf > tmp.conf && mv tmp.conf conseil.conf"

	read, writer, e = this.Docker.ExecRW(name, []string{"sh"})

	fmt.Println(writer.Write([]byte(replacecmd + "\n")))
	fmt.Println(writer.Write([]byte("\nexit\n")))

	this.result.Success = true
	this.result.Message = "ApiKeys updated."
	this.wg.Done()
}
