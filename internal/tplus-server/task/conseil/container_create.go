package conseil

import (
	"encoding/json"
	"fmt"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ConseilPostgresContainerCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilPostgresContainerCreateTask(e *db.Environment, conseil *db.Conseil) *ConseilPostgresContainerCreate {
	t := ConseilPostgresContainerCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilPostgresContainerCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresContainerCreate) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresContainerCreate) GetEventPrefix() string {
	return "container_create_conseil_postgres"
}

func (this *ConseilPostgresContainerCreate) GetName() string {
	return "conseil Postgres Container create"
}

func (this *ConseilPostgresContainerCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresContainerCreate) GetDescription() string {
	return "Create Container"
}

func (this *ConseilPostgresContainerCreate) GetId() string {
	return this.Id
}

func (this *ConseilPostgresContainerCreate) Run() {

	name := this.conseil.Id + "_conseil_postgres"

	image := "docker.io/library/postgres:12-alpine"
	datadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/conseil/db/data"
	schemadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/conseil/db/schema"

	id, err := this.Docker.DEFAULTCONTAINERDEPRECATED(
		name,
		image,
		this.Environment.Tezos.Container.NetworkID,
		[]mount.Mount{
			mount.Mount{
				Type:   mount.TypeBind,
				Source: datadir,
				Target: "/var/lib/postgresql/data",
			},
			mount.Mount{
				Type:   mount.TypeBind,
				Source: schemadir + "/conseil.sql",
				Target: "/docker-entrypoint-initdb.d/conseil.sql",
			},
		},
		db.Resources{
			CPUCount:  2000,
			MemoryMax: 1024000000,
		},
		[]string{
			"POSTGRES_USER=user",
			"POSTGRES_PASSWORD=password",
			"POSTGRES_DB=conseil",
			"POSTGRES_INITDB_ARGS=--nosync --lc-collate=C",
		},
	)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}

/**********************************************/

type ConseilIndexerContainerCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilIndexerContainerCreateTask(e *db.Environment, conseil *db.Conseil) *ConseilIndexerContainerCreate {
	t := ConseilIndexerContainerCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilIndexerContainerCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilIndexerContainerCreate) GetProgress() string {
	return this.Status
}

func (this *ConseilIndexerContainerCreate) GetEventPrefix() string {
	return "container_create_conseil_indexer"
}

func (this *ConseilIndexerContainerCreate) GetName() string {
	return "conseil Indexer Container create"
}

func (this *ConseilIndexerContainerCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilIndexerContainerCreate) GetDescription() string {
	return "Create Container for conseil Indexer"
}

func (this *ConseilIndexerContainerCreate) GetId() string {
	return this.Id
}

func (this *ConseilIndexerContainerCreate) Run() {

	dbcontainer, _ := this.Docker.GetContainerIP(this.conseil.Id + "_conseil_postgres")
	nodecontainer, _ := this.Docker.GetContainerIP(this.Environment.Tezos.Container.Name)

	template := docker.GetConseilTemplate(this.conseil, dbcontainer, nodecontainer)

	template.Command = strslice.StrSlice{"lorre"}
	template.NetworkAliases = []string{"conseilindexer"}
	template.ContainerName = this.conseil.Id + "_conseil_indexer"

	id, err := this.Docker.ContainerCreate(template)

	if err != nil {
		fmt.Println(err)
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}

/***********************************************/

type ConseilAPIContainerCreate struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilAPIContainerCreateTask(e *db.Environment, conseil *db.Conseil) *ConseilAPIContainerCreate {
	t := ConseilAPIContainerCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilAPIContainerCreate) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilAPIContainerCreate) GetProgress() string {
	return this.Status
}

func (this *ConseilAPIContainerCreate) GetEventPrefix() string {
	return "container_create_conseil_API"
}

func (this *ConseilAPIContainerCreate) GetName() string {
	return "conseil API Container create"
}

func (this *ConseilAPIContainerCreate) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilAPIContainerCreate) GetDescription() string {
	return "Create Container for conseil API"
}

func (this *ConseilAPIContainerCreate) GetId() string {
	return this.Id
}

func (this *ConseilAPIContainerCreate) Run() {

	dbcontainer, _ := this.Docker.GetContainerIP(this.conseil.Id + "_conseil_postgres")
	nodecontainer, _ := this.Docker.GetContainerIP(this.Environment.Tezos.Container.Name)

	template := docker.GetConseilTemplate(this.conseil, dbcontainer, nodecontainer)

	template.Command = strslice.StrSlice{"conseil"}
	template.NetworkAliases = []string{"conseil"}
	template.ContainerName = this.conseil.Id + "_conseil_api"

	id, err := this.Docker.ContainerCreate(template)

	if err != nil {
		this.result.Success = false
		this.result.Message = err.Error()
		this.wg.Done()
		return
	}

	time.Sleep(2 * time.Second)

	infoLogs, _ := this.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	this.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	this.result.Success = true
	this.result.Message = "Container created"
	this.wg.Done()
	return
}
