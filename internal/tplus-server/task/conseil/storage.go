package conseil

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type ConseilPostgresStoragePrepareTask struct {
	task.TaskBase
	Environment *db.Environment
	conseil     *db.Conseil
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
}

func NewConseilPostgresStoragePrepareTask(e *db.Environment, conseil *db.Conseil) *ConseilPostgresStoragePrepareTask {
	t := ConseilPostgresStoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilPostgresStoragePrepareTask) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresStoragePrepareTask) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresStoragePrepareTask) GetEventPrefix() string {
	return "conseil_postgres_storage"
}

func (this *ConseilPostgresStoragePrepareTask) GetName() string {
	return "Storage Prepare for conseil DB ( Postgres) "
}

func (this *ConseilPostgresStoragePrepareTask) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresStoragePrepareTask) GetDescription() string {
	return "Preparing filesystem for conseil  Postgres"
}

func (this *ConseilPostgresStoragePrepareTask) GetId() string {
	return this.Id
}

func (this *ConseilPostgresStoragePrepareTask) Run() {
	datadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/conseil/db/data"
	schemadir := this.Config.DataDir + "/plugins/" + this.Environment.Id + "/conseil/db/schema"
	os.MkdirAll(datadir, 0777)
	os.MkdirAll(schemadir, 0777)

	file, err := this.Box.FindString("./conseil.sql")
	if err != nil {
		this.result.Success = false
		this.wg.Done()
		return
	}

	err = ioutil.WriteFile(schemadir+"/conseil.sql", []byte(file), 0777)
	if err != nil {
		this.result.Success = false
		this.result.Message = "Error writing File: " + err.Error()
		this.wg.Done()
		return
	}

	this.result.Success = true
	this.result.Message = "Schmea file written."
	this.wg.Done()
}
