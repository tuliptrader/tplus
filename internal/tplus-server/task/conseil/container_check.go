package conseil

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
)

type ConseilPostgresContainerCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilPostgresContainerCheck(e *db.Environment, conseil *db.Conseil) *ConseilPostgresContainerCheck {
	t := ConseilPostgresContainerCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilPostgresContainerCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilPostgresContainerCheck) GetProgress() string {
	return this.Status
}

func (this *ConseilPostgresContainerCheck) GetEventPrefix() string {
	return "container_check_conseil_postgres"
}

func (this *ConseilPostgresContainerCheck) GetName() string {
	return "conseilPostgres Container Check"
}

func (this *ConseilPostgresContainerCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilPostgresContainerCheck) GetDescription() string {
	return "Check if container exists"
}

func (this *ConseilPostgresContainerCheck) GetId() string {
	return this.Id
}

func (this *ConseilPostgresContainerCheck) Run() {

	id, e := this.Docker.GetContainerId(this.conseil.Id + "_conseil_postgres")
	if e != nil || len(id) <= 4 {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}

/*----------------------------------------*/

type ConseilAPIContainerCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilAPIContainerCheck(e *db.Environment, conseil *db.Conseil) *ConseilAPIContainerCheck {
	t := ConseilAPIContainerCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilAPIContainerCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilAPIContainerCheck) GetProgress() string {
	return this.Status
}

func (this *ConseilAPIContainerCheck) GetEventPrefix() string {
	return "container_check_conseil_api"
}

func (this *ConseilAPIContainerCheck) GetName() string {
	return "conseilAPI Container Check"
}

func (this *ConseilAPIContainerCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilAPIContainerCheck) GetDescription() string {
	return "Check if container exists"
}

func (this *ConseilAPIContainerCheck) GetId() string {
	return this.Id
}

func (this *ConseilAPIContainerCheck) Run() {

	id, e := this.Docker.GetContainerId(this.conseil.Id + "_conseil_api")
	if e != nil || len(id) <= 4 {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}

/*----------------------------------------*/

type ConseilIndexerContainerCheck struct {
	task.TaskBase
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.TaskResult
	Status      string
	Id          string
	conseil     *db.Conseil
}

func NewConseilIndexerContainerCheck(e *db.Environment, conseil *db.Conseil) *ConseilIndexerContainerCheck {
	t := ConseilIndexerContainerCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.conseil = conseil
	return &t
}

func (this *ConseilIndexerContainerCheck) GetResult() *task.TaskResult {
	this.wg.Wait()
	return &this.result
}

func (this *ConseilIndexerContainerCheck) GetProgress() string {
	return this.Status
}

func (this *ConseilIndexerContainerCheck) GetEventPrefix() string {
	return "container_check_conseil_indexer"
}

func (this *ConseilIndexerContainerCheck) GetName() string {
	return "conseilIndexer Container Check"
}

func (this *ConseilIndexerContainerCheck) GetEnvironment() *db.Environment {
	return this.Environment
}

func (this *ConseilIndexerContainerCheck) GetDescription() string {
	return "Check if container exists"
}

func (this *ConseilIndexerContainerCheck) GetId() string {
	return this.Id
}

func (this *ConseilIndexerContainerCheck) Run() {

	id, e := this.Docker.GetContainerId(this.conseil.Id + "_conseil_indexer")
	if e != nil || len(id) <= 4 {
		this.result.Success = false
		this.result.Message = "Container does not exists"
		this.wg.Done()
		return
	}
	this.result.Success = true
	this.result.Message = "Container exists"
	this.wg.Done()
	return
}

/*----------------------------------------*/
