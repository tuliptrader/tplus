package task

import (
	"github.com/gobuffalo/packr"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"time"
)

type TaskBase struct {
	Next      string
	OnFail    string
	OnSuccess string
	SaveChan  chan (db.Model)
	Docker    *service.DockerService
	Config    *config.Config
	Box       *packr.Box
}

func (this *TaskBase) SetDocker(d *service.DockerService) {
	this.Docker = d
}

func (this *TaskBase) SetConfig(d *config.Config) {
	this.Config = d
}

func (this *TaskBase) GetNext() string {
	return this.Next
}

func (this *TaskBase) SetNext(t Task) {
	this.Next = t.GetId()
}

func (this *TaskBase) SetBox(p *packr.Box) {
	this.Box = p
}

func (this *TaskBase) SetOnFailure(t Task) {
	this.OnFail = t.GetId()
}

func (this *TaskBase) SetOnSuccess(t Task) {
	this.OnSuccess = t.GetId()
}

func (this *TaskBase) GetOnFail() string {
	return this.OnFail
}

func (this *TaskBase) GetOnSuccess() string {
	return this.OnSuccess
}

func (this *TaskBase) GetSaveChan() chan db.Model {
	return this.SaveChan
}

type Task interface {
	GetResult() *TaskResult // blocks until task is done
	GetProgress() string
	GetName() string
	GetEnvironment() *db.Environment
	GetDescription() string
	Run()
	GetId() string
	GetEventPrefix() string
	GetNext() string
	GetOnFail() string
	GetOnSuccess() string
	GetSaveChan() chan db.Model

	SetDocker(d *service.DockerService)
	SetOnSuccess(t Task)
	SetOnFailure(t Task)
	SetBox(p *packr.Box)
	SetConfig(d *config.Config)
}

type TaskResult struct {
	Success  bool
	Message  string
	duration time.Duration
	Finished time.Time
	Logs     map[string]string
}

type TaskGroup struct {
	Tasks []Task
}
