package manager

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/node"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/remote"
	"time"
)

type EnvManager struct {
	tm     *task.TaskManager
	db     *db.DB
	em     *HttpEndpointManager
	docker *service.DockerService
}

func NewEnvManager(tm *task.TaskManager, db *db.DB, em *HttpEndpointManager, d *service.DockerService) *EnvManager {
	e := EnvManager{}
	e.tm = tm
	e.db = db
	e.em = em
	e.docker = d
	go e.boot()
	return &e
}

func (this *EnvManager) boot() {
	time.Sleep(3 * time.Second)
	list, _ := this.db.GetEnvironments()

	for i, _ := range list {
		env := list[i]
		state := env.Tezos.Status
		if state == "running" || state == "pending" || state == "" {
			if env.Tezos.Sandboxed == false && env.Tezos.Remote == false {
				_ = this.CreatePublicNetworkNode(&env)
			}
			if env.Tezos.Sandboxed == true && env.Tezos.Remote == false {
				_ = this.CreateSandboxNode(&env)
			}
			if env.Tezos.Remote == true {
				_ = this.CreateRemoteNode(&env)
			}
		}
	}
}

func (this *EnvManager) CreatePublicNetworkNode(environment *db.Environment) error {
	createTask := this.GetLocalEnvironmentCreateTask(environment)
	go func() {
		this.tm.RunAndBlock(createTask)
		this.registerEndpoint(environment)
	}()
	return nil
}

func (this *EnvManager) CreateSandboxNode(environment *db.Environment) error {
	createTask := this.GetLocalSandboxCreateTask(environment)
	go func() {
		this.tm.RunAndBlock(createTask)
		this.registerEndpoint(environment)
	}()
	return nil
}


func (this *EnvManager) CreateRemoteNode(environment *db.Environment) error {
	createTask := this.GetRemoteEnvCreateTask(environment)
	go func() {
		this.tm.RunAndBlock(createTask)
		this.registerEndpoint(environment)
	}()
	return nil
}

func (this *EnvManager) Start(environment *db.Environment) error {
	this.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "running"
		update.Tezos.Status = "running"
		return update
	})
	if environment.Tezos.Sandboxed == false && environment.Tezos.Remote == false {
		_ = this.CreatePublicNetworkNode(environment)
	}
	if environment.Tezos.Sandboxed == true && environment.Tezos.Remote == false {
		_ = this.CreateSandboxNode(environment)
	}
	if environment.Tezos.Remote == true {
		_ = this.CreateRemoteNode(environment)
	}
	return nil
}

func (this *EnvManager) registerEndpoint(e *db.Environment) {
	e, _ = this.db.GetEnvironmentById(e.Id)
	if e.Tezos.Container.Status == "running" {
		name := e.Tezos.Container.Name
		ip, err := this.docker.GetContainerIP(name)
		if err == nil && e.Tezos.Endpoint == "" {
			e.Tezos.Endpoint = db.GetRandomId("endpoint")
			this.db.Save(e)
		}

		this.em.RegisterEndpoint(e.Tezos.Endpoint, "http://" +ip+":8732")
	}
}

func (this *EnvManager) Stop(environment *db.Environment) error {
	this.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "stopping..."
		update.Tezos.Status = "stopping..."
		return update
	})
	stopTask := this.tm.RegisterInternal(node.NewStopEnvTask(environment))
	this.tm.RunAndBlock(stopTask)
	this.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "stopped"
		update.Tezos.Status = "stopped"
		return update
	})
	return nil
}

func (this *EnvManager) Restart(environment *db.Environment) error {
	stop := this.tm.RegisterInternal(node.NewStopEnvTask(environment))
	this.tm.RunAndBlock(stop)
	return this.Start(environment)
}

func (this *EnvManager) Clean(environment *db.Environment) error {
	stop := this.tm.RegisterInternal(node.NewStopEnvTask(environment))
	clean := this.tm.RegisterInternal(node.NewRemoveEnvDataTask(environment))
	stop.SetOnSuccess(clean)
	this.tm.RunAndBlock(stop)
	return this.Start(environment)
}

func (this *EnvManager) Destroy(environment *db.Environment) error {
	stop := this.tm.RegisterInternal(node.NewStopEnvTask(environment))
	clean := this.tm.RegisterInternal(node.NewRemoveEnvDataTask(environment))
	remove := this.tm.RegisterInternal(node.NewRemoveEnvContainerTask(environment))
	stop.SetOnSuccess(clean)
	clean.SetOnSuccess(remove)
	this.tm.RunAndBlock(stop)
	this.db.DeleteEnvironment(environment.Id)
	return nil
}

func (this *EnvManager) GetEnvironments(user *db.User) ([]db.Environment, error) {
	res := []db.Environment{}
	list, error := this.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if env.Shared || env.CreatedByUserId == user.Id {
			res = append(res, env)
		}
	}
	return res, nil
}

func (this *EnvManager) GetPeers(user *db.User) ([]db.Environment, error) {
	res := []db.Environment{}
	list, error := this.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if env.Shared || env.CreatedByUserId == user.Id {
			res = append(res, env)
		}
	}
	return res, nil
}

func (this *EnvManager) GetEnvironment(user *db.User, id string) (db.Environment, error) {
	res := db.Environment{}
	list, error := this.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if (env.Shared || env.CreatedByUserId == user.Id) && env.Id == id {
			res = env
		}
	}
	return res, nil
}

func (this *EnvManager) GetLocalEnvironmentCreateTask(env *db.Environment) task.Task {
	dirs := this.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := this.tm.RegisterInternal(node.NewNodeImagePullTask(env))

	configCheck := this.tm.RegisterInternal(node.NewNodeConfigCheck(env))
	configCreate := this.tm.RegisterInternal(node.NewNodeConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := this.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := this.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)


	NetworkCheck := this.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := this.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)

	VersionCheck := this.tm.RegisterInternal(node.NewVersionFileCheck(env))
	VersionCreate := this.tm.RegisterInternal(node.NewNodeVersionFileCreate(env))

	VersionCheck.SetOnFailure(VersionCreate)

	IdCheck := this.tm.RegisterInternal(node.NewNodeIdentityCheckTask(env))
	IdGen := this.tm.RegisterInternal(node.NewNodeIdentityCreateTask(env))

	IdCheck.SetOnFailure(IdGen)


	SnapCheck := this.tm.RegisterInternal(node.NewSnapshotCheckTask(env))
	SnapDownload := this.tm.RegisterInternal(node.NewSnapshotDownloadTask(env))

	SnapImport := this.tm.RegisterInternal(node.NewSnapshotImportTask(env))

	SnapCheck.SetOnFailure(SnapDownload)


	ContainerCheck := this.tm.RegisterInternal(node.NewContainerCheckTask(env))
	ContainerCreate := this.tm.RegisterInternal(node.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := this.tm.RegisterInternal(node.NewContainerStartTask(env))

	ClientContainerCheck := this.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := this.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := this.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)


	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)


	SnapCheck.SetOnSuccess(SnapImport)
	SnapImport.SetOnSuccess(NetworkCheck)



	clientconfigCheck.SetOnSuccess(SnapCheck)
	configCheck.SetOnSuccess(clientconfigCheck)
	NetworkCheck.SetOnSuccess(VersionCheck)
	VersionCheck.SetOnSuccess(IdCheck)
	IdCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)


	if env.Tezos.Branch == "sandbox" {
		Sandbox := this.tm.RegisterInternal(node.NewSandboxSetupTask(env))
		ClientContainerStart.SetOnSuccess(Sandbox)
	}



	return dirs
}




func (this *EnvManager) GetLocalSandboxCreateTask(env *db.Environment) task.Task {
	dirs := this.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := this.tm.RegisterInternal(node.NewNodeImagePullTask(env))

	configCheck := this.tm.RegisterInternal(node.NewNodeConfigCheck(env))
	configCreate := this.tm.RegisterInternal(node.NewNodeConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := this.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := this.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)


	NetworkCheck := this.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := this.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)

	VersionCheck := this.tm.RegisterInternal(node.NewVersionFileCheck(env))
	VersionCreate := this.tm.RegisterInternal(node.NewNodeVersionFileCreate(env))

	VersionCheck.SetOnFailure(VersionCreate)

	IdCheck := this.tm.RegisterInternal(node.NewNodeIdentityCheckTask(env))
	IdGen := this.tm.RegisterInternal(node.NewNodeIdentityCreateTask(env))

	IdCheck.SetOnFailure(IdGen)


	ContainerCheck := this.tm.RegisterInternal(node.NewContainerCheckTask(env))
	ContainerCreate := this.tm.RegisterInternal(node.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := this.tm.RegisterInternal(node.NewContainerStartTask(env))

	ClientContainerCheck := this.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := this.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := this.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)


	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)


	clientconfigCheck.SetOnSuccess(NetworkCheck)
	configCheck.SetOnSuccess(clientconfigCheck)
	NetworkCheck.SetOnSuccess(VersionCheck)
	VersionCheck.SetOnSuccess(IdCheck)
	IdCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)

	Sandbox := this.tm.RegisterInternal(node.NewSandboxSetupTask(env))
	ClientContainerStart.SetOnSuccess(Sandbox)



	return dirs
}




func (this *EnvManager) GetRemoteEnvCreateTask(env *db.Environment) task.Task {
	dirs := this.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := this.tm.RegisterInternal(remote.NewRemoteImagePullTask(env))

	configCheck := this.tm.RegisterInternal(remote.NewRemoteConfigCheck(env))
	configCreate := this.tm.RegisterInternal(remote.NewRemoteConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := this.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := this.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)


	NetworkCheck := this.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := this.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)


	ContainerCheck := this.tm.RegisterInternal(remote.NewContainerCheckTask(env))
	ContainerCreate := this.tm.RegisterInternal(remote.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := this.tm.RegisterInternal(remote.NewContainerStartTask(env))

	ClientContainerCheck := this.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := this.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := this.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)

	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)

	clientconfigCheck.SetOnSuccess(NetworkCheck)
	configCheck.SetOnSuccess(clientconfigCheck)

	NetworkCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)

	return dirs
}
