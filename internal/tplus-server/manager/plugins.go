package manager

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/tyler-sommer/stick"
	config2 "gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/plugin"
	"gopkg.in/yaml.v2"
	"strconv"
	"strings"
	"sync"
	"time"
)

type PluginsManager struct {
	db           *db.DB
	docker       *service.DockerService
	tm           *task.TaskManager
	pluginsById map[string]*PluginManager
	pLock        *sync.Mutex
	endpoints    *HttpEndpointManager
	Box       *packr.Box
	config    *config2.Config
}

func NewPluginsManager(tm *task.TaskManager, db *db.DB, docker *service.DockerService, end *HttpEndpointManager, box *packr.Box, config *config2.Config) *PluginsManager {

	p := PluginsManager{}
	p.db = db
	p.tm = tm
	p.docker = docker
	p.pluginsById = map[string]*PluginManager{}
	p.pLock = &sync.Mutex{}
	p.endpoints = end
	p.Box = box
	p.config = config
	go p.init()
	return &p
}


func (this *PluginsManager) RegisterPlugin(env *db.Environment, pluginYML string, ) error {

	pluginYML = strings.Replace(pluginYML,"\\\"","",-1)

	vars := map[string]stick.Value{
		"tezos_privatemode":    "false",
		"tezos_historymode":    env.Tezos.HistoryMode,
		"docker_network":        env.Tezos.Branch,
		"node":			 "http://node:8732",
		"node_port":			 "8732",
		"node_hostname":			 "node",
		"node_schema":			 "http",
		"env_name": env.Name,
	}



    // register Endpoint stuff before parsing the main yml
    endpointIds := map[string]string{}
	yml := models.PluginYMLRoot{}
	yaml.Unmarshal([]byte(pluginYML),&yml)
	p := db.Plugin{}
	p.Name = yml.Name
	p.Description = yml.Description
	p.Id = db.GetRandomId("plugin")
	for i,s := range yml.Services {
		for j,e := range s.Endpoints {
			mapname := i +  "_" + strconv.Itoa(j)
			id :=  db.GetRandomId("endpoint")
			endpointIds[mapname] =id
			if e.Register != "" {
				vars[e.Register] = "http://localhost:" + strconv.Itoa(this.config.WebPort) + "/endpoints/" + id
				vars[e.Register + "_ID"] = id
				vars[e.Register + "_PATH"] =  "/endpoints/" + id + "/"
			}
		}
	}


	for key,val := range vars {
		vars[strings.ToUpper(key)] = val
	}
	stickEnv := stick.New(nil)
	filebuffer := bytes.Buffer{}
	if err := stickEnv.Execute(pluginYML, &filebuffer, vars); err == nil {

		yml := models.PluginYMLRoot{}
		yaml.Unmarshal(filebuffer.Bytes(),&yml)
		p := db.Plugin{}
		p.Name = yml.Name
		p.Description = yml.Description
		p.Id = db.GetRandomId("plugin")

		for _,e := range env.Plugins {
			if e.Name == p.Name {
				return errors.New("Plugin arleady exists")
			}
		}


		for name,s := range yml.Services {
			service :=  db.PluginService{}
			service.Name = name
			service.Image = s.Image
			service.Env = s.Env
			service.ServiceName = s.ServiceName
			service.Description = s.Description
			service.Endpoints = []db.PluginEndpoints{}
			service.Tempaltes = []db.PluginTemplate{}
			service.Volumes = []db.PluginVolume{}
			service.Entrypoint = s.Entrypoint
			service.Command = s.Command
			service.Volumes = []db.PluginVolume{}
			for i,e := range s.Endpoints {
				newE := db.PluginEndpoints{}
				newE.Name = e.Name
				newE.Id = endpointIds[name + "_" + strconv.Itoa(i)]
				newE.Proto = e.Proto
				newE.Port = e.Port
				newE.Register = e.Register
				newE.Type = e.Type
				newE.HomePath = e.HomePath
				service.Endpoints = append(service.Endpoints, newE)
			}
			for _,v := range s.Volumes {
				vol := db.PluginVolume{}
				vol.Description = v.Description
				vol.Name = v.Name
				vol.Mount = v.Mount
				vol.HostDir = this.config.DataDir + "/envs/" + env.Id + "/plugins/" + service.Name + "/" + vol.Name
				service.Volumes = append(service.Volumes, vol)
			}
			for _,t := range s.Templates {
				nt := db.PluginTemplate{}
				nt.Name = t.Name
				nt.Dest = t.Dest
				nt.Source = t.Source
				nt.HostFile = this.config.DataDir + "/envs/" + env.Id + "/plugins/" + service.Name + "/configs/" + nt.Name
				service.Tempaltes = append(service.Tempaltes, nt)
			}
			p.Services = append(p.Services, service)
		}

		if env.Plugins == nil {
			env.Plugins = []db.Plugin{}
		}

		env.Plugins = append(env.Plugins, p)
		this.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
			update.Plugins = env.Plugins
			return update
		})

	} else {
		return errors.New("Cant parse Services YML")
	}

	return nil
}


func (this *PluginsManager) getPluginManager(env *db.Environment, x *db.Plugin) *PluginManager {
	p := x
	id := p.Id

	this.pLock.Lock()
	defer this.pLock.Unlock()

	if val, ok := this.pluginsById[id]; ok {
		return val
	}

	new := PluginManager{}
	new.db = this.db
	new.docker = this.docker
	new.tm = this.tm
	new.plugin = p
	new.PluginStatus = "pending"
	new.env = env
	new.endpoints = this.endpoints
	new.canceled = false
	new.config = this.config

	go new.init()

	this.pluginsById[id] = &new
	return &new
}


func (this *PluginsManager) GetPluginsManager(id string) *PluginManager {
	if val, ok := this.pluginsById[id]; ok {
		return val
	}
	return nil
}

func (this *PluginsManager) init() {
	time.Sleep(10*time.Second)
	for {
			time.Sleep(3 * time.Second)
			envs,err := this.db.GetEnvironments()
			if err == nil {
				for _,e := range envs {
					if e.Tezos.Status == "running"{
						for _,p := range e.Plugins{
							en := e
							ep := p
							this.getPluginManager(&en,&ep)
						}
					}
				}
			}
		}
}

type PluginManager struct {
	db           *db.DB
	docker       *service.DockerService
	tm           *task.TaskManager
	PluginStatus string // status of all containers
	env          *db.Environment
	plugin       *db.Plugin
	endpoints    *HttpEndpointManager
	config       *config2.Config
	canceled bool
	stickVars map[string]stick.Value
}

func (this *PluginManager) initStick(){
	ename := this.env.Name
	ename = strings.ToLower(ename)
	this.stickVars = map[string]stick.Value{
		"tezos_privatemode":    "false",
		"tezos_historymode":    this.env.Tezos.HistoryMode,
		"docker_network":        this.env.Tezos.Branch,
		"node":			         "http://node:8732",
		"node_port":			 "8732",
		"node_hostname":		"node",
		"node_schema":			 "http",
		"rpc_endpoint":         "http://locahost:" + strconv.Itoa(this.config.WebPort) + "/endpoints/" + this.env.Tezos.Endpoint,
		"env_name": ename,
	}
}


func (this *PluginManager) GetPlugin() *db.Plugin {
	return this.plugin
}

func (this *PluginManager) init() {
	this.initStick()
	fmt.Println("staring new Manager for Plugin ", this.plugin.Name, this.plugin.Id, this.env.Id)
	go func() {
		for {
			if this.canceled {
				break
			}
			time.Sleep(5*time.Second)
			this.updateEnv()
		}
	}()
	this.updateEnv()
	this.updatePlugin()
	if this.plugin.Status != "stopped" {
		this.plugin.Status = "pending"
		this.savePlugin()
		for _,s := range this.plugin.Services {
			this.registerEndpointSticks(&s)
		}
		for i,_ := range this.plugin.Services {
			start := this.GetServiceStartTask(&this.plugin.Services[i])
			this.tm.RunAndBlock(start)
			time.Sleep(500*time.Millisecond)
			this.registerEndpoint(&this.plugin.Services[i])
		}

		this.plugin.Status = "running"
		this.savePlugin()
	}

	return
}

func (this *PluginManager) Stop(){
	this.plugin.Status = "stopped"
	this.savePlugin()
	for _,s := range this.plugin.Services {
		stop := this.GetServiceStopTask(&s)
		this.tm.RunAndBlock(stop)
		time.Sleep(500*time.Millisecond)
	}
}

func (this *PluginManager) Start(){
	this.plugin.Status = "starting...."
	this.savePlugin()
	for _,s := range this.plugin.Services {
		start := this.GetServiceOnlyStartTask(&s)
		this.tm.RunAndBlock(start)
		time.Sleep(500*time.Millisecond)
		this.registerEndpoint(&s)
	}
	this.plugin.Status = "running"
	this.savePlugin()
}

func (this *PluginManager) Delete(){
	this.plugin.Status = "removing..."
	this.savePlugin()
	for _,s := range this.plugin.Services {
		rm := this.GetServiceRemoveTask(&s)
		this.tm.RunAndBlock(rm)
		time.Sleep(500*time.Millisecond)
	}
	this.deletePlugin()
	this.canceled = true
}



func (this *PluginManager) registerEndpoint(s *db.PluginService){
	if len(s.Endpoints) == 0 {
		return
	}
	envID := strings.Replace(this.env.Id, "env-","",-1)
	envID = strings.ToLower(envID)

	sid := strings.ToLower(s.Name)
	sid = strings.Replace(sid, " ","",-1)
	containerName := "tplus_plugin_" + envID + "_" + sid
	id,_ := this.docker.GetContainerId(containerName)
	info,err := this.docker.GetContainerInfo(id)
	ip := ""
	for _,n := range info.NetworkSettings.Networks {
		ip = n.IPAddress
		break
	}
	if err == nil {
		for _,e := range s.Endpoints {
			port := strconv.Itoa(e.Port)
			if e.Type != "redirect" {
				this.endpoints.RegisterEndpoint(e.Id, "http://" + ip + ":" + port)
			} else {
				this.endpoints.RegisterRedirect(e.Id, "http://" + ip + ":" + port)
			}
		}
	} else {

	}
}


func (this *PluginManager) registerEndpointSticks(s *db.PluginService){
	if len(s.Endpoints) == 0 {
		return
	}
		for _,e := range s.Endpoints {
			if e.Register != ""{
				this.stickVars[e.Register] = "http://localhost:" + strconv.Itoa(this.config.WebPort) + "/endpoints/" + e.Id
				this.stickVars[e.Register + "_ID"] = e.Id
				this.stickVars[e.Register + "_PATH"] =  "/endpoints/" + e.Id + "/"
			}
		}
}


func (this *PluginManager) GetServiceStartTask(service *db.PluginService) task.Task{
	storage := this.tm.RegisterInternal(plugin.NewPluginStoragePrepareTask(this.env,service))
	config := this.tm.RegisterInternal(plugin.NewPluginConfigPrepareTask(this.env,service,this.stickVars))
	container := this.tm.RegisterInternal(plugin.NewPluginContainerPrepareTask(this.env, service))
	start := this.tm.RegisterInternal(plugin.NewPluginContainerStartTask(this.env, service))
	storage.SetOnSuccess(config)
	config.SetOnSuccess(container)
	container.SetOnSuccess(start)
	return storage
}

func (this *PluginManager) GetServiceOnlyStartTask(service *db.PluginService) task.Task{
	start := this.tm.RegisterInternal(plugin.NewPluginContainerStartTask(this.env, service))
	return start
}

func (this *PluginManager) GetServiceRemoveTask(service *db.PluginService) task.Task{
	stop := this.tm.RegisterInternal(plugin.NewPluginContainerRemoveTask(this.env, service))
	storage := this.tm.RegisterInternal(plugin.NewPluginStorageDeleteTask(this.env, service))
	stop.SetOnSuccess(storage)
	return stop
}

func (this *PluginManager) GetServiceStopTask(service *db.PluginService) task.Task{
	stop := this.tm.RegisterInternal(plugin.NewPluginContainerStopTask(this.env, service))
	return stop
}


func (this *PluginManager) updateEnv(){
	e,err := this.db.GetEnvironmentById(this.env.Id)
	if err == nil {
		this.env = e
	}
}

func (this *PluginManager) updatePlugin(){
	e,err := this.db.GetEnvironmentById(this.env.Id)
	if err == nil {
		for _,p := range e.Plugins {
			if p.Id == this.plugin.Id {
				x := p
				this.plugin = &x
			}
		}
	}
}

func (this *PluginManager) savePlugin(){
	this.db.UpdateEnvironment(this.env, func(update *db.Environment) *db.Environment {
		for i,op := range update.Plugins {
			if op.Name == this.plugin.Name {
				update.Plugins[i] = *this.plugin
			}
		}
		return update
	})
}
func (this *PluginManager) deletePlugin(){
	this.db.UpdateEnvironment(this.env, func(update *db.Environment) *db.Environment {
		np := []db.Plugin{}
		for _,op := range update.Plugins {
			if op.Name != this.plugin.Name {
				np = append(np, op)
			}
		}
		update.Plugins = np
		return update
	})
}