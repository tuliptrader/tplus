package manager

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"sync"
)

type HttpEndpointManager struct {
	docker    *service.DockerService
	endpoints map[string]string
	redirects map[string]string
	lock      *sync.Mutex
}

func NewEndpointManager(d *service.DockerService) *HttpEndpointManager {
	em := HttpEndpointManager{}
	em.docker = d
	em.endpoints = map[string]string{}
	em.redirects = map[string]string{}
	em.lock = &sync.Mutex{}
	go em.init()
	return &em
}

func (this *HttpEndpointManager) init() {

}

func (this *HttpEndpointManager) HandleGin(id string, c *gin.Context) {
	this.lock.Lock()
	defer this.lock.Unlock()

	if id[len(id)-1:] != "/" {
		id = id + "/"
	}
	if len(id) <= 25 {
		c.String(500, "Endpoint not ready")
		return
	}
	id = id[1:26]
	if redirect, ok := this.redirects[id]; ok {
		c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/endpoints/" + id, "", 1)
		c.Redirect(302, redirect + c.Request.RequestURI)
		return
	}

	var target string
	var ok bool
	if target, ok = this.endpoints[id]; !ok {
		c.String(404, "Endpoint not found!!")
	} else {
		giturl := target
		remote, err := url.Parse(giturl)
		if err != nil {
			panic(err)
		}
		if remote.Scheme == "https" {
			c.Request.URL.Host = remote.Host
			c.Request.URL.Scheme = remote.Scheme
			c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
			c.Request.Host = remote.Host
		}
		c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/endpoints/" + id, "", 1)
		c.Request.URL, _ = url.Parse(c.Request.RequestURI)
		proxy := httputil.NewSingleHostReverseProxy(remote)
		proxy.ModifyResponse = func(response *http.Response) error {
			response.Header.Del("Access-Control-Allow-Origin")
			return nil
		}
		proxy.ServeHTTP(c.Writer, c.Request)
	}
}

func (this *HttpEndpointManager) RegisterEndpoint(id, target string) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.endpoints[id] = target
}

func (this *HttpEndpointManager) RegisterRedirect(id, target string) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.redirects[id] = target
}


func (this *HttpEndpointManager) DeRegisterEndpoint(id string) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.endpoints, id)
}

