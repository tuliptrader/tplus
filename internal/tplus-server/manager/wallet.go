package manager

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

type WalletManager struct {
	db     *db.DB
	docker *service.DockerService
	pendingLock *sync.Mutex
	pendingOps map[string][]string
}

func NewWalletManager( db *db.DB, d *service.DockerService) *WalletManager {
	e := WalletManager{}
	e.db = db
	e.docker = d
	e.pendingLock = &sync.Mutex{}
	e.pendingOps = map[string][]string{}
	return &e
}

func (this *WalletManager) addOp(envid, msg string){
	this.pendingLock.Lock()
	defer this.pendingLock.Unlock()
	if _,ok := this.pendingOps[envid]; !ok {
		this.pendingOps[envid] = []string{msg}
	} else {
		this.pendingOps[envid] = append(this.pendingOps[envid], msg)
	}
}

func (this *WalletManager) removeOp(envid, msg string){
	this.pendingLock.Lock()
	defer this.pendingLock.Unlock()
	old := this.pendingOps[envid]
	this.pendingOps[envid] = []string{}
	for _,a := range old {
		if a != msg {
			this.pendingOps[envid] = append(this.pendingOps[envid], msg)
		}
	}
}

func (this *WalletManager) GetAddresses(env *db.Environment) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client list known contracts", " ")
	cmdRes, err := this.docker.ExecInContainerById(walletContainer, cmd, false)

	addresses := []models.WalletAddress{}
	if err != nil  {
		res.Success = false
		res.Error = "Cant get Addresses..."
		return res
	}
	addresses = this.parseAddresses(cmdRes, addresses,env)

	res.Addresses = addresses
	return res
}

func (this *WalletManager) NewAddress(env *db.Environment, name string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client gen keys " + name, " ")
	_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)

	return this.GetAddresses(env)
}

func (this *WalletManager) NewAlias(env *db.Environment, name string, address string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	if address[:2] == "KT" {
		cmd := strings.Split( "tezos-client remember contract " + name + " " + address, " ")
		_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)
	} else {
		cmd := strings.Split( "tezos-client add address " + name + " " + address, " ")
		_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)
	}


	return this.GetAddresses(env)
}

func (this *WalletManager) NewFaucet(env *db.Environment, name string, faucet []byte) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletpath := env.Tezos.ClientDataDir + "/faucet_files"
	os.Mkdir(walletpath, 0777)
	walletfile := db.GetRandomString(24) + ".json"
	ioutil.WriteFile(walletpath + "/" + walletfile, faucet,0777)

	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client activate account " + name + " with /root/.tezos-client/faucet_files/" + walletfile, " ")
	_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)

	return this.GetAddresses(env)
}

func (this *WalletManager) DeleteKey(env *db.Environment, name string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client forget address " + name + " --force", " ")
	_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)
	cmd = strings.Split( "tezos-client forget contract " + name, " ")
	_, err = this.docker.ExecInContainerById(walletContainer, cmd, false)

	return this.GetAddresses(env)
}

func (this *WalletManager) GetPendingOps(env *db.Environment) []string {
	this.pendingLock.Lock()
	defer this.pendingLock.Unlock()
	if val,ok := this.pendingOps[env.Id]; ok {
		return val
	} else {
		return []string{}
	}
}

func (this *WalletManager) Delegate(env *db.Environment, from string, to string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client set delegate for " + from + " to " + to, " ")
	go func() {
		msg := "Setting delegate for " + from + " to " + to
		this.addOp(env.Id, msg)
		this.docker.ExecInContainerById(walletContainer, cmd, false)
		this.removeOp(env.Id, msg)
	}()

	return this.GetAddresses(env)
}

func (this *WalletManager) Bake(env *db.Environment) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split( "tezos-client bake for baker", " ")
	go func() {
		this.docker.ExecInContainerById(walletContainer, cmd, false)
	}()

	return this.GetAddresses(env)
}

func (this *WalletManager) Transfer(env *db.Environment, from string, to string, amount string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info,err := this.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmdString := "transfer " + amount + " from " + from + " to " + to
	cmd := strings.Split("tezos-client " + cmdString + " --burn-cap 0.257", " ")
	go func() {
		msg := cmdString
		this.addOp(env.Id, msg)
		this.docker.ExecInContainerById(walletContainer, cmd, false)
		this.removeOp(env.Id, msg)
	}()

	return this.GetAddresses(env)
}

func (this *WalletManager) getContractInfo(ip string, address string) models.RPCContract{
	contract := models.RPCContract{}
	url := "http://" + ip + ":8732/chains/main/blocks/head/context/contracts/" + address
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	res,err := client.Get(url)
	if err != nil {
		fmt.Println(err)
		return contract
	}
	body,_ := ioutil.ReadAll(res.Body)
	json.Unmarshal(body,&contract)

	return contract
}




func (this *WalletManager) parseAddresses(cmdRes []byte, addresses []models.WalletAddress, env *db.Environment) []models.WalletAddress {
	lines := bytes.Split(cmdRes, []byte("\n"))
	for _, l := range lines {
		if bytes.Contains(l, []byte("tz1")) || bytes.Contains(l, []byte("tz2")) || bytes.Contains(l, []byte("tz3")) || bytes.Contains(l, []byte("SG")) || bytes.Contains(l, []byte("KT")) {
			e := bytes.Split(l, []byte(": "))
			addresses = append(addresses, models.WalletAddress{
				Address: string(e[1][:36]),
				Alias:   string(e[0]),
				Balance: "0",
			})
		}
	}

	ip := env.Tezos.Container.Ips[0]

	for i,a := range addresses {
		c := this.getContractInfo(ip,a.Address)
		addresses[i].Balance = c.Balance
		addresses[i].Delegate = c.Delegate
		addresses[i].Counter = c.Counter
	}

	return addresses
}