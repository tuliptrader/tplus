package manager

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// Bash Manager helps with creating a Bash Container on the Server
// for remote exec on the Client + File Sync

type BashManager struct {
	db     *db.DB
	docker *service.DockerService
	config *config.Config
	eubs   map[string]*EnvUserBash
	lock   *sync.Mutex
}

type EnvUserBash struct {
	docker        *service.DockerService
	db            *db.DB
	env           *db.Environment
	user          *db.User
	config        *config.Config
	containerName string
	datadir       string
	isReady       bool
}

func NewBashManager(c *config.Config, docker *service.DockerService, db *db.DB) *BashManager {
	b := BashManager{}
	b.config = c
	b.db = db
	b.eubs = map[string]*EnvUserBash{}
	b.lock = &sync.Mutex{}
	b.docker = docker
	return &b
}

func (this *EnvUserBash) Init() {
	// make sure container exists
	id, err := this.docker.GetContainerId(this.containerName)
	if err != nil || id == "" {
		id = this.createContainer()
		if id == "" {
			log.Print("Could not create CLI container! \n")
		}
	}
}

func (this *EnvUserBash) GetContainerID() string {
	s, _ := this.docker.GetContainerId(this.containerName)
	return s
}

func (this *EnvUserBash) GetLocalFilelist(remote db.Syncfiles) map[string]string {
	res := map[string]string{}
	dir := this.datadir
	files := []string{}
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, strings.Replace(path, dir, "", 1))
		}
		return nil
	})

	if err != nil {
		panic(err)
	}
	for _, file := range files {
		if remote.Initial && (!strings.Contains(file, ".git") && !strings.Contains(file, ".idea")) {
			if !remote.Exists(file) || !remote.HashMatch(file, this.getFileHash(dir, file)) {
				this.deleteFile(dir, file)
			} else {
				res[file] = this.getFileHash(dir, file)
			}
		} else {
			res[file] = this.getFileHash(dir, file)
		}
	}

	return res
}

func (This *EnvUserBash) deleteFile(path, name string) {
	os.Remove(path + name)
}

func (this *EnvUserBash) getFileHash(path, name string) string {
	hasher := sha256.New()
	s, err := ioutil.ReadFile(path + name)
	hasher.Write(s)
	if err != nil {
		log.Fatal(err)
	}
	return hex.EncodeToString(hasher.Sum(nil))
}

func (this *EnvUserBash) createContainer() string {
	this.docker.PullImageBlocking("docker.io/tuiptools:cli:latest")
	id, err := this.docker.GetContainerId(this.containerName)

	if err == nil && id != "" {
		_ = this.docker.ContainerStart(id)
		return id
	}

	template := docker.GetCLITemplate(this.env, this.datadir, this.containerName)
	id, err = this.docker.ContainerCreate(template)
	_ = this.docker.ContainerStart(id)

	this.env.Consoles[this.user.Username+"_bash"] = db.Container{
		Id:     id,
		Name:   this.containerName,
		Status: "running",
	}
	fmt.Println(this.db.Save(this.env))

	if err == nil {
		return id
	}

	return ""
}

func (this *BashManager) GetFor(user *db.User, env *db.Environment) *EnvUserBash {
	this.lock.Lock()
	defer this.lock.Unlock()
	if val, ok := this.eubs[user.Id+env.Id]; ok {
		return val
	} else {
		n := NewEnvUserBash(this.config, user, env, this.docker, this.db)
		this.eubs[user.Id+env.Id] = n
		return n
	}
}

func NewEnvUserBash(c *config.Config, user *db.User, env *db.Environment, docker *service.DockerService, db *db.DB) *EnvUserBash {
	eub := EnvUserBash{}
	eub.user = user
	eub.env = env
	eub.db = db
	eub.docker = docker
	eub.containerName = strings.ToLower("tplus_bash_" + cutstr(user.Username) + cutstr(env.Name))
	eub.datadir = c.DataDir + "/bash/" + eub.containerName
	eub.datadir = strings.Replace(eub.datadir, "//", "/", -1)
	os.MkdirAll(eub.datadir, 0777)
	eub.Init()
	return &eub
}

func (this *EnvUserBash) GetDataDir() string {
	return this.datadir
}

func (this *EnvUserBash) IsReady() bool {
	return this.isReady
}

func cutstr(c string) string {
	if len(c) <= 5 {
		return c
	} else {
		return c[0:5]
	}
}
