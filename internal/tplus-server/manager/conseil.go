package manager

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/conseil"
	"reflect"
	"sync"
	"time"
)

type ConseilEnvManager struct {
	tm       *task.TaskManager
	target   *db.Conseil
	log      *logrus.Entry
	Env      *db.Environment
	db       *db.DB
	endpoint *HttpEndpointManager
	docker   *service.DockerService
}

type ConseilManager struct {
	tm       *task.TaskManager
	db       *db.DB
	log      *logrus.Entry
	Conseils map[string]*ConseilEnvManager
	maplock  *sync.Mutex
	endpoint *HttpEndpointManager
	docker   *service.DockerService
}

func NewConseilManager(tm *task.TaskManager, db *db.DB, log *logrus.Logger, endpoint *HttpEndpointManager, docker *service.DockerService) *ConseilManager {
	cm := ConseilManager{}
	cm.tm = tm
	cm.db = db
	cm.log = log.WithField("source", "ConseilManager")
	go cm.run()
	cm.Conseils = map[string]*ConseilEnvManager{}
	cm.maplock = &sync.Mutex{}
	cm.endpoint = endpoint
	cm.docker = docker
	go cm.init()
	return &cm
}

func (this *ConseilManager) run() {
	time.Sleep(5 * time.Second)
	conseils, err := this.db.GetConseils()
	if err == nil {
		for _, a := range conseils {
			c := a
			this.getConseilManager(c)
		}
	}
}

func (this *ConseilManager) getConseilManager(c db.Conseil) *ConseilEnvManager {
	this.maplock.Lock()
	defer this.maplock.Unlock()
	if val, ok := this.Conseils[c.Id]; ok {
		return val
	}

	this.log.Info("Creating new Manager for conseil in env: ", c.EnvId)
	ne := ConseilEnvManager{}
	dbtarget, _ := this.db.GetConseilForEnv(c.EnvId)
	ne.target = dbtarget
	ne.tm = this.tm
	ne.log = this.log.WithField("environment", c.EnvId)
	env, _ := this.db.GetEnvironmentById(c.EnvId)
	ne.Env = env
	ne.db = this.db
	ne.endpoint = this.endpoint
	ne.docker = this.docker
	this.Conseils[c.Id] = &ne
	ne.UpdateConseil(c)
	return &ne
}

func (this *ConseilManager) init() {
	cs, _ := this.db.GetConseils()
	for _, c := range cs {
		if c.Running == true {
			this.UpdateConseil(c)
		}
	}
}
func (this *ConseilManager) UpdateConseil(c db.Conseil) {
	envm := this.getConseilManager(c)
	envm.UpdateConseil(c)
}

func (this *ConseilEnvManager) UpdateConseil(target db.Conseil) {

	if this.target.Running == false && target.Running == true {
		this.log.Info("Starting conseil")
		target.Running = false
		target.Status = "starting..."
		this.db.Save(&target)
		this.target = &target
		exec := this.getStartTask()
		this.tm.RunAndBlock(exec)
		this.target.Running = true
		this.target.Status = "ready"
		this.db.Save(&target)
		this.registerEndpoint()
		return
	}

	if !reflect.DeepEqual(this.target.ApiKeys, target.ApiKeys) {
		this.log.Info("Updating Config")
		target.Running = false
		target.Status = "updating config..."
		this.db.Save(&target)
		this.target = &target
		exec := this.getUpdateKeysTask()
		this.tm.RunAndBlock(exec)
		this.target.Running = true
		this.target.Status = "ready"
		this.registerEndpoint()
		return
	}

	if this.target.Running == true && target.Running == false {
		this.log.Info("Stopping conseil")
		target.Running = false
		target.Status = "stopping..."
		this.db.Save(&target)
		this.target = &target
		exec := this.GetStopTask()
		this.tm.RunAndBlock(exec)
		this.target.Running = false
		this.target.Status = "stopped"
		this.db.Save(&target)
		return
	}

	if this.target.Installed == false && target.Installed == true {
		this.log.Info("Installing conseil")
		target.Running = false
		target.Status = "installing..."
		this.db.Save(&target)
		this.target = &target
		exec := this.GetStopTask()
		this.tm.RunAndBlock(exec)
		this.target.Running = true
		this.target.Status = "ready"
		return
	}

	time.Sleep(3 * time.Second)

	this.registerEndpoint()

}

func (this *ConseilEnvManager) registerEndpoint() {
	if this.target.HTTPEndpoint == "" {
		this.target.HTTPEndpoint = db.GetRandomId("endpoint")
	}

	name := this.target.Id + "_conseil_api"
	ip, err := this.docker.GetContainerIP(name)
	if err == nil {
		this.endpoint.RegisterEndpoint(this.target.HTTPEndpoint, ip+":80")
		this.target.HTTPEndpointActive = true
	} else {
		this.target.HTTPEndpointActive = false
	}
	this.db.Save(this.target)
}

func (this *ConseilEnvManager) getStartTask() task.Task {
	storage := this.tm.RegisterInternal(conseil.NewConseilPostgresStoragePrepareTask(this.Env, this.target))

	pull1 := this.tm.RegisterInternal(conseil.NewConseilPostgresPullTask(this.Env, this.target))
	pull2 := this.tm.RegisterInternal(conseil.NewConseilPullTask(this.Env, this.target))

	checkdbExists := this.tm.RegisterInternal(conseil.NewConseilPostgresContainerCheck(this.Env, this.target))
	createDB := this.tm.RegisterInternal(conseil.NewConseilPostgresContainerCreateTask(this.Env, this.target))
	checkdbExists.SetOnFailure(createDB)

	dbstart := this.tm.RegisterInternal(conseil.NewConseilPostgresStartTask(this.Env, this.target))

	checkLorreExists := this.tm.RegisterInternal(conseil.NewConseilIndexerContainerCheck(this.Env, this.target))
	createLorre := this.tm.RegisterInternal(conseil.NewConseilIndexerContainerCreateTask(this.Env, this.target))
	checkLorreExists.SetOnFailure(createLorre)

	checkConseilExists := this.tm.RegisterInternal(conseil.NewConseilAPIContainerCheck(this.Env, this.target))
	createConseil := this.tm.RegisterInternal(conseil.NewConseilAPIContainerCreateTask(this.Env, this.target))
	checkConseilExists.SetOnFailure(createConseil)

	lorrestart := this.tm.RegisterInternal(conseil.NewConseilIndexerStartTask(this.Env, this.target))
	conseilstart := this.tm.RegisterInternal(conseil.NewConseilApiStartTask(this.Env, this.target))

	storage.SetOnSuccess(pull1)
	pull1.SetOnSuccess(pull2)
	pull2.SetOnSuccess(checkdbExists)

	checkdbExists.SetOnSuccess(dbstart)
	dbstart.SetOnSuccess(checkLorreExists)
	checkLorreExists.SetOnSuccess(checkConseilExists)

	checkConseilExists.SetOnSuccess(lorrestart)
	lorrestart.SetOnSuccess(conseilstart)

	return storage
}

func (this *ConseilEnvManager) getUpdateKeysTask() task.Task {
	rewrite := this.tm.RegisterInternal(conseil.NewConseilUpdateApiKeysTask(this.Env, this.target))
	restart := this.tm.RegisterInternal(conseil.NewConseilApiReStartTask(this.Env, this.target))
	rewrite.SetOnSuccess(restart)
	return rewrite
}

func (this *ConseilEnvManager) GetStopTask() task.Task {
	stop1 := this.tm.RegisterInternal(conseil.NewConseilIndexerStopTask(this.Env, this.target))
	stop2 := this.tm.RegisterInternal(conseil.NewConseilApiStopTask(this.Env, this.target))
	stop3 := this.tm.RegisterInternal(conseil.NewConseilPostgresStopTask(this.Env, this.target))

	stop1.SetOnSuccess(stop2)
	stop2.SetOnSuccess(stop3)

	return stop1
}
