package manager

import (
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/code"
	"sync"
	"time"
)

type ProjectManager struct {
	db            *db.DB
	docker        *service.DockerService
	tm            *task.TaskManager
	projects      map[string]*EnvProjectManager
	projectsByEnv map[string][]*EnvProjectManager
	pLock         *sync.Mutex
	endpoints     *HttpEndpointManager
}

func NewProjectManager(tm *task.TaskManager, db *db.DB, docker *service.DockerService, end *HttpEndpointManager) *ProjectManager {

	p := ProjectManager{}
	p.db = db
	p.tm = tm
	p.docker = docker
	p.projects = map[string]*EnvProjectManager{}
	p.projectsByEnv = map[string][]*EnvProjectManager{}
	p.pLock = &sync.Mutex{}
	p.endpoints = end
	go p.init()
	return &p
}

func (this *ProjectManager) GetProject(env *db.Environment, name string) *EnvProjectManager {
	this.pLock.Lock()
	if val, ok := this.projectsByEnv[env.Id]; ok {
		for _, p := range val {
			if p.GetProject().Name == name {
				this.pLock.Unlock()
				return p
			}
		}
	}
	this.pLock.Unlock()

	newp := db.Project{}
	newp.Name = name
	newp.Id = db.GetRandomId("project")
	newp.Created = time.Now()
	newp.LastSave = time.Now()
	newp.LastRead = time.Now()
	newp.EnvId = env.Id

	this.db.Save(&newp)

	return this.getProject(env, &newp)

}

func (this *ProjectManager) GetProjectById(env *db.Environment, id string) *EnvProjectManager {
	this.pLock.Lock()
	if val, ok := this.projectsByEnv[env.Id]; ok {
		for _, p := range val {
			if p.GetProject().Id == id {
				this.pLock.Unlock()
				return p
			}
		}
	}
	this.pLock.Unlock()
	return nil
}

func (this *ProjectManager) GetProjectsForEnv(env *db.Environment) []*EnvProjectManager {
	this.pLock.Lock()
	defer this.pLock.Unlock()
	if val, ok := this.projectsByEnv[env.Id]; ok {
		return val
	}
	return []*EnvProjectManager{}
}

func (this *ProjectManager) getProject(env *db.Environment, p *db.Project) *EnvProjectManager {
	id := env.Id + "::" + p.Name

	this.pLock.Lock()
	defer this.pLock.Unlock()

	if val, ok := this.projects[id]; ok {
		return val
	}

	new := EnvProjectManager{}
	new.db = this.db
	new.docker = this.docker
	new.tm = this.tm
	new.project = p
	new.CodeStatus = "preparing...."
	new.env = env
	new.endpoints = this.endpoints

	go new.init()

	this.projects[id] = &new
	if _, ok := this.projectsByEnv[env.Id]; ok {
		this.projectsByEnv[env.Id] = append(this.projectsByEnv[env.Id], &new)
	} else {
		this.projectsByEnv[env.Id] = []*EnvProjectManager{&new}
	}
	return &new
}

func (this *ProjectManager) init() {
	for {
		time.Sleep(1 * time.Second)
		projects, err := this.db.GetProjects()
		if err == nil {
			for _, pi := range projects {
				p := pi
				env, _ := this.db.GetEnvironmentById(p.EnvId)
				this.getProject(env, &p)
			}
		} else {
			fmt.Println(err)
		}
	}

}

type EnvProjectManager struct {
	db         *db.DB
	docker     *service.DockerService
	tm         *task.TaskManager
	CodeStatus string // status of the vscode container
	project    *db.Project
	env        *db.Environment
	endpoints  *HttpEndpointManager
}

func (this *EnvProjectManager) GetProject() *db.Project {
	return this.project
}

func (this *EnvProjectManager) init() {
	// main purpose of this is to keep the vscode container running
	// for web ui dev

	if this.project.CodeContainer.Id == "" {
		// create container
		this.CodeStatus = "creating vscode"
		this.tm.RunAndBlock(this.createContainerTask())
	}

	// make sure is running
	if this.project.CodeContainer.Status != "stopped" {
		this.tm.RunAndBlock(this.startContainerTask())
	}

	// make Endpoint

	if this.project.Endpoint == "" {
		this.project.Endpoint = db.GetRandomId("endpoint")
		this.db.Save(this.project)
	}

	target, err := this.docker.GetContainerIP(this.project.CodeContainer.Name)
	if err == nil {
		target = target + ":8080"
		this.endpoints.RegisterEndpoint(this.project.Endpoint, target)
	} else {
		fmt.Println(err)
	}

}

func (this *EnvProjectManager) createContainerTask() task.Task {
	init := this.tm.RegisterInternal(code.NewCodeContainerCreateTask(this.env, this.project))
	storage := this.tm.RegisterInternal(code.NewStoragePrepareTask(this.env, this.project))
	pull := this.tm.RegisterInternal(code.NewCodePullTask(this.env, this.project))
	check := this.tm.RegisterInternal(code.NewCodeContainerCheck(this.env, this.project))
	create := this.tm.RegisterInternal(code.NewCodeContainerCreateTask(this.env, this.project))

	init.SetOnFailure(storage)
	storage.SetOnSuccess(pull)
	pull.SetOnSuccess(check)
	check.SetOnFailure(create)

	return init
}

func (this *EnvProjectManager) startContainerTask() task.Task {
	init := this.tm.RegisterInternal(code.NewCodeStartTask(this.env, this.project))
	return init
}
