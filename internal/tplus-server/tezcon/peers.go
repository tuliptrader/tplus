package tezcon

type Peer struct {
	Id                        string
	LastDisconnection         []interface{} `json:"last_disconnection"`
	LastEstablishedConnection []interface{} `json:"last_established_connection"`
	LastMiss                  []interface{} `json:"last_miss"`
	LastSeen                  []interface{} `json:"last_seen"`
	PeerMetadata              struct {
		Advertisements struct {
			Received struct {
				Branch string `json:"branch"`
				Head   string `json:"head"`
			} `json:"received"`
			Sent struct {
				Branch string `json:"branch"`
				Head   string `json:"head"`
			} `json:"sent"`
		} `json:"advertisements"`
		FutureBlocksAdvertised string `json:"future_blocks_advertised"`
		InactiveChains         string `json:"inactive_chains"`
		OldHeads               string `json:"old_heads"`
		PrevalidatorResults    struct {
			Applied             string `json:"applied"`
			BranchDelayed       string `json:"branch_delayed"`
			BranchRefused       string `json:"branch_refused"`
			CannotDownload      string `json:"cannot_download"`
			CannotParse         string `json:"cannot_parse"`
			Duplicate           string `json:"duplicate"`
			Outdated            string `json:"outdated"`
			Refused             string `json:"refused"`
			RefusedByPostfilter string `json:"refused_by_postfilter"`
			RefusedByPrefilter  string `json:"refused_by_prefilter"`
		} `json:"prevalidator_results"`
		Requests struct {
			Failed struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"failed"`
			Received struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"received"`
			Scheduled struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"scheduled"`
			Sent struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"sent"`
		} `json:"requests"`
		Responses struct {
			Failed struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"failed"`
			Outdated string `json:"outdated"`
			Received struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"received"`
			Sent struct {
				BlockHeader             string `json:"block_header"`
				Branch                  string `json:"branch"`
				Head                    string `json:"head"`
				OperationHashesForBlock string `json:"operation_hashes_for_block"`
				Operations              string `json:"operations"`
				OperationsForBlock      string `json:"operations_for_block"`
				Other                   string `json:"other"`
				Protocols               string `json:"protocols"`
			} `json:"sent"`
			Unexpected string `json:"unexpected"`
		} `json:"responses"`
		UnactivatedChains string `json:"unactivated_chains"`
		Unadvertised      struct {
			Block      string `json:"block"`
			Operations string `json:"operations"`
			Protocol   string `json:"protocol"`
		} `json:"unadvertised"`
		ValidBlocks string `json:"valid_blocks"`
	} `json:"peer_metadata"`
	Score int `json:"score"`
	Stat  struct {
		CurrentInflow  int    `json:"current_inflow"`
		CurrentOutflow int    `json:"current_outflow"`
		TotalRecv      string `json:"total_recv"`
		TotalSent      string `json:"total_sent"`
	} `json:"stat"`
	State   string `json:"state"`
	Trusted bool   `json:"trusted"`
}
