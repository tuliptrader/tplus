package service

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
)

type UserService struct {
	db     *db.DB
	config *config.Config
}

func NewUserService(db *db.DB, conf *config.Config) *UserService {
	u := UserService{}
	u.db = db
	u.config = conf
	return &u
}

func (this *UserService) Register(username, password, invite string) error {
	if this.config.Register.Enabled == false {
		return errors.New("Registration not allowed")
	}
	u := db.User{}
	u.Username = username
	u.SetPassword(password)

	if this.config.Register.RequireInviteCode {
		i, e := this.db.GetInviteCodeById(invite)
		if i.Id == invite && e == nil && i.Used+1 <= i.MaxUsed {
			i.Used++
			this.db.Save(i)
		} else {
			return errors.New("Invalid invite code")
		}
	}
	u.Id = db.GetRandomId("user")
	return this.db.Save(&u)
}

func (this *UserService) GetToken(username, password string) (*db.Token, error) {
	var res *db.Token
	u, e := this.db.GetUserByUsername(username)
	if e != nil {
		return res, e
	}
	if !u.PasswordIsCorrect(password) {
		return res, errors.New("Password incorrect")
	}
	token := db.Token{}
	token.UserId = u.Id
	token.Id = db.GetTokenString()
	e = this.db.Create(&token)
	return &token, e
}

func (this *UserService) TokenToUser(token string) (*db.User, error) {
	tok, e := this.db.GetTokenById(token)
	if e != nil {
		return nil, e
	}
	return this.db.GetUserById(tok.UserId)
}

func (this *UserService) IsAuthed(c *gin.Context) {

}
