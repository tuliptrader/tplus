package service

import (
	"time"
)

/* returns UI IP */
func (this *DockerService) RunUI() (string, error) {
	cname := "tplus_ui"
	cimage := "registry.gitlab.com/tuliptools/tplusgui"
	id, err := this.GetContainerId(cname)
	if err != nil {
		this.PullImageBlocking(cimage)
		id, err = this.SimpleCreate(cname, cimage)
		if err != nil {
			return "", err
		}
	}
	this.ContainerStart(id)
	time.Sleep(1 * time.Second)
	return this.GetContainerIP(cname)
}