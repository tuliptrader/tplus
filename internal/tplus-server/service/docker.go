package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	network2 "github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/docker/client"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"io"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"time"
)

type DockerService struct {
	docker *client.Client
	db     *db.DB
}

func NewDockerService(db *db.DB) *DockerService {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	ds := DockerService{}
	ds.docker = cli
	ds.db = db

	// prepare a few images
	go ds.PullImageBlocking("docker.io/tuliptools/cli")

	return &ds
}

func (this *DockerService) PullImageBlocking(image string) ([]byte, error) {
	dbe, _ := this.db.GetDockerImage(image)
	ctx := context.Background()
	var res []byte
	reader, err := this.docker.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return res, err
	}
	dbe.LastPull = time.Now()
	this.db.Save(dbe)
	return ioutil.ReadAll(reader)
}

func (this *DockerService) PullImage(image string) chan error {
	res := make(chan error)
	go func() {
		_, e := this.PullImageBlocking(image)
		res <- e
	}()
	return res
}

func (this *DockerService) FindContainerWithPrefix(prefix string) ([]string, error) {
	prefix = "/" + prefix
	ctx := context.Background()
	res := []string{}
	list, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	l := len(prefix)
	for _, c := range list {
		for _, n := range c.Names {
			if len(n) >= l && n[:l] == prefix {
				res = append(res, c.ID)
			}
		}
	}
	return res, err
}

func (this *DockerService) FindContainersForEnv(envid string) ([]string, error) {
	ctx := context.Background()
	res := []string{}
	list, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	for _, c := range list {
		if val,ok := c.Labels["tplus_env"]; ok && val == envid {
			res = append(res, c.ID)
		}
	}
	return res, err
}

func (this *DockerService) Inspect(id string) (types.ContainerJSON, error) {
	ctx := context.Background()
	return this.docker.ContainerInspect(ctx, id)
}

func (this *DockerService) GetContainerStats(id string) *docker.TimedContainerStat {
	// need 2 samples to calc cpu usage
	res := docker.TimedContainerStat{}
	this.getStats(id, &res.First)
	time.Sleep(1 * time.Second)
	this.getStats(id, &res.Second)
	return &res
}

func (this *DockerService) getStats(cid string, res interface{}) {
	reader, err := this.docker.ContainerStats(context.Background(), cid, false)
	if err != nil {
		return
	}
	b, _ := ioutil.ReadAll(reader.Body)
	json.Unmarshal(b, res)
}

func (this *DockerService) GetLogs(id string, follow bool, tail string) (io.ReadCloser, error) {
	res, e := this.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: true,
		ShowStderr: true,
		Tail:       tail,
	})

	return res, e
}

func (this *DockerService) GetLogsStdin(id string, follow bool) io.ReadCloser {
	res, _ := this.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: true,
		ShowStderr: false,
	})

	return res
}

func (this *DockerService) GetLogsStderr(id string, follow bool) io.ReadCloser {
	res, _ := this.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: false,
		ShowStderr: true,
	})

	return res
}

func (this *DockerService) ExecInContainerById(id string, command []string, tty bool) ([]byte, error) {
	res := []byte{}
	ctx := context.Background()
	ex, e := this.docker.ContainerExecCreate(ctx, id, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          tty,
	})
	hr, e := this.docker.ContainerExecAttach(ctx, ex.ID, types.ExecConfig{
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
	})
	if tty {
		go func() {
			cmd := ""
			for _, s := range command {
				cmd += s + " "
			}
			io.Copy(hr.Conn, os.Stdin)
		}()
	}
	this.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	if e != nil {
		return res, e
	}
	if tty {
		io.Copy(os.Stdout, hr.Reader)
	} else {
		res, _ = ioutil.ReadAll(hr.Reader)
	}
	return res, nil
}


func (this *DockerService) ExecInContainer(containername string, command []string, tty bool) ([]byte, error) {
	res := []byte{}
	ctx := context.Background()
	id, _ := this.GetContainerId(containername)
	ex, e := this.docker.ContainerExecCreate(ctx, id, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          tty,
	})
	hr, e := this.docker.ContainerExecAttach(ctx, ex.ID, types.ExecConfig{
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
	})
	if tty {
		go func() {
			cmd := ""
			for _, s := range command {
				cmd += s + " "
			}
			io.Copy(hr.Conn, os.Stdin)
		}()
	}
	this.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	if e != nil {
		return res, e
	}
	if tty {
		io.Copy(os.Stdout, hr.Reader)
	} else {
		res, _ = ioutil.ReadAll(hr.Reader)
	}
	return res, nil
}

func (this *DockerService) ExecRW(containername string, command []string) (io.Reader, io.Writer, error) {
	id, _ := this.GetContainerId(containername)
	return this.ExecRWID(id, command)
}

func (this *DockerService) ExecRWID(ContainerID string, command []string) (io.Reader, net.Conn, error) {
	ctx := context.Background()
	ex, e := this.docker.ContainerExecCreate(ctx, ContainerID, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
		Env: []string{
			"COLUMNS=80",
			"LINES=23",
		},
	})
	if e != nil {
		return nil, nil, e
	}
	hr, e := this.docker.ContainerExecAttach(ctx, ex.ID, types.ExecConfig{
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
		Env: []string{
			"COLUMNS=80",
			"LINES=23",
		},
	})
	if e != nil {
		return nil, nil, e
	}
	go this.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	return hr.Reader, hr.Conn, nil
}

func (this *DockerService) GetContainerId(name string) (string, error) {
	ctx := context.Background()
	resp, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return "", err
	}
	for _, c := range resp {
		for _, n := range c.Names {
			if n == "/"+name {
				return c.ID, nil
			}
		}
	}
	return "", errors.New("No such Container: " + name)
}

func (this *DockerService) CheckNetworkExists(name string) (bool, error) {
	res := false
	nets, e := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if strings.ToLower(n.Name) == strings.ToLower(name) || strings.ToLower(n.ID) == strings.ToLower(name) {
			res = true
			break
		}
	}
	return res, e
}

func (this *DockerService) CreateNetwork(name string) (string, error) {
	ctx := context.Background()
	resp, err := this.docker.NetworkCreate(ctx, name, types.NetworkCreate{
		CheckDuplicate: true,
		Labels: map[string]string{
			"created_by": "tplus",
		},
	})
	ctx.Done()
	return resp.ID, err
}

func (this *DockerService) RemoveNetwork(id string) error {
	ctx := context.Background()
	err := this.docker.NetworkRemove(ctx, id)
	ctx.Done()
	return err
}

func (this *DockerService) RunTask(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount) ([]byte, error) {
	ctx := context.Background()
	cconfig := container.Config{
		Hostname:     "tplusTask",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "Tplus", "tmp": "true"},
	}
	chostc := container.HostConfig{
		DNS:    []string{"1.1.1.1", "8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	cnetc := network2.NetworkingConfig{
		EndpointsConfig: map[string]*network2.EndpointSettings{
			"default": &network2.EndpointSettings{
				NetworkID: network,
			},
		},
	}
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, &cnetc, name)
	this.ContainerStart(resp.ID)
	if err != nil {
		return []byte{}, err
	}
	ctx.Done()
	ctx = context.Background()
	time.Sleep(200 * time.Millisecond)
	logs, err := this.docker.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Follow:     true,
	})
	if err != nil {
		fmt.Print("ERROR: ", err)
		return []byte{}, nil
	}
	res, _ := ioutil.ReadAll(logs)
	_ = this.docker.ContainerRemove(context.Background(), resp.ID, types.ContainerRemoveOptions{})
	return res, nil
}

func (this *DockerService) RunTezosTask(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount) ([]byte, error) {
	ctx := context.Background()
	cconfig := container.Config{
		Hostname:     "tplusContainer",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "Tplus", "tmp": "true"},
	}
	chostc := container.HostConfig{
		DNS:    []string{"1.1.1.1", "8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	cnetc := network2.NetworkingConfig{
		EndpointsConfig: map[string]*network2.EndpointSettings{
			"default": &network2.EndpointSettings{
				NetworkID: network,
			},
		},
	}
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, &cnetc, name)
	this.ContainerStart(resp.ID)
	if err != nil {
		return []byte{}, err
	}
	ctx.Done()
	ctx = context.Background()
	time.Sleep(800 * time.Millisecond)
	logs, err := this.docker.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Follow:     true,
	})
	if err != nil {
		fmt.Print("ERROR: ", err)
		return []byte{}, nil
	}
	res, _ := ioutil.ReadAll(logs)
	_ = this.docker.ContainerRemove(context.Background(), resp.ID, types.ContainerRemoveOptions{})
	return res, nil
}

func (this *DockerService) StopContainer(id string) {
	d := 3 * time.Second
	this.docker.ContainerStop(context.Background(), id, &d)
}

func (this *DockerService) DeleteContainer(id string) {
	this.docker.ContainerRemove(context.Background(), id, types.ContainerRemoveOptions{Force: true})
}

func (this *DockerService) DeleteLike(str string) {
	list, _ := this.docker.ContainerList(context.Background(), types.ContainerListOptions{All: true})
	for _, c := range list {
		for _, n := range c.Names {
			if strings.Contains(n, str) || n == str {
				n = n[1:]
				id, err := this.GetContainerId(n)
				if err == nil {
					this.DeleteContainer(id)
				} else {
					fmt.Println(err)
				}
			}
		}
	}
}

func (this *DockerService) DeleteNetwork(str string) {
	nets, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if n.Name == str || n.ID == str {
			this.docker.NetworkRemove(context.Background(), n.ID)
		}
	}
}

func (this *DockerService) GetContainerIP(name string) (string, error) {
	id, err := this.GetContainerId(name)
	if err != nil {
		return "", err
	}
	info, err := this.Inspect(id)
	for _, e := range info.NetworkSettings.Networks {
		return e.IPAddress, nil
	}
	return "", errors.New("No networks")
}

func (this *DockerService) ContainerStart(id string) error {
	return this.docker.ContainerStart(context.Background(), id, types.ContainerStartOptions{})
}

func (this *DockerService) ContainerStop(id string) error {
	duration := 5 * time.Second
	return this.docker.ContainerStop(context.Background(), id, &duration)
}

func (this *DockerService) ContainerRM(id string) error {
	return this.docker.ContainerRemove(context.Background(), id, types.ContainerRemoveOptions{
		RemoveLinks:   true,
		RemoveVolumes: true,
		Force:         true,
	})
}

func (this *DockerService) NetworkRM(id string) error {
	return this.docker.NetworkRemove(context.Background(), id)
}

func (this *DockerService) EnsureNetworkExists(name string) (string, error) {
	ctx := context.Background()
	resp, err := this.docker.NetworkCreate(ctx, "tplus_"+name, types.NetworkCreate{
		CheckDuplicate: true,
		Labels: map[string]string{
			"created_by": "tplus",
		},
	})
	if err != nil {
		if strings.Contains(err.Error(), "already exists") {
			nets, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
			for _, n := range nets {
				if n.Name == strings.ToLower(name) {
					return n.ID, nil
				}
			}
		}
		return "", err
	}
	ctx.Done()
	return resp.ID, nil
}

func (this *DockerService) ContainerCreate(template docker.DockerTemplate) (string, error) {
	ctx := context.Background()
	envs := []string{}
	for k, v := range template.EnvVariables {
		envs = append(envs, k+"="+v)
	}
	mounts := []mount.Mount{}
	for k, v := range template.Mounts {
		mounts = append(mounts,
			mount.Mount{
				Type:   mount.TypeBind,
				Source: k,
				Target: v,
			})
	}
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          template.Command,
		Image:        template.Image,
		Entrypoint:   template.Entrypoint,
		Labels:       template.Labels,
		Env:          envs,
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(template.Resources.CPUCount)
	chostc.Resources.Memory = int64(template.Resources.MemoryMax)
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, template.ContainerName)
	if err != nil {
		return "", err

	}
	// disconnect from all nets
	info, _ := this.GetContainerInfo(resp.ID)
	list, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n, _ := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				this.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	if template.EnvironmentId != "" {
		env, err := this.db.GetEnvironmentById(template.EnvironmentId)
		if err != nil {
			return "", err
		}
		this.docker.NetworkConnect(context.Background(), env.Tezos.Container.NetworkID, resp.ID, &network2.EndpointSettings{
			Aliases: template.NetworkAliases,
		})
	}
	return resp.ID, nil
}

func (this *DockerService) ContainerCreateDEPRECETAED(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount, resources db.Resources, env []string) (string, error) {
	fmt.Println("CONTAINER CALL DEPRECATED")
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "tplus"},
		Env:          env,
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(resources.CPUCount * 1000000)
	chostc.Resources.Memory = int64(resources.MemoryMax * 1000000)
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	// disconnect from all nets
	info, _ := this.GetContainerInfo(resp.ID)
	list, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n, _ := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				this.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	this.docker.NetworkConnect(context.Background(), network, resp.ID, nil)
	return resp.ID, nil
}

func (this *DockerService) DEFAULTCONTAINERDEPRECATED(name, image, network string, mounts []mount.Mount, resources db.Resources, env []string) (string, error) {
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "node",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Image:        image,
		Labels:       map[string]string{"created_by": "tplus"},
		Env:          env,
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(resources.CPUCount * 1000000)
	chostc.Resources.Memory = int64(resources.MemoryMax * 1000000)
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	// disconnect from all nets
	info, _ := this.GetContainerInfo(resp.ID)
	list, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n, _ := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				this.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	this.docker.NetworkConnect(context.Background(), network, resp.ID, nil)
	return resp.ID, nil
}

func (this *DockerService) SimpleCreate(name, image string) (string, error) {
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "node",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Image:        image,
		Labels:       map[string]string{"created_by": "tplus"},
	}
	chostc := container.HostConfig{
		DNS: []string{"8.8.8.8", "8.8.4.4"},
	}
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	return resp.ID, nil
}

func (this *DockerService) GetContainerInfo(id string) (types.ContainerJSON, error) {
	return this.docker.ContainerInspect(context.Background(), id)
}
