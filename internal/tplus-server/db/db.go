package db

// TODO https://github.com/go-validator/validator

import (
	"errors"
	"fmt"
	"github.com/BurntSushi/locker"
	lru "github.com/hashicorp/golang-lru"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	bolt "go.etcd.io/bbolt"
	"os"
	"sort"
	"strings"
	"time"
)

func NewBoltDb(config *config.Config) *bolt.DB {
	file := config.DataDir + "/db/bolt.db"
	os.MkdirAll(config.DataDir+"/db", 0777)
	boltdb, err := bolt.Open(file, 0777, &bolt.Options{Timeout: 100 * time.Millisecond})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return boltdb
}

type DB struct {
	StorageDir string
	bolt       *bolt.DB
	postsave   []func(string, []byte)
	locks      *locker.Locker
	lru        *lru.Cache
}

func NewDB(boltdb *bolt.DB) *DB {
	d := DB{}
	d.bolt = boltdb
	d.locks = locker.NewLocker()
	d.lru, _ = lru.New(2048)

	_ = d.bolt.Update(func(tx *bolt.Tx) error {
		_, _ = tx.CreateBucket([]byte("Task"))
		_, _ = tx.CreateBucket([]byte("InviteCode"))
		_, _ = tx.CreateBucket([]byte("Environment"))
		_, _ = tx.CreateBucket([]byte("User"))
		_, _ = tx.CreateBucket([]byte("Token"))
		_, _ = tx.CreateBucket([]byte("DockerImage"))
		_, _ = tx.CreateBucket([]byte("Conseil"))
		_, _ = tx.CreateBucket([]byte("Arronax"))
		_, _ = tx.CreateBucket([]byte("Project"))

		return nil
	})

	return &d
}

func (this *DB) Create(obj Model) error {
	this.lru.Add(obj.GetId(), obj.Marshal())
	obj.SetCreatedNow()
	obj.setLastSaveNow()
	obj.setLastReadNow()
	return this.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		err := b.Put([]byte(obj.GetId()), obj.Marshal())
		return err
	})
}

func (this *DB) RegisterPostSaveHook(h func(string, []byte)) {
	this.postsave = append(this.postsave, h)
}

func (this *DB) Save(obj Model) error {
	this.lru.Add(obj.GetId(), obj.Marshal())
	obj.setLastSaveNow()
	return this.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		err := b.Put([]byte(obj.GetId()), obj.Marshal())
		for _, h := range this.postsave {
			h(obj.GetId(), obj.Marshal())
		}
		return err
	})
}

func (this *DB) GetTasksForEnvironment(envid string) ([]Task, error) {
	res := []Task{}
	var e error
	tasks, e := this.GetTasks()
	for _, t := range tasks {
		if strings.ToLower(t.EnvironmentId) == strings.ToLower(envid) && t.State != "registered" {
			res = append(res, t)
		}
	}

	if e != nil {
		return res, e
	}

	sort.Slice(res, func(i, j int) bool {
		return res[j].Created.Before(res[i].Created)
	})

	return res, e
}

func (this *DB) GetUserByUsername(name string) (*User, error) {
	res := User{}
	var e error
	users, e := this.GetUsers()
	for _, u := range users {
		if strings.ToLower(u.Username) == strings.ToLower(name) {
			res = u
			break
		}
	}

	if e == nil && res.Username == "" {
		e = errors.New("Username not found")
	}

	return &res, e
}

func (this *DB) Delete(obj Model) error {
	return this.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		return b.Delete([]byte(obj.GetId()))
	})
}

func (this *DB) GetDockerImage(id string) (*DockerImage, error) {
	res := DockerImage{}
	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("DockerImage"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		res.Id = id
		this.Save(&res)
	}
	return &res, e
}

func (this *DB) DeleteEnvironment(envid string) error {
	e := this.bolt.Update(func(tx *bolt.Tx) error {
		tx.Bucket([]byte("Environment")).Delete([]byte(envid))
		return nil
	})
	return e
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generated methods

// generated for Environment

func (this *DB) UpdateEnvironment(val *Environment, f func(update *Environment) *Environment) {
	old, e := this.GetEnvironmentById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Environment

func (this *DB) GetEnvironments() ([]Environment, error) {
	res := []Environment{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Environment"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Environment{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetEnvironmentById(id string) (*Environment, error) {
	res := Environment{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Environment"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateInviteCode(val *InviteCode, f func(update *InviteCode) *InviteCode) {
	old, e := this.GetInviteCodeById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for InviteCode

func (this *DB) GetInviteCodes() ([]InviteCode, error) {
	res := []InviteCode{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("InviteCode"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := InviteCode{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetInviteCodeById(id string) (*InviteCode, error) {
	res := InviteCode{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("InviteCode"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateUser(val *User, f func(update *User) *User) {
	old, e := this.GetUserById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for User

func (this *DB) GetUsers() ([]User, error) {
	res := []User{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("User"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := User{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetUserById(id string) (*User, error) {
	res := User{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("User"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateToken(val *Token, f func(update *Token) *Token) {
	old, e := this.GetTokenById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Token

func (this *DB) GetTokens() ([]Token, error) {
	res := []Token{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Token"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Token{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetTokenById(id string) (*Token, error) {
	res := Token{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Token"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateTask(val *Task, f func(update *Task) *Task) {
	old, e := this.GetTaskById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Task

func (this *DB) GetTasks() ([]Task, error) {
	res := []Task{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Task"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Task{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetTaskById(id string) (*Task, error) {
	res := Task{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Task"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateConseil(val *Conseil, f func(update *Conseil) *Conseil) {
	old, e := this.GetConseilById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Conseil

func (this *DB) GetConseils() ([]Conseil, error) {
	res := []Conseil{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Conseil"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Conseil{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetConseilById(id string) (*Conseil, error) {
	res := Conseil{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Conseil"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateArronax(val *Arronax, f func(update *Arronax) *Arronax) {
	old, e := this.GetArronaxById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Arronax

func (this *DB) GetArronaxs() ([]Arronax, error) {
	res := []Arronax{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Arronax"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Arronax{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetArronaxById(id string) (*Arronax, error) {
	res := Arronax{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Arronax"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}

// generated for Environment

func (this *DB) UpdateProject(val *Project, f func(update *Project) *Project) {
	old, e := this.GetProjectById(val.GetId())
	if e == nil {
		this.locks.Lock(val.GetId())
		defer this.locks.Unlock(val.GetId())
		n := f(old)
		this.Save(n)
	}
}

// generated for Project

func (this *DB) GetProjects() ([]Project, error) {
	res := []Project{}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Project"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Project{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res, e
}

func (this *DB) GetProjectById(id string) (*Project, error) {
	res := Project{}

	if val, ok := this.lru.Get(id); ok {
		res.UnMarshal(val.([]byte))
		return &res, nil
	}

	e := this.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Project"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		this.Save(&res)
	}
	return &res, e
}
