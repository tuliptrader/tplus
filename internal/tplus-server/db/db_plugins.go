package db

func (this *DB) GetConseilForEnv(envid string) (*Conseil, error) {
	list, _ := this.GetConseils()
	for _, l := range list {
		if l.EnvId == envid {
			return &l, nil
		}
	}

	new := Conseil{}
	new.EnvId = envid
	new.Id = GetRandomId("conseil")
	this.Save(&new)
	return &new, nil
}

func (this *DB) GetArronaxForEnv(envid string) (*Arronax, error) {
	list, _ := this.GetArronaxs()

	for _, l := range list {
		if l.EnvId == envid {
			return &l, nil
		}
	}

	new := Arronax{}
	new.EnvId = envid
	new.Id = GetRandomId("arronax")
	this.Save(&new)
	return &new, nil
}
