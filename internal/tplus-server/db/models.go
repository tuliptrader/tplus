package db

/*
 Codegened file, do not edit directly
*/

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type ModelBase struct {
	Created  time.Time
	LastRead time.Time
	LastSave time.Time
}

type SyncFile struct {
	Name string
	Hash string
}

type Syncfiles struct {
	EnvId   string
	Files   []SyncFile
	Initial bool
}

func (this *Syncfiles) Exists(file string) bool {
	for _, a := range this.Files {
		if a.Name == file {
			return true
		}
	}
	return false
}

func (this *Syncfiles) HashMatch(file, hash string) bool {
	for _, a := range this.Files {
		if a.Hash == hash && a.Name == file {
			return true
		}
	}
	return false
}

type SyncResponse struct {
	Existing []SyncFile
}

type Model interface {
	SetCreatedNow()
	setLastReadNow()
	setLastSaveNow()
	Marshal() []byte
	GetId() string
	GetBucket() []byte
}

type PluginBase struct {
	Id                 string
	EnvId              string
	Installed          bool
	Enabled            bool
	HTTPEndpoint       string
	HTTPEndpointActive bool
	Containers         []Container
}

type Conseil struct {
	PluginBase
	ModelBase
	ApiKeys []string
	Running bool
	Status  string
}

type Arronax struct {
	PluginBase
	ModelBase
}

type Project struct {
	ModelBase
	Id            string
	CodeContainer Container
	EnvId         string
	Name          string
	Endpoint      string
}

type Environment struct {
	Id string
	ModelBase
	Name            string
	Tezos           TezosNode
	ClientContainer Container
	CreatedByUserId string
	Shared          bool
	Consoles        map[string]Container
	SnapshotSource string
	BackupSource   string
	Error          string
	SandboxParams  string
	SandboxConfig  string
	SandboxSetupDone bool
	Protocol string
	Plugins []Plugin
}


type DockerImage struct {
	Id string
	ModelBase
	LastPull time.Time
}

type TezosNode struct {
	Remote        bool
	RemoteNets    []string
	Sandboxed     bool
	Branch        string
	Status        string
	HeadLevel     uint64
	HeadHash      string
	Container     Container
	DataDir       string
	ConfigDir     string
	ClientDataDir string
	SharedDir     string
	SnapshotDir   string
	BackupDir     string
	HistoryMode   string
	Endpoint      string
}

type Container struct {
	Id              string
	Name            string
	Ips             []string
	Network         string
	NetworkID       string
	UpdateAvailable bool
	Status          string
	Resources       Resources
}

type Metric struct {
	Time          time.Time
	EnvironmentId string
	Source        string
	Metric        string
	Value         float64
}

type MetricSimple struct {
	Time  time.Time
	Value float64
}

func (this *Metric) ToBytes() []byte {
	b, _ := json.Marshal(this)
	return b
}

type Task struct {
	ModelBase
	Id             string
	Name           string
	Description    string
	Progress       string
	Done           bool
	EnvironmentId  string
	Task           string
	Logs           map[string]string
	State          string
	OnSuccess      string
	OnFailure      string
	ObjectsUpdated [][]string
	Type           string
}

type InviteCode struct {
	ModelBase
	Id          string
	Description string
	Used        int
	MaxUsed     int
}

type Resources struct {
	CPUCount  uint // in milliCPU
	MemoryMax uint // in byte
}

type User struct {
	ModelBase
	Id           string
	Admin        bool
	Username     string
	Password     []byte
	Environments Environment
}

func (this *User) SetPassword(plain string) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(plain), 12)
	this.Password = hash
}

func (this *User) PasswordIsCorrect(plain string) bool {
	err := bcrypt.CompareHashAndPassword(this.Password, []byte(plain))
	if err != nil {
		return false
	}
	return true
}

type Token struct {
	ModelBase
	Id     string
	UserId string
}

// Generated interface functions

// generated for Environment

func (this *Environment) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Environment) GetBucket() []byte {
	return []byte("Environment")
}

func (this *Environment) GetId() string {
	return this.Id
}
func (this *Environment) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Environment) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Environment) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Environment) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Environment) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for InviteCode

func (this *InviteCode) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *InviteCode) GetBucket() []byte {
	return []byte("InviteCode")
}

func (this *InviteCode) GetId() string {
	return this.Id
}
func (this *InviteCode) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *InviteCode) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *InviteCode) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *InviteCode) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *InviteCode) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for User

func (this *User) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *User) GetBucket() []byte {
	return []byte("User")
}

func (this *User) GetId() string {
	return this.Id
}
func (this *User) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *User) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *User) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *User) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *User) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for Token

func (this *Token) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Token) GetBucket() []byte {
	return []byte("Token")
}

func (this *Token) GetId() string {
	return this.Id
}
func (this *Token) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Token) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Token) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Token) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Token) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for Task

func (this *Task) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Task) GetBucket() []byte {
	return []byte("Task")
}

func (this *Task) GetId() string {
	return this.Id
}
func (this *Task) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Task) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Task) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Task) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Task) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for DockerImage

func (this *DockerImage) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *DockerImage) GetBucket() []byte {
	return []byte("DockerImage")
}

func (this *DockerImage) GetId() string {
	return this.Id
}
func (this *DockerImage) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *DockerImage) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *DockerImage) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *DockerImage) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *DockerImage) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for Conseil

func (this *Conseil) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Conseil) GetBucket() []byte {
	return []byte("Conseil")
}

func (this *Conseil) GetId() string {
	return this.Id
}
func (this *Conseil) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Conseil) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Conseil) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Conseil) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Conseil) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for Arronax

func (this *Arronax) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Arronax) GetBucket() []byte {
	return []byte("Arronax")
}

func (this *Arronax) GetId() string {
	return this.Id
}
func (this *Arronax) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Arronax) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Arronax) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Arronax) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Arronax) setLastSaveNow() {
	this.LastSave = time.Now()
}

// generated for Project

func (this *Project) Size() int64 {
	return int64(len(this.Marshal()))
}

func (this *Project) GetBucket() []byte {
	return []byte("Project")
}

func (this *Project) GetId() string {
	return this.Id
}
func (this *Project) Marshal() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *Project) UnMarshal(b []byte) error {
	return json.Unmarshal(b, this)
}

func (this *Project) SetCreatedNow() {
	this.Created = time.Now()
}

func (this *Project) setLastReadNow() {
	this.LastRead = time.Now()
}

func (this *Project) setLastSaveNow() {
	this.LastSave = time.Now()
}
