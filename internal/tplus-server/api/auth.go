package api

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

func (this *Api) registerAutRoutes(r *gin.Engine) {
	r.POST("/token", this.getToken)
	r.POST("/register", this.register)
	r.GET("/register_requirements", this.registerRequirements)
	r.GET("/info", this.info)
}

func (this *Api) getToken(c *gin.Context) {
	params := TokenRequest{}
	if e := c.Bind(&params); e != nil {
		this.error(errors.New("Invalid Username or Password"), c)
		return
	}
	t, e := this.user.GetToken(params.Username, params.Password)
	if e == nil {
		c.JSON(200, Token{Token: t.Id})
		return
	}
	this.error(e, c)
}

func (this *Api) register(c *gin.Context) {
	params := RegisterInput{}
	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}
	e := this.user.Register(params.Username, params.Password, params.InviteCode)
	this.genericResponse(e, "User created", c)
}

func (this *Api) info(c *gin.Context) {
	res := Info{}
	res.ServerMode = this.config.Mode
	u := this.User(c)
	if u == nil {
		res.LoggedIn = false
		res.UserRole = "na"
		res.UserID = ""
	} else {
		res.LoggedIn = true
		res.UserRole = "user"
		if u.Admin {
			res.UserRole = "admin"
		}
		res.UserID = u.Id
	}
	c.JSON(200, res)
}

func (this *Api) registerRequirements(c *gin.Context) {
	type Body struct {
		RegisterAllowed bool
		CodeOnly        bool
	}
	body := Body{}
	if this.config.Mode == "local" {
		body.RegisterAllowed = this.config.Register.Enabled
		body.CodeOnly = false
	} else {
		body.RegisterAllowed = this.config.Register.Enabled
		body.CodeOnly = true
	}
	c.JSON(200, body)
}
