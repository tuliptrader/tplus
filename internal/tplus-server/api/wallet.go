package api

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) walletRoutes(r *gin.Engine) {
	r.POST("/wallet/:id/new/:name", this.authUser(this.NewAddress))
	r.POST("/wallet/:id/alias/:name/:key", this.authUser(this.NewAlias))
	r.POST("/wallet/:id/faucet/:name", this.authUser(this.NewFaucet))
	r.GET("/wallet/:id/remove/:name", this.authUser(this.DeleteKey))
	r.POST("/wallet/:id/delegate/:from/:to", this.authUser(this.Delegate))
	r.POST("/wallet/:id/transfer/:from/:to/:amount", this.authUser(this.Transfer))
	r.GET("/wallet/:id/list", this.authUser(this.GetAddresses))
	r.GET("/wallet/:id/pending", this.authUser(this.GetPendingOps))
	r.GET("/wallet/:id/bake", this.authUser(this.Bake))
}


func (this *Api) GetAddresses(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.GetAddresses(&envs)
	c.JSON(200, res)
}

func (this *Api) Delegate(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.Delegate(&envs,c.Param("from"),c.Param("to"))
	c.JSON(200, res)
}

func (this *Api) Bake(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	res := this.wallet.Bake(&envs)
	c.JSON(200, res)
}

func (this *Api) Transfer(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.Transfer(&envs,c.Param("from"),c.Param("to"), c.Param("amount"))
	c.JSON(200, res)
}

func (this *Api) GetPendingOps(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.GetPendingOps(&envs)
	c.JSON(200, res)
}

func (this *Api) NewAddress(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.NewAddress(&envs, c.Param("name"))
	c.JSON(200, res)
}

func (this *Api) NewAlias(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.NewAlias(&envs, c.Param("name"),c.Param("key"))
	c.JSON(200, res)
}

func (this *Api) DeleteKey(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.DeleteKey(&envs, c.Param("name"))
	c.JSON(200, res)
}

func (this *Api) NewFaucet(c *gin.Context) {
	type B struct {
		File string `json:"json",form:"json"`
	}
	body := B{}
	c.BindJSON(&body)
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}

	res := this.wallet.NewFaucet(&envs, c.Param("name"),[]byte(body.File))
	c.JSON(200, res)
}


type Address struct {
	Address string
	Balance uint64
	Alias string
}