package api

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (this *Api) registerSocketRoutes(r *gin.Engine) {
	r.GET("/env/:id/console", this.authUser(this.GetConsoles))
	r.GET("/env/:id/console/:name", this.authUser(this.GetSocketToken))
	r.GET("/env/:id/ws/:token", this.GetWebsocket)
}

func (this *Api) GetConsoles(c *gin.Context) {
	env, _ := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	res := []string{}
	for k, _ := range env.Consoles {
		res = append(res, k)
	}
	c.JSON(200, res)
	return
}

func (this *Api) GetSocketToken(c *gin.Context) {
	env, _ := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	target := c.Param("name")
	for k, v := range env.Consoles {
		if target == k {
			token := db.GetRandomId("wstoken")
			this.wsTokens[token] = v.Id
			c.String(200, token)
			return
		}
	}
	if strings.Contains(c.Param("name"), "_bash") {
		token := db.GetRandomId("wstoken")
		bash := this.bash.GetFor(this.User(c), &env)
		this.wsTokens[token] = bash.GetContainerID()
		c.String(200, token)
		return
	}
	return
}

func (this *Api) GetWebsocket(c *gin.Context) {
	token := c.Param("token")
	if _, ok := this.wsTokens[token]; !ok {
		c.JSON(404, "no connection found")
		return
	}

	wsupgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	conn, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}

	r, w, err := this.docker.ExecRWID(this.wsTokens[token], []string{"bash"})
	if err != nil {
		fmt.Println(err)
		c.String(500, "error creating WS connection")
		return
	}

	go func() {
		for {
			// ws -> container
			_, r2, err := conn.NextReader()
			if err != nil {
				break
			}
			data, err := ioutil.ReadAll(r2)
			if err != nil {
				break
			}
			w.Write(data)
		}
	}()

	out := make(chan string)

	go func(out chan string) {
		// container -> ws
		scanner := bufio.NewScanner(r)
		scanner.Split(bufio.ScanBytes)
		for scanner.Scan() {
			b := scanner.Text()
			if err != nil {
				break
			}
			out <- b
		}
		conn.Close()
	}(out)

	go func(out chan string) {

		msg := ""
		ticker := time.NewTicker(25 * time.Millisecond).C
		for {
			select {
			case b := <-out:
				msg = msg + b
				if len(msg) >= 10 {
					conn.WriteMessage(1, Decode([]byte(msg)))
					msg = ""
				}

			case <-ticker:
				if len(msg) >= 1 {
					conn.WriteMessage(1, Decode([]byte(msg)))
					msg = ""
				}
			}
		}
	}(out)

	time.Sleep(5000 * time.Second)

	return
}


func Decode(s []byte) ([]byte) {
	I := bytes.NewReader(s)
	O := transform.NewReader(I, traditionalchinese.Big5.NewDecoder())
	d, e := ioutil.ReadAll(O)
	if e != nil {
		return nil
	}
	return d
}