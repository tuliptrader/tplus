package api

import (
	"bytes"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/packr"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/manager"
	manager2 "gitlab.com/tuliptools/tplus/internal/tplus-server/metrics"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Api struct {
	port      int
	box       *packr.Box
	config    *config.Config
	user      *service.UserService
	docker    *service.DockerService
	envS      *manager.EnvManager
	wallet      *manager.WalletManager
	metrics   *manager2.Metrics
	db        *db.DB
	events    *events.Events
	ws        *events.WebSockets
	tezos     *tezcon.TezosConnector
	wsTokens  map[string]string
	uiUrl     string
	conseil   *manager.ConseilManager
	projects  *manager.ProjectManager
	endpoints *manager.HttpEndpointManager
	log       *logrus.Entry
	bash      *manager.BashManager
	plugins   *manager.PluginsManager
}

type ApiMessage struct {
	Success bool
	Error   string
	Message string
}

func NewApi(conf *config.Config,
	box *packr.Box,
	user *service.UserService,
	d *service.DockerService,
	env *manager.EnvManager,
	metrics *manager2.Metrics,
	db *db.DB, events *events.Events,
	ws *events.WebSockets,
	tezos *tezcon.TezosConnector,
	conseils *manager.ConseilManager,
	projects *manager.ProjectManager,
	endpoints *manager.HttpEndpointManager,
	log *logrus.Logger,
	bash *manager.BashManager,
	wallet *manager.WalletManager,
	plugins *manager.PluginsManager,
) *Api {
	a := Api{}
	a.port = conf.WebPort
	a.config = conf
	a.box = box
	a.user = user
	a.ws = ws
	a.db = db
	a.docker = d
	a.envS = env
	a.metrics = metrics
	a.events = events
	a.tezos = tezos
	a.conseil = conseils
	a.wsTokens = map[string]string{}
	a.projects = projects
	a.endpoints = endpoints
	a.log = log.WithField("source", "api")
	a.wallet = wallet
	a.bash = bash
	a.plugins = plugins
	return &a
}

func (this *Api) genericResponse(e error, msg string, c *gin.Context) {
	if e != nil {
		this.error(e, c)
	} else {
		res := ApiMessage{}
		res.Success = true
		res.Message = msg
		res.Error = ""
		c.JSON(200, res)
	}
}

func (this *Api) error(e error, c *gin.Context) {
	res := ApiMessage{}
	res.Success = false
	res.Message = "Error..."
	res.Error = e.Error()
	c.JSON(400, res)
}

func (this *Api) Run() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(RequestIdMiddleware())

	if url, ok := os.LookupEnv("TPLUS_UI_URL"); ok {
		this.uiUrl = url
		fmt.Println("Setting UI URL to", this.uiUrl)
	} else {
		ip, err := this.docker.RunUI()
		if err != nil {
			panic(err)
		}
		this.uiUrl = "http://" + ip
	}
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true
	config.AddAllowHeaders("*")
	r.Use(cors.New(config))

	this.registerAutRoutes(r)
	this.registerdockerRoutes(r)
	this.registerTaskRoutes(r)
	this.registerMetricsRoutes(r)
	this.registerLogRoutes(r)
	this.registerPeerRoutes(r)
	this.registerSocketRoutes(r)
	this.conseilRoutes(r)
	this.projectRoutes(r)
	this.bashRoutes(r)
	this.envRoutes(r)
	this.walletRoutes(r)
	this.HostStatRoutes(r)
	this.pluginRoutes(r)

	r.GET("/", this.root)
	r.GET("/ws", gin.WrapF(this.ws.GetHandlerfunc()))
	r.Any("/ui/*webPath", this.webHandler)
	r.Any("/tezblock/:envId/*webPath", this.tezblockHandler)
	r.Any("/endpoints/*endpointid", this.epHandler)
	r.Any("/endpoints/*endpointid/*url", this.epHandler)
	r.Any("/file/:file", this.fileHandler)
	r.Any("/pluginsrepo/*url", this.pluginsProxy)

	al := sync.WaitGroup{}
	al.Add(1)

	go func() {
		e := r.Run(":" + strconv.Itoa(this.port))
		if e == nil {
			this.log.Info("API server exited")
		} else {
			this.log.Error("API exited with error:", e.Error())
		}
		time.Sleep(1 * time.Second)
		al.Done()
	}()

	go func() {
		time.Sleep(1 * time.Second)
		this.log.Info("API starting")
		this.log.Info("API Endpoint: http://localhost:" + strconv.Itoa(this.port))
		this.log.Info("UI Endpoint:  http://localhost:" + strconv.Itoa(this.port) + "/ui")
	}()

	al.Wait()

}

func (this *Api) tezblockHandler(c *gin.Context) {
	// forward to tezblock website
	envid := c.Param("envId")
	conseil, err := this.db.GetConseilForEnv(envid)
	if err != nil {
		c.String(500, "cant find conseil for tezblock")
	}
	cl := http.Client{}
	url := "https://tezblock.tulip.tools" + c.Param("webPath")
	req, err := http.NewRequest(c.Request.Method, url, c.Request.Body)
	resp, err := cl.Do(req)
	if err != nil {
		c.Abort()
		return
	}
	body, _ := ioutil.ReadAll(resp.Body)
	endpointurl := "localhost:" + strconv.Itoa(this.port) + "/endpoints/" + conseil.HTTPEndpoint
	body = bytes.Replace(body, []byte("conseilurl"), []byte(endpointurl), -1)
	body = bytes.Replace(body, []byte("CONSEILURL"), []byte(endpointurl), -1)
	body = bytes.Replace(body, []byte("CONSEILKEY"), []byte("key"), -1)
	body = bytes.Replace(body, []byte("href=\"/"), []byte("href=\"http://localhost:"+strconv.Itoa(this.port)+"/tezblock/"+envid+"/"), -1)
	body = bytes.Replace(body, []byte("url(\"/"), []byte("url(\"http://localhost:"+strconv.Itoa(this.port)+"/tezblock/"+envid+"/"), -1)
	body = bytes.Replace(body, []byte("url(/"), []byte("url(http://localhost:"+strconv.Itoa(this.port)+"/tezblock/"+envid+"/"), -1)
	body = bytes.Replace(body, []byte("src=\"/"), []byte("src=\"http://localhost:"+strconv.Itoa(this.port)+"/tezblock/"+envid+"/"), -1)
	body = bytes.Replace(body, []byte("/assets/img/tezblock_logo.svg"), []byte("http://localhost:"+strconv.Itoa(this.port)+"/tezblock/"+envid+"/assets/img/tezblock_logo.svg"), -1)
	c.Header("Content-Type", resp.Header.Get("Content-Type"))

	io.Copy(c.Writer, bytes.NewReader(body))
	return
}


func (this *Api) webHandler(c *gin.Context) {
	// forward to web container
	cl := http.Client{}
	url := this.uiUrl + c.Param("webPath")
	req, err := http.NewRequest(c.Request.Method, url, c.Request.Body)
	resp, err := cl.Do(req)
	if err != nil {
		c.Abort()
		return
	}
	c.Header("Content-Type", resp.Header.Get("Content-Type"))
	io.Copy(c.Writer, resp.Body)
	return
}

func (this *Api) pluginsProxy(c *gin.Context) {
	// forward to web container
	giturl := "https://gitlab.com/tuliptools/TplusPlugins/-/raw/master/"
	remote, err := url.Parse(giturl)
	if err != nil {
		panic(err)
	}
	if remote.Scheme == "https" {
		c.Request.URL.Host = remote.Host
		c.Request.URL.Scheme = remote.Scheme
		c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
		c.Request.Host = remote.Host
	}
	c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/pluginsrepo", "", 1)
	c.Request.URL, _ = url.Parse(c.Request.RequestURI)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ServeHTTP(c.Writer, c.Request)
}


func (this *Api) fileHandler(c *gin.Context) {
	file := c.Param("file")
	path := ""
	switch file {
	case "sandbox_json":
		path = "tezos/proto_params/sandbox.json"
	case "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb":
		path = "tezos/proto_params/PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb.json"
	}

	b,err := this.box.Find(path)
	if err != nil {
		c.String(404,"file not found")
	} else {
		c.String(200,string(b))
	}
	return
}

func (this *Api) epHandler(c *gin.Context) {
	this.endpoints.HandleGin(c.Param("endpointid"), c)
}

func (this *Api) root(c *gin.Context) {
	c.String(200, "UI is under /ui")
}

func (this *Api) authUser(next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("TOKEN")
		if len(token) <= 10 {
			c.String(401, "unauthorized")
			return
		}
		_, e := this.user.TokenToUser(token)
		if e == nil {
			next(c)
			return
		}
		c.String(401, "unauthorized")
		return
	}
}


func (this *Api) authAdmin(next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("TOKEN")
		if len(token) <= 10 {
			c.String(401, "unauthorized")
			return
		}
		u, e := this.user.TokenToUser(token)
		if e == nil && u.Admin {
			next(c)
			return
		}
		c.String(401, "unauthorized")
		return
	}
}

func (this *Api) User(c *gin.Context) *db.User {
	token := c.GetHeader("TOKEN")
	u, _ := this.user.TokenToUser(token)
	return u
}

func RequestIdMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("X-Request-Id", uuid.New().String())
		c.Next()
	}
}
