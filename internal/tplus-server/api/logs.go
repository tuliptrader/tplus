package api

import (
	"bufio"
	"github.com/gin-gonic/gin"
)

func (this *Api) registerLogRoutes(r *gin.Engine) {
	r.GET("/env/:id/logs/tezos/:tail", this.authUser(this.getLogs))
}

func (this *Api) getLogs(c *gin.Context) {
	e, _ := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	reader, err := this.docker.GetLogs(e.Tezos.Container.Id, false, c.Param("tail"))
	buffer := []string{}
	scanner := bufio.NewScanner(reader)
	if err == nil {
		for scanner.Scan() {
			text := scanner.Text()
			if len(text) >= 8 {
				buffer = append(buffer, text[8:])
			}
		}
	}
	c.JSON(200, buffer)
}
