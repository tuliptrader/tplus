package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/helpers"
)

func (this *Api) envRoutes(r *gin.Engine) {
	r.POST("/env/create/local", this.authUser(this.newLocalEnv))
	r.POST("/env/create/remote", this.authUser(this.newRemoteEnv))
	r.GET("/env", this.authUser(this.GetEnvs))
	r.GET("/env/:id", this.authUser(this.GetEnv))
	r.GET("/env/:id/stop", this.authUser(this.StopEnv))
	r.GET("/env/:id/start", this.authUser(this.StartEnv))
	r.GET("/env/:id/restart", this.authUser(this.RestartEnv))
	r.GET("/env/:id/clean", this.authUser(this.CleanEnv))
	r.GET("/env/:id/destroy", this.authUser(this.DestroyEnv))
}

func (this *Api) newLocalEnv(c *gin.Context) {
	params := AddEnvInput{}
	params.HistoryMode = "archive"
	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}
	snapsource := ""
	if params.FromSnapshot {
		if params.SnapshotSource == "tulip" {
			snapsource,_ = helpers.GetTulipSnapshotUrl(params.Network,params.HistoryMode)
		} else {
			snapsource = params.SnapshotSource
		}
	}
	backup := ""
	if params.FromBackup {
		if params.BackupSource == "tulip" {
			backup = "tulip"
		} else {
			backup = params.BackupSource
		}
	}
	env := db.Environment{
		Protocol: params.Protocol,
		SandboxParams: params.SandboxParams,
		SandboxConfig: params.SandboxConfig,
		Id:              db.GetRandomId("env"),
		Name:            params.Name,
		Shared:          params.Shared,
		CreatedByUserId: this.User(c).Id,
		SnapshotSource: snapsource,
		BackupSource: backup,
		Error: "",
		Tezos: db.TezosNode{
			Remote: false,
			RemoteNets: []string{},
			Branch:      params.Network,
			Status:      "pending",
			HeadLevel:   0,
			HistoryMode: params.HistoryMode,
			Sandboxed:   params.Network == "sandbox",
			Container: db.Container{
				Resources: db.Resources{
					MemoryMax: params.RAM,
					CPUCount:  params.CPU,
				},
				Status: "pending",
			},
		},
	}
	var err error
	if env.Tezos.Sandboxed {
		err = this.envS.CreateSandboxNode(&env)
	} else {
		err = this.envS.CreatePublicNetworkNode(&env)
	}
	if err != nil {
		this.error(err, c)
		return
	}
	this.genericResponse(nil, "Environment created", c)
}



func (this *Api) newRemoteEnv(c *gin.Context) {
	params := AddRemoteInput{}

	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}

	if len(params.RemoteNodes) == 0 {
		this.error(errors.New("no remote Endpoints configured"),c)
		return
	}

	env := db.Environment{
		Protocol: "",
		SandboxParams: "",
		SandboxConfig: "",
		Id:              db.GetRandomId("env"),
		Name:            params.Name,
		Shared:          params.Shared,
		CreatedByUserId: this.User(c).Id,
		SnapshotSource: "",
		BackupSource: "",
		Error: "",
		Tezos: db.TezosNode{
			Remote: true,
			RemoteNets: params.RemoteNodes,
			Branch:      "remotenet",
			Status:      "pending",
			HeadLevel:   0,
			HistoryMode: "unknown",
			Sandboxed:   false,
			Container: db.Container{
				Resources: db.Resources{
					MemoryMax: 500,
					CPUCount:  1,
				},
				Status: "pending",
			},
		},
	}
	var err error
	err = this.envS.CreateRemoteNode(&env)
	if err != nil {
		this.error(err, c)
		return
	}
	this.genericResponse(nil, "Environment created", c)
}

func (this *Api) GetEnvs(c *gin.Context) {
	envs, err := this.envS.GetEnvironments(this.User(c))
	if err != nil {
		this.error(err, c)
		return
	}
	c.JSON(200, envs)
}

func (this *Api) GetEnv(c *gin.Context) {
	envs, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	c.JSON(200, envs)
}

func (this *Api) StopEnv(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.envS.Stop(&env)
	c.JSON(200, "Sheduled.")
}

func (this *Api) StartEnv(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.envS.Start(&env)
	c.JSON(200, "Sheduled.")
}

func (this *Api) RestartEnv(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.envS.Restart(&env)
	c.JSON(200, "Sheduled.")
}

func (this *Api) CleanEnv(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.envS.Clean(&env)
	c.JSON(200, "Sheduled.")
}

func (this *Api) DestroyEnv(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.envS.Destroy(&env)
	c.JSON(200, "Sheduled.")
}
