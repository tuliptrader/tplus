package api

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) conseilRoutes(r *gin.Engine) {
	r.GET("/conseil/:id", this.authUser(this.GetConseil))
	r.GET("/conseil/:id/apikeys", this.authUser(this.GetConseilApiKeys))
	r.POST("/conseil/:id/setapikeys", this.authUser(this.SetConseilAPIKeys))
	r.POST("/conseil/:id/endpoint", this.authUser(this.SetConseilEndpointStatus))
	r.GET("/conseil/:id/start", this.authUser(this.StartConseil))
	r.GET("/conseil/:id/stop", this.authUser(this.StopConseil))
}

// returns array of keys
func (this *Api) GetConseilApiKeys(c *gin.Context) {
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilById(c.Param("id"))
	c.JSON(200, conseil.ApiKeys)
}

func (this *Api) GetConseil(c *gin.Context) {
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilForEnv(c.Param("id"))
	if conseil.Running && conseil.Installed {
		conseil.Status = "ready"
	}
	c.JSON(200, conseil)
}

func (this *Api) SetConseilAPIKeys(c *gin.Context) {
	params := struct {
		Keys []string `json:"keys,required"`
	}{}
	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilForEnv(c.Param("id"))
	conseil.ApiKeys = params.Keys
	this.conseil.UpdateConseil(*conseil)
	c.JSON(200, conseil)
}

func (this *Api) SetConseilEndpointStatus(c *gin.Context) {
	params := struct {
		Active bool `json:"active,required"`
	}{}
	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilForEnv(c.Param("id"))
	conseil.HTTPEndpointActive = params.Active
	this.conseil.UpdateConseil(*conseil)
	c.JSON(200, conseil)
}

func (this *Api) StartConseil(c *gin.Context) {
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilForEnv(c.Param("id"))
	conseil.Installed = true
	conseil.Running = true
	this.conseil.UpdateConseil(*conseil)
	c.JSON(200, conseil)
}

func (this *Api) StopConseil(c *gin.Context) {
	_, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	conseil, _ := this.db.GetConseilForEnv(c.Param("id"))
	conseil.Running = false
	this.conseil.UpdateConseil(*conseil)
	c.JSON(200, conseil)
}
