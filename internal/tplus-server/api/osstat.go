package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/memory"
	linuxproc "github.com/c9s/goprocinfo/linux"
	"os"
	"time"
)

func (this *Api) HostStatRoutes(r *gin.Engine) {
	r.GET("/os/memory", this.authUser(this.GetHostMemoryStats))
	r.GET("/os/proc", this.authUser(this.GetHostProc))
	r.GET("/os/cpu", this.authUser(this.GetCPUStat))
}


func (this *Api) GetHostMemoryStats(c *gin.Context) {
	res := HostMemory{}

	m,err := memory.Get()
	if err != nil {
		res.Error = err.Error()
		res.Success = false
		c.JSON(200,res)
		return
	}
	res.Total = m.Total
	res.Used = m.Used
	res.Cached = m.Cached
	res.Free = m.Free
	c.JSON(200, res)
}

func (this *Api) GetHostProc(c *gin.Context) {
	res := HostProc{}

	stat,err := linuxproc.ReadStat("/proc/stat")
	if err != nil {
		res.Error = err.Error()
		res.Success = false
		c.JSON(200,res)
		return
	}
	res.CPUCount = len(stat.CPUStats)
	res.Processes = stat.Processes
	res.ProcessesBlocked = stat.ProcsBlocked
	res.ProcessesRunning = stat.ProcsRunning
	res.BootTime = stat.BootTime
	res.Hostname,_ = os.Hostname()
	c.JSON(200, res)
}


func (this *Api) GetCPUStat(c *gin.Context) {
	res := CpuStats{}
	before, err := cpu.Get()
	if err != nil {
		res.Success = false
		res.Error = err.Error()
		c.JSON(200,res)
		return
	}
	time.Sleep(time.Duration(1) * time.Second)
	after, err := cpu.Get()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return
	}
	total := float64(after.Total - before.Total)
	res.Total = total
	res.Idle = float64(after.Idle-before.Idle)/total*100
	res.System =  float64(after.System-before.System)/total*100
	res.User = float64(after.User-before.User)/total*100
	c.JSON(200,res)
}

type CpuStats struct {
	Error string
	Success bool
	Total float64
	User float64
	System  float64
	Idle float64
}


type HostProc struct {
	Error string
	Success bool
	CPUCount int
	Processes uint64
	ProcessesRunning uint64
	ProcessesBlocked uint64
	BootTime time.Time
	Hostname string
}


type HostMemory struct {
	Error string
	Success bool
	Total uint64
	Used  uint64
	Cached uint64
	Free uint64
}