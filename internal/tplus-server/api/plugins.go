package api

import (
	"bufio"
	"encoding/json"
	"github.com/gin-gonic/gin"
)

func (this *Api) pluginRoutes(r *gin.Engine) {
	r.POST("/plugins/:id/register", this.authUser(this.RegisterPlugin))
	r.POST("/plugins/:id/stop", this.authUser(this.StopPlugin))
	r.POST("/plugins/:id/start", this.authUser(this.StartPlugin))
	r.POST("/plugins/:id/rm", this.authUser(this.DeletePlugin))
	r.GET("/plugins/:id", this.authUser(this.GetPlugin))
	r.GET("/plugins/:id/logs/:service", this.authUser(this.GetPluginServiceLogs))
}


func (this *Api) RegisterPlugin(c *gin.Context) {
	type B struct {
		ServiceDefinition string `form:"serviceDefinition" json:"serviceDefinition"`
	}
	fa,_ := c.GetRawData()
	body := B{}
	json.Unmarshal(fa,&body)
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	e := this.plugins.RegisterPlugin(&env,body.ServiceDefinition)

	if e != nil {
		c.String(500,e.Error())
	} else {
		c.String(200,"ok")
	}

}

func (this *Api) GetPlugin(c *gin.Context) {
	p := this.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400,"Plugin not found")
	} else {
		c.JSON(200,p.GetPlugin())
	}
}

func (this *Api) GetPluginServiceLogs(c *gin.Context) {
	p := this.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400,"Plugin not found")
	} else {
		for _,s := range p.GetPlugin().Services {
			if s.Name == c.Param("service") {
				reader, err := this.docker.GetLogs(s.ContainerId, false, c.Param("tail"))
				buffer := []string{}
				scanner := bufio.NewScanner(reader)
				if err == nil {
					for scanner.Scan() {
						text := scanner.Text()
						if len(text) >= 8 {
							buffer = append(buffer, text[8:])
						}
					}
				}
				c.JSON(200, buffer)
			}
		}
	}
}


func (this *Api) StopPlugin(c *gin.Context) {
	p := this.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400,"Plugin not found")
	} else {
		p.Stop()
		c.String(200,"ok")
	}
}

func (this *Api) StartPlugin(c *gin.Context) {
	p := this.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400,"Plugin not found")
	} else {
		p.Start()
		c.String(200,"ok")
	}
}

func (this *Api) DeletePlugin(c *gin.Context) {
	p := this.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400,"Plugin not found")
	} else {
		p.Delete()
		c.String(200,"ok")
	}
}