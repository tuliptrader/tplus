package api

import (
	"github.com/gin-gonic/gin"
	"strings"
)

func (this *Api) registerdockerRoutes(r *gin.Engine) {
	r.GET("/container", this.getContainers)
	r.GET("/container/:id", this.containerInspect)
}

func (this *Api) getContainers(c *gin.Context) {
	result := []ContainerLS{}
	containers, err := this.docker.FindContainerWithPrefix("mon")
	for _, c := range containers {
		inspect, e := this.docker.Inspect(c)
		if e == nil {
			result = append(result,
				ContainerLS{
					Name:    inspect.Name[1:],
					ID:      inspect.ID,
					Image:   inspect.Image,
					Command: strings.Join(inspect.Config.Entrypoint, " "),
					Created: inspect.Created,
					Status:  inspect.State.Status,
				})
		}
	}

	if err != nil {
		this.error(err, c)
		return
	}
	c.JSON(200, result)
	return
}

func (this *Api) containerInspect(c *gin.Context) {
	inspect, e := this.docker.Inspect(c.Param("id"))

	if e != nil {
		this.error(e, c)
		return
	}
	c.JSON(200, inspect)
	return
}
