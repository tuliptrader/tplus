package api

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) registerPeerRoutes(r *gin.Engine) {
	r.GET("/env/:id/peers", this.authUser(this.getPeers))
}

func (this *Api) getPeers(c *gin.Context) {
	envid := c.Param("id")

	peers, _ := this.tezos.GetPeers(envid)

	c.JSON(200, peers)

	return
}
