package api

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) registerTaskRoutes(r *gin.Engine) {
	r.GET("/env/:id/tasks", this.authUser(this.getTasks))
	r.GET("/env/:id/tasks/:taskId", this.authUser(this.getTask))
}

func (this *Api) getTasks(c *gin.Context) {
	e, _ := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	tasks, _ := this.db.GetTasksForEnvironment(e.Id)
	c.JSON(200, tasks)
}

func (this *Api) getTask(c *gin.Context) {
	env, e := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if e == nil && env.Id != "" {
		tasks, _ := this.db.GetTaskById(c.Param("taskId"))
		c.JSON(200, tasks)
	} else {
		c.String(404, "task not found")
	}
}
