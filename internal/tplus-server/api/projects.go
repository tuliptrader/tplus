package api

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) projectRoutes(r *gin.Engine) {
	r.GET("/env/:id/projects", this.authUser(this.GetProjects))
	r.GET("/env/:id/projects/:pid", this.authUser(this.GetProject))
	r.POST("/projects/:id/new", this.authUser(this.CreateProject))
}

func (this *Api) GetProjects(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	projects := this.projects.GetProjectsForEnv(&env)
	ps := []struct {
		Name string
		Id   string
	}{}
	for _, p := range projects {
		ps = append(ps, struct {
			Name string
			Id   string
		}{
			Name: p.GetProject().Name,
			Id:   p.GetProject().Id,
		})
	}
	c.JSON(200, ps)
}

func (this *Api) GetProject(c *gin.Context) {
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	project := this.projects.GetProjectById(&env, c.Param("pid"))
	c.JSON(200, project.GetProject())
}

func (this *Api) CreateProject(c *gin.Context) {
	params := struct {
		Name string
	}{}
	if e := c.Bind(&params); e != nil {
		this.error(e, c)
		return
	}
	env, err := this.envS.GetEnvironment(this.User(c), c.Param("id"))
	if err != nil {
		this.error(err, c)
		return
	}
	this.projects.GetProject(&env, params.Name)
	c.JSON(200, "ok")
}
