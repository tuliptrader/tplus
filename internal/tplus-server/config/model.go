package config

import "github.com/gobuffalo/packr"

type Config struct {
	Register RegisterSettings
	Mode     string
	DataDir  string
	ApiPort  int
	WebPort  int
	Box      *packr.Box
}



type RegisterSettings struct {
	Enabled bool
	ReCaptchaEnabled bool
	ReCaptchaCode string
	AllowPublicNodes bool
	MaxEnvs int
	RequireInviteCode bool
}
