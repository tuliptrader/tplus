package config

import (
	"bytes"
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/spf13/viper"
	"github.com/tyler-sommer/stick"
	"log"
	"os"
)

func ReadConfig(box *packr.Box) *Config {

	viper.SetConfigName("server")

	if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
		viper.AddConfigPath("$HOME/.tplus")
	} else {

		viper.AddConfigPath(os.Getenv("TPLUS_SERVER_CONFIG_PATH"))
	}

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("No config file found!!")
		fmt.Println("Run `tplus-server init`")

	}

	c := Config{}
	err = viper.Unmarshal(&c)
	if err != nil {
		fmt.Printf("unable to read config File, %v", err)
	}

	c.Box = box

	os.MkdirAll(c.DataDir, 0777)

	return &c
}

func (this *Config) Save() {

	var hd string

	if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
		hd, _ = os.UserHomeDir() // no trailing /
	} else {
		hd = os.Getenv("TPLUS_SERVER_CONFIG_PATH")
	}

	path := hd + "/.tplus"

	err := os.MkdirAll(path, 0700)
	if err != nil {
		fmt.Println("Could not create config directory " + path)
		fmt.Println("Please make sure this directory exists and retry")
		os.Exit(1)
	}

	os.Remove(path + "/server.yml")
	data, e := this.Box.Find("config.yml.twig")
	f, e := os.OpenFile(path+"/server.yml", os.O_CREATE|os.O_RDWR, 0700)
	if e != nil {
		fmt.Println("Could not create config file in " + path)
		os.Exit(1)
	}

	varmap := map[string]stick.Value{}
	varmap["datadir"] = this.DataDir
	varmap["mode"] = this.Mode
	varmap["reg_enabled"] = this.Register.Enabled
	varmap["req_recaptcha_enabled"] = IfThenElse(this.Register.ReCaptchaEnabled, "true", "false")
	varmap["reg_recaptcha_code"] = this.Register.ReCaptchaCode
	varmap["reg_public_nodes"] = IfThenElse(this.Register.AllowPublicNodes, "true", "false")
	varmap["reg_max_envs"] = this.Register.MaxEnvs
	varmap["reg_invite"] = IfThenElse(this.Register.RequireInviteCode, "true", "false")
	varmap["webport"] = this.WebPort

	buf := &bytes.Buffer{}
	env := stick.New(nil)
	if err := env.Execute(string(data), buf, varmap); err != nil {
		log.Fatal(err)
	}

	f.Write(buf.Bytes())
	f.Close()

}

func IfThenElse(condition bool, a interface{}, b interface{}) interface{} {
	if condition {
		return a
	}
	return b
}
