package events

import (
	"encoding/json"
	"fmt"
	"github.com/BurntSushi/locker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"time"
)

type HubClientMessage struct {
	Client  *Client
	Message []byte
}

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	clientMessages chan HubClientMessage

	// RegisterInternal requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	db   *db.DB
	user *service.UserService

	clientToUsers map[string]*db.User

	pendingmsg map[string][]SocketMessage

	subscriptions map[string][]*Subscription

	locker *locker.Locker
}

type Subscription struct {
	ID     string
	Client *Client
}
type SocketMessage struct {
	Action string
	Params []string
}

type ClientMessage struct {
	Action string      `json:"action"`
	Data   interface{} `json:"data"`
	ID     string      `json:omitempty`
}

func (this *ClientMessage) json() []byte {
	s, _ := json.Marshal(this)
	return s
}

func NewHub(d *db.DB, user *service.UserService) *Hub {
	return &Hub{
		clientMessages: make(chan HubClientMessage, 100),
		register:       make(chan *Client),
		unregister:     make(chan *Client),
		clients:        make(map[*Client]bool),
		db:             d,
		user:           user,
		clientToUsers:  map[string]*db.User{},
		pendingmsg:     map[string][]SocketMessage{},
		subscriptions:  map[string][]*Subscription{},
		locker:         locker.NewLocker(),
	}
}

func (this *Hub) send(message []byte, client *Client) {
	if val, ok := this.clients[client]; !ok || val == false {
		return
	}
	select {
	case client.send <- message:
	default:
		close(client.send)
		delete(this.clients, client)
	}
}

func (this *Hub) SendMessage(topic, key string, msg interface{}) {

	key2 := topic + "::" + key
	for k, _ := range this.subscriptions {
		if MatchesSocket(k, key2) {
			for _, c := range this.subscriptions[k] {
				m := ClientMessage{}
				m.Action = "update"
				m.Data = msg
				m.ID = c.ID
				this.send(m.json(), c.Client)
			}
		}
	}
}

func (this *Hub) ping() {
	ping := ClientMessage{}
	ping.Action = "wsping"
	ping.Data = "ping"
	bytes := ping.json()
	for {
		for c, _ := range this.clients {
			this.send([]byte(bytes), c)
		}
		time.Sleep(2500 * time.Millisecond)
	}
}

func (this *Hub) run() {
	go this.ping()
	for {
		select {
		case client := <-this.register:
			this.clients[client] = true
		case client := <-this.unregister:
			if _, ok := this.clients[client]; ok {
				delete(this.clients, client)
				close(client.send)
			}
		case message := <-this.clientMessages:
			msg := SocketMessage{}
			e := json.Unmarshal(message.Message, &msg)
			if e == nil {
				this.handleClientMessage(message.Client, msg)
			} else {
				max := 120
				if len(message.Message) <= 120 {
					max = len(message.Message)
				}
				fmt.Println("invalid message on socket: ", string(message.Message[:max]))
			}
		}
	}
}

func (this *Hub) handleClientMessage(client *Client, m SocketMessage) {

	if m.Action == "auth" && len(m.Params) == 1 {
		answer := ClientMessage{}
		answer.Action = "msg"
		token := m.Params[0]
		user, err := this.user.TokenToUser(token)
		if err == nil && user != nil {
			this.clientToUsers[client.ID] = user
			answer.Data = "you are now authenticated, congrats!"
			pendings := this.pendingmsg[client.ID]
			this.pendingmsg[client.ID] = []SocketMessage{}
			for _, p := range pendings {
				this.handleClientMessage(client, p)
			}
		} else {
			answer.Data = "failed"
		}
		this.send(answer.json(), client)
		return
	}

	if _, ok := this.clientToUsers[client.ID]; !ok {
		answer := ClientMessage{}
		answer.Action = "socketAuth"
		answer.Data = "you must authenticate first."
		if _, ok := this.pendingmsg[client.ID]; !ok {
			this.pendingmsg[client.ID] = []SocketMessage{}
		}
		this.pendingmsg[client.ID] = append(this.pendingmsg[client.ID], m)
		this.send(answer.json(), client)
		return
	}

	if m.Action == "sub" {
		key := m.Params[0] + "::" + m.Params[1]
		if _, ok := this.subscriptions[key]; !ok {
			this.subscriptions[key] = []*Subscription{}
		}
		for _, r := range this.subscriptions[key] {
			if r.Client.ID == client.ID {
				return
			}
		}
		this.subscriptions[key] = append(this.subscriptions[key], &Subscription{
			Client: client,
			ID:     m.Params[2],
		})
	}

	if m.Action == "unsub" {
		key := m.Params[0] + "::" + m.Params[1]
		if _, ok := this.subscriptions[key]; !ok {
			return
		}
		for _, r := range this.subscriptions[key] {
			new := []*Subscription{}
			if r.ID != m.Params[2] {
				new = append(new, r)
			}
			this.subscriptions[key] = new
		}
	}
}
