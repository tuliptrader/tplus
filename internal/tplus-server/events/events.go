package events

import (
	"container/list"
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"strings"
	"sync"
)

type Events struct {
	listeners map[string]map[string]*list.List
	chans     map[string]map[string]chan struct{}
	m         *sync.Mutex
	socket    *WebSockets
}

func NewEvents(tw *TezosWatcher, db *db.DB, socket *WebSockets) *Events {
	ev := Events{}
	db.RegisterPostSaveHook(ev.postsave)
	ev.m = &sync.Mutex{}
	ev.chans = map[string]map[string]chan struct{}{}
	ev.listeners = map[string]map[string]*list.List{}
	ev.socket = socket
	tw.Start(&ev)
	return &ev
}

func (this *Events) broadcast(topic, key string, value interface{}) {
	this.socket.SendMessage(topic, key, value)
}

func (this *Events) postsave(id string, bytes []byte) {
	if !strings.Contains(id, "user") {
		this.DBSaveEvent(id, string(bytes))
	}
	if !strings.Contains(id, "token") {
		this.DBSaveEvent(id, string(bytes))
	}
	if strings.Contains(id, "env") {
		env := db.Environment{}
		_ = json.Unmarshal(bytes, &env)
		this.EnvUpdateEvent(id, &env)
	}
	if !strings.Contains(id, "conseil") {
		conseil := db.Conseil{}
		_ = json.Unmarshal(bytes, &conseil)
		this.ConseilUpdatedEvent(id, &conseil)
	}
	if !strings.Contains(id, "arronax") {
		arro := db.Conseil{}
		_ = json.Unmarshal(bytes, &arro)
		this.ConseilUpdatedEvent(id, &arro)
	}
	if strings.Contains(id, "task") {
		task := db.Task{}
		_ = json.Unmarshal(bytes, &task)
		this.TaskUpdateEvent(task.EnvironmentId+"::"+task.Id, &task)
		if task.State == "failed" {
			this.TaskFailedEvent(task.EnvironmentId+"::"+task.Id, &task)
		}
		if task.State == "pending" {
			this.TaskCreatedEvent(task.EnvironmentId, &task)
		}
		if task.State == "successfull" {
			this.TaskSuccessEvent(task.EnvironmentId+"::"+task.Id, &task)
		}
	}

}

// Generated Method, do not edit directly
func (this *Events) TezosLogsEvent(key string, value string) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TezosLogs", key, value)

	if _, ok := this.listeners["TezosLogs"]; !ok {
		this.listeners["TezosLogs"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosLogs"]; !ok {
		this.chans["TezosLogs"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TezosLogs"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TezosLogs"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTezosLogs(key string) (chan string, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TezosLogs"]; !ok {
		this.listeners["TezosLogs"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosLogs"]; !ok {
		this.chans["TezosLogs"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan string)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TezosLogs"][key] = q
	this.chans["TezosLogs"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(string)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TezosLogs"], key)
					delete(this.listeners["TezosLogs"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TezosErrorsEvent(key string, value string) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TezosErrors", key, value)

	if _, ok := this.listeners["TezosErrors"]; !ok {
		this.listeners["TezosErrors"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosErrors"]; !ok {
		this.chans["TezosErrors"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TezosErrors"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TezosErrors"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTezosErrors(key string) (chan string, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TezosErrors"]; !ok {
		this.listeners["TezosErrors"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosErrors"]; !ok {
		this.chans["TezosErrors"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan string)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TezosErrors"][key] = q
	this.chans["TezosErrors"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(string)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TezosErrors"], key)
					delete(this.listeners["TezosErrors"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) EnvUpdateEvent(key string, value *db.Environment) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("EnvUpdate", key, value)

	if _, ok := this.listeners["EnvUpdate"]; !ok {
		this.listeners["EnvUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["EnvUpdate"]; !ok {
		this.chans["EnvUpdate"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["EnvUpdate"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["EnvUpdate"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchEnvUpdate(key string) (chan *db.Environment, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["EnvUpdate"]; !ok {
		this.listeners["EnvUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["EnvUpdate"]; !ok {
		this.chans["EnvUpdate"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Environment)
	q := list.New()
	block := make(chan struct{})
	this.listeners["EnvUpdate"][key] = q
	this.chans["EnvUpdate"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Environment)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["EnvUpdate"], key)
					delete(this.listeners["EnvUpdate"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) BlockHeightUpdateEvent(key string, value int64) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("BlockHeightUpdate", key, value)

	if _, ok := this.listeners["BlockHeightUpdate"]; !ok {
		this.listeners["BlockHeightUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["BlockHeightUpdate"]; !ok {
		this.chans["BlockHeightUpdate"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["BlockHeightUpdate"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["BlockHeightUpdate"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchBlockHeightUpdate(key string) (chan int64, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["BlockHeightUpdate"]; !ok {
		this.listeners["BlockHeightUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["BlockHeightUpdate"]; !ok {
		this.chans["BlockHeightUpdate"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan int64)
	q := list.New()
	block := make(chan struct{})
	this.listeners["BlockHeightUpdate"][key] = q
	this.chans["BlockHeightUpdate"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(int64)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["BlockHeightUpdate"], key)
					delete(this.listeners["BlockHeightUpdate"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) HeadUpdateEvent(key string, value string) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("HeadUpdate", key, value)

	if _, ok := this.listeners["HeadUpdate"]; !ok {
		this.listeners["HeadUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["HeadUpdate"]; !ok {
		this.chans["HeadUpdate"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["HeadUpdate"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["HeadUpdate"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchHeadUpdate(key string) (chan string, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["HeadUpdate"]; !ok {
		this.listeners["HeadUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["HeadUpdate"]; !ok {
		this.chans["HeadUpdate"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan string)
	q := list.New()
	block := make(chan struct{})
	this.listeners["HeadUpdate"][key] = q
	this.chans["HeadUpdate"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(string)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["HeadUpdate"], key)
					delete(this.listeners["HeadUpdate"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TezosStatusChangeEvent(key string, value string) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TezosStatusChange", key, value)

	if _, ok := this.listeners["TezosStatusChange"]; !ok {
		this.listeners["TezosStatusChange"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosStatusChange"]; !ok {
		this.chans["TezosStatusChange"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TezosStatusChange"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TezosStatusChange"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTezosStatusChange(key string) (chan string, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TezosStatusChange"]; !ok {
		this.listeners["TezosStatusChange"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TezosStatusChange"]; !ok {
		this.chans["TezosStatusChange"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan string)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TezosStatusChange"][key] = q
	this.chans["TezosStatusChange"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(string)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TezosStatusChange"], key)
					delete(this.listeners["TezosStatusChange"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) MetricEvent(key string, value *db.Metric) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("Metric", key, value)

	if _, ok := this.listeners["Metric"]; !ok {
		this.listeners["Metric"] = map[string]*list.List{}
	}
	if _, ok := this.chans["Metric"]; !ok {
		this.chans["Metric"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["Metric"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["Metric"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchMetric(key string) (chan *db.Metric, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["Metric"]; !ok {
		this.listeners["Metric"] = map[string]*list.List{}
	}
	if _, ok := this.chans["Metric"]; !ok {
		this.chans["Metric"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Metric)
	q := list.New()
	block := make(chan struct{})
	this.listeners["Metric"][key] = q
	this.chans["Metric"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Metric)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["Metric"], key)
					delete(this.listeners["Metric"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) EnvCreatedEvent(key string, value *db.Environment) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("EnvCreated", key, value)

	if _, ok := this.listeners["EnvCreated"]; !ok {
		this.listeners["EnvCreated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["EnvCreated"]; !ok {
		this.chans["EnvCreated"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["EnvCreated"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["EnvCreated"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchEnvCreated(key string) (chan *db.Environment, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["EnvCreated"]; !ok {
		this.listeners["EnvCreated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["EnvCreated"]; !ok {
		this.chans["EnvCreated"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Environment)
	q := list.New()
	block := make(chan struct{})
	this.listeners["EnvCreated"][key] = q
	this.chans["EnvCreated"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Environment)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["EnvCreated"], key)
					delete(this.listeners["EnvCreated"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TaskUpdateEvent(key string, value *db.Task) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TaskUpdate", key, value)

	if _, ok := this.listeners["TaskUpdate"]; !ok {
		this.listeners["TaskUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskUpdate"]; !ok {
		this.chans["TaskUpdate"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TaskUpdate"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TaskUpdate"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTaskUpdate(key string) (chan *db.Task, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TaskUpdate"]; !ok {
		this.listeners["TaskUpdate"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskUpdate"]; !ok {
		this.chans["TaskUpdate"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Task)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TaskUpdate"][key] = q
	this.chans["TaskUpdate"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Task)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TaskUpdate"], key)
					delete(this.listeners["TaskUpdate"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TaskCreatedEvent(key string, value *db.Task) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TaskCreated", key, value)

	if _, ok := this.listeners["TaskCreated"]; !ok {
		this.listeners["TaskCreated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskCreated"]; !ok {
		this.chans["TaskCreated"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TaskCreated"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TaskCreated"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTaskCreated(key string) (chan *db.Task, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TaskCreated"]; !ok {
		this.listeners["TaskCreated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskCreated"]; !ok {
		this.chans["TaskCreated"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Task)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TaskCreated"][key] = q
	this.chans["TaskCreated"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Task)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TaskCreated"], key)
					delete(this.listeners["TaskCreated"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TaskFailedEvent(key string, value *db.Task) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TaskFailed", key, value)

	if _, ok := this.listeners["TaskFailed"]; !ok {
		this.listeners["TaskFailed"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskFailed"]; !ok {
		this.chans["TaskFailed"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TaskFailed"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TaskFailed"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTaskFailed(key string) (chan *db.Task, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TaskFailed"]; !ok {
		this.listeners["TaskFailed"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskFailed"]; !ok {
		this.chans["TaskFailed"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Task)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TaskFailed"][key] = q
	this.chans["TaskFailed"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Task)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TaskFailed"], key)
					delete(this.listeners["TaskFailed"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) TaskSuccessEvent(key string, value *db.Task) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("TaskSuccess", key, value)

	if _, ok := this.listeners["TaskSuccess"]; !ok {
		this.listeners["TaskSuccess"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskSuccess"]; !ok {
		this.chans["TaskSuccess"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["TaskSuccess"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["TaskSuccess"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchTaskSuccess(key string) (chan *db.Task, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["TaskSuccess"]; !ok {
		this.listeners["TaskSuccess"] = map[string]*list.List{}
	}
	if _, ok := this.chans["TaskSuccess"]; !ok {
		this.chans["TaskSuccess"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Task)
	q := list.New()
	block := make(chan struct{})
	this.listeners["TaskSuccess"][key] = q
	this.chans["TaskSuccess"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Task)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["TaskSuccess"], key)
					delete(this.listeners["TaskSuccess"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) DBSaveEvent(key string, value string) {
	this.m.Lock()
	defer this.m.Unlock()

	if _, ok := this.listeners["DBSave"]; !ok {
		this.listeners["DBSave"] = map[string]*list.List{}
	}
	if _, ok := this.chans["DBSave"]; !ok {
		this.chans["DBSave"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["DBSave"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["DBSave"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchDBSave(key string) (chan string, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["DBSave"]; !ok {
		this.listeners["DBSave"] = map[string]*list.List{}
	}
	if _, ok := this.chans["DBSave"]; !ok {
		this.chans["DBSave"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan string)
	q := list.New()
	block := make(chan struct{})
	this.listeners["DBSave"][key] = q
	this.chans["DBSave"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(string)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["DBSave"], key)
					delete(this.listeners["DBSave"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) ConseilUpdatedEvent(key string, value *db.Conseil) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("ConseilUpdated", key, value)

	if _, ok := this.listeners["ConseilUpdated"]; !ok {
		this.listeners["ConseilUpdated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["ConseilUpdated"]; !ok {
		this.chans["ConseilUpdated"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["ConseilUpdated"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["ConseilUpdated"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchConseilUpdated(key string) (chan *db.Conseil, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["ConseilUpdated"]; !ok {
		this.listeners["ConseilUpdated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["ConseilUpdated"]; !ok {
		this.chans["ConseilUpdated"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Conseil)
	q := list.New()
	block := make(chan struct{})
	this.listeners["ConseilUpdated"][key] = q
	this.chans["ConseilUpdated"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Conseil)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["ConseilUpdated"], key)
					delete(this.listeners["ConseilUpdated"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

// Generated Method, do not edit directly
func (this *Events) ArronaxUpdatedEvent(key string, value *db.Arronax) {
	this.m.Lock()
	defer this.m.Unlock()

	this.broadcast("ArronaxUpdated", key, value)

	if _, ok := this.listeners["ArronaxUpdated"]; !ok {
		this.listeners["ArronaxUpdated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["ArronaxUpdated"]; !ok {
		this.chans["ArronaxUpdated"] = map[string]chan struct{}{}
	}

	for Lkey, i := range this.listeners["ArronaxUpdated"] {
		if Matches(Lkey, key) {
			i.PushBack(value)
		}
	}

	for Lkey, b := range this.chans["ArronaxUpdated"] {
		if Matches(Lkey, key) {
			select {
			case b <- struct{}{}:
				// sent
			default:
				// non blocking send
				// does not matter because element is in list and will be fetched from there
			}
		}
	}
}

// Generated Method, do not edit directly
func (this *Events) WatchArronaxUpdated(key string) (chan *db.Arronax, chan struct{}) {
	this.m.Lock()
	defer this.m.Unlock()
	key = key + db.GetRandomString(12)
	if _, ok := this.listeners["ArronaxUpdated"]; !ok {
		this.listeners["ArronaxUpdated"] = map[string]*list.List{}
	}
	if _, ok := this.chans["ArronaxUpdated"]; !ok {
		this.chans["ArronaxUpdated"] = map[string]chan struct{}{}
	}

	close := make(chan struct{})
	res := make(chan *db.Arronax)
	q := list.New()
	block := make(chan struct{})
	this.listeners["ArronaxUpdated"][key] = q
	this.chans["ArronaxUpdated"][key] = block

	go func() {
		for {
			qi := q.Front()
			if qi != nil {
				res <- qi.Value.(*db.Arronax)
				q.Remove(qi)
			} else {
				select {
				case <-block:
					// do nothing, send items
				case <-close:
					// kill goroutine
					this.m.Lock()
					delete(this.chans["ArronaxUpdated"], key)
					delete(this.listeners["ArronaxUpdated"], key)
					this.m.Unlock()
					return
				}
			}
		}
	}()
	return res, close
}

func MatchesSocket(listenKey, sendKey string) bool {
	if listenKey == "*" {
		return true
	}
	listensplits := strings.Split(listenKey, "::")
	sendSplits := strings.Split(sendKey, "::")

	if len(listensplits) != len(sendSplits) {
		return false
	}

	for i, _ := range sendSplits {
		if listensplits[i] == "*" && i != len(listensplits)-1 {
			return false
			// accept wildcard only on end
		}
		if listensplits[i] != "*" {
			if listensplits[i] != sendSplits[i] {
				return false
			}
		}
	}
	return true
}

func Matches(listenKey, sendKey string) bool {
	listenKey = listenKey[:len(listenKey)-12]
	if listenKey == "*" {
		return true
	}
	listensplits := strings.Split(listenKey, "::")
	sendSplits := strings.Split(sendKey, "::")

	if len(listensplits) != len(sendSplits) {
		return false
	}

	for i, _ := range sendSplits {
		if listensplits[i] == "*" && i != len(listensplits)-1 {
			return false
			// accept wildcard only on end
		}
		if listensplits[i] != "*" {
			if listensplits[i] != sendSplits[i] {
				return false
			}
		}
	}
	return true
}
