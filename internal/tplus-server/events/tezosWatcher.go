package events

import (
	"bufio"
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"io/ioutil"
	"net/http"
	"regexp"
	"sync"
	"time"
)

type TezosWatcher struct {
	events   *Events
	db       *db.DB
	docker   *service.DockerService
	envs     map[string]bool
	envsLock *sync.Mutex
}

func NewTezosWatcher(docker *service.DockerService, db *db.DB) *TezosWatcher {
	tw := TezosWatcher{}
	tw.docker = docker
	tw.envs = map[string]bool{}
	tw.db = db
	tw.envsLock = &sync.Mutex{}
	return &tw
}

func (this *TezosWatcher) Start(events *Events) {
	this.events = events
	go func() {
		for {
			time.Sleep(1 * time.Second)
			this.watch()
		}
	}()
}

func (this *TezosWatcher) watch() {
	envs, _ := this.db.GetEnvironments()
	for _, e := range envs {
		watch := e
		time.Sleep(10 * time.Millisecond)
		if val, ok := this.envs[watch.Id]; !ok || val == false {
			go func() {
				this.envsLock.Lock()
				this.envs[watch.Id] = true
				this.envsLock.Unlock()

				if watch.Tezos.Remote == false {
					this.watchEnv(&watch)
				} else {
					this.watchRemoteEnv(&watch)
				}

				this.envsLock.Lock()
				this.envs[watch.Id] = false
				this.envsLock.Unlock()
			}()
		}
	}
}

func (this *TezosWatcher) watchEnv(env *db.Environment) {
	if env.Tezos.Container.Status != "running" && env.Tezos.Container.Status != "starting..." {
		return
	}
	logs, err := this.docker.GetLogs(env.Tezos.Container.Id, true, "0")
	reader := bufio.NewScanner(logs)
	blockex, _ := regexp.Compile("Update current head to (\\S*)")
	currenthead := int64(0)

	lastSave := time.Now()

	if reader != nil && err == nil {
		for reader.Scan() {
			text := reader.Text()
			if len(text) >= 8 {
				matches := blockex.FindStringSubmatch(text)
				if len(matches) == 2 {

					this.events.HeadUpdateEvent(env.Id, matches[1])
					if currenthead == 0 {
						info, err := this.docker.GetContainerInfo(env.Tezos.Container.Id)
						ip := info.NetworkSettings.Networks[env.Tezos.Container.Network].IPAddress
						client := http.Client{
							Timeout: 5 * time.Second,
						}
						resp, _ := client.Get("http://" + ip + ":8732/chains/main/blocks/" + matches[1])
						if err == nil {
							bytes, _ := ioutil.ReadAll(resp.Body)
							block := tezcon.Block{}
							err2 := json.Unmarshal(bytes, &block)
							if err2 == nil {
								currenthead = block.Header.Level
								this.events.BlockHeightUpdateEvent(env.Id, currenthead)
							}
							this.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
								update.Tezos.Status = "running"
								update.Tezos.HeadLevel = uint64(currenthead)
								update.Tezos.HeadHash = block.Hash
								return update
							})
						}
					} else {
						currenthead = currenthead + 1
						this.events.BlockHeightUpdateEvent(env.Id, currenthead)

						if lastSave.Before(time.Now().Add(-2 * time.Second)) {
							lastSave = time.Now()
							this.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
								if update.Tezos.Container.Status == "running" {
									update.Tezos.Status = "running"
									update.Tezos.HeadLevel = uint64(currenthead)
									update.Tezos.HeadHash = matches[1]
								}
								return update
							})
						}
					}
				}
				this.events.TezosLogsEvent(env.Id, text[8:])
			}
		}
	}

}


func (this *TezosWatcher) watchRemoteEnv(env *db.Environment) {
	if env.Tezos.Container.Status != "running" && env.Tezos.Container.Status != "starting..." {
		return
	}
	currenthead := int64(0)

	info, err := this.docker.GetContainerInfo(env.Tezos.Container.Id)
	if err != nil {
		ip := info.NetworkSettings.Networks[env.Tezos.Container.Network].IPAddress

		for {
			time.Sleep(5*time.Second)

			client := http.Client{
				Timeout: 5 * time.Second,
			}
			resp, err := client.Get("http://" + ip + ":8732/chains/main/blocks/head")
			if err == nil {
				bytes, _ := ioutil.ReadAll(resp.Body)
				block := tezcon.Block{}
				err2 := json.Unmarshal(bytes, &block)
				currenthead = block.Header.Level
				if err2 == nil && uint64(currenthead) != env.Tezos.HeadLevel {
					this.events.BlockHeightUpdateEvent(env.Id, currenthead)
					this.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
						update.Tezos.Status = "running"
						update.Tezos.HeadLevel = uint64(currenthead)
						update.Tezos.HeadHash = block.Hash
						return update
					})
				}

			}
		}

	}

}
