package events

import (
	"flag"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"net/http"
)

var addr = flag.String("addr", ":8080", "http service address")

type WebSockets struct {
	config *config.Config
	hub    *Hub
}

func NewWebsocket(hub *Hub) *WebSockets {
	ws := WebSockets{}
	ws.hub = hub
	go ws.hub.run()
	return &ws
}

func (this *WebSockets) GetHandlerfunc() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		serveWs(this.hub, w, r)
	}
}

func (this *WebSockets) SendMessage(topic, key string, msg interface{}) {
	this.hub.SendMessage(topic, key, msg)
}
