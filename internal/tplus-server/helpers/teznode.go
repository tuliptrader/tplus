package helpers

import (
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func GetPeers(network string) []string {
	if network == "carthagenet" {
		return []string{}
	}
	res := []string{}
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	resp, err := client.Get("https://files.tplus.dev/peers_" + network)
	if err != nil || resp.StatusCode != 200 {
		return res
	} else {
		body, _ := ioutil.ReadAll(resp.Body)
		return strings.Split(string(body), "\n")
	}
}
