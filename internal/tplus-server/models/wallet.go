package models

type WalletAddressesResponse struct {
	Error string
	Success bool
	Addresses []WalletAddress
}


type WalletAddress struct {
	Address string
	Alias string
	Balance string
	Delegate string
	Counter string
}
