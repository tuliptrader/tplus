package models

type RPCContract struct {
	Balance  string `json:"balance"`
	Delegate string `json:"delegate"`
	Counter  string `json:"counter"`
}
