# Go parameters
GOCMD=go
GOBUILD=packr build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GORUN=$(GOCMD) run
BINARY_NAME= tplus-server
BINARY_UNIX=$(BINARY_NAME)_unix


all: build
build:
		$(GOBUILD) -o $(BINARY_NAME) main.go

codegen:
		$(GORUN) codegen.go